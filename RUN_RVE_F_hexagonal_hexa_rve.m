%% Clear workspace
clc; % clear command line
close all;
warning('on');
matlabrc; % restore MATLAB path, etc.
path(genpath([pwd,'/mfiles_mech']),path); % add path to mfiles folder containing all *.m files related to mechanics
path([pwd,'/mex'],path); % add path to mex folder containing all *.mex files
path([pwd,'/gmsh'],path); % add path to gmsh folder containing gmsh.exe and corresponding IO files

%% Solve statics
% Specify maximum number of threads used in mex files by OpenMP
maxNumThreads = 1;

% Call mode of functions: 's' - silent mode, 'v' - verbose mode
callmode = 'v';

% Specify geometry
CellSize = 1; % size of a rectangular primitive cell
InclusionType = 'circle'; % 'circle', 'hexagon'
DiamOuter = CellSize*1.6; % diameter of the outer circle
if strcmp(InclusionType,'hexagon')
    const = 1-0.1*tan(pi/6); % hexagon, for w/l = 0.1
    DiamInner = const*DiamOuter; % diameter of the inner circle
    hmax = (DiamOuter-DiamInner)/2; % maximum element's size
elseif strcmp(InclusionType,'circle')
    const = 0.8; % circle
    DiamInner = const*DiamOuter; % diameter of the inner circle
    hmax = (DiamOuter-DiamInner)/2; % maximum element's size
end

% Material = [SW,m1,m2,m3,m4,m5,kappa,thickness], C = F'*F, I1 = trace(C), I2 = 0.5*(tr(C)^2-tr(C^2)), J = det(F)
% SW = 0: no material (hole)
% SW = 1: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+1/2*kappa(ln(J))^2 (OOFEM)
% SW = 2: W(F) = m1(I1-3)+m2(I1-3)^2-2m1*ln(J)+1/2*kappa(J-1)^2 (Bertoldi, Boyce)
% SW = 3: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+m3(I1/J^{2/3}-3)*(I2/J^{4/3}-3)+m4(I1/J^{2/3}-3)^2+m5(I1/J^{2/3}-3)^3+9/2*kappa(J^{1/3}-1)^2 (Jamus, Green, Simpson)
% SW = 4: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+m3(I1/J^{2/3}-3)*(I2/J^{4/3}-3)+m4(I1/J^{2/3}-3)^2+m5(I2/J^{2/3}-3)^2+kappa*(J-1)^2 (five-term Mooney-Rivlin)
% SW = 5: W(F) = 0.5*(0.5*(C-I)*(m1*)*0.5*(C-I)) (linear elastic material)
material = [2,0.55e3,0.3e3,0,0,0,55e3,1 % for the surrounding matrix
    0,1e-6,0,0,0,0,1e-6,1]; % for circular inclusions

% Choose element type
elemtype = 9; % 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9, 36 = Q16

% Choose Gauss integration rule (the mesh is homogeneous and the same integration rule for all elements is used)
% triangles: ngauss = 1, 3 (interior), -3 (midedge), 4, 6, 7
% quadrangles: ngauss = 1, 4, 9, 16, 25
switch elemtype
    case 2 % linear triangle
        ngauss = 1;
    case 3 % four node quadrangle
        ngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 9 % quadratic triangle
        ngauss = 3;
    case 16 % eight node quadrangle
        ngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 10 % nine node quadrangle
        ngauss = 3 * 3; % product of two one-dimensional Gauss integration rules
    case 21 % cubic triangle
        ngauss = 6;
    case 36 % cubic quadrangle
        ngauss = 4 * 4;
    otherwise
        error('Wrong element type chosen.');
end

% Type of bifurcation
BifurcationType = 'trueperturbation'; % choose 'trueperturbation' or 'imperfection'

% Tolerances
TOL_g = 1e-10; % geometric tolerance; distance < TOL_g is treated as zero
TOL_r = 1e-3; % elasticity solver realtive tolerance

% Maximum number of Newton iterations
MaxNiter = 30;

% Prescribed defromation gradient F = I+G
loadMult = 5/100;
G1 = loadMult*[0 0;
    0 -1]; % pattern I
G2 = loadMult*[-1 0;
    0 -0.5]; % pattern II
G3 = loadMult*[-1 0;
    0 -1]; % pattern III
G4 = loadMult*[-1 0;
    0 -1.15]; % temporal pattern switching mode I-III
G5 = loadMult*[-1 0;
    0 -0.95]; % temporal pattern switching mode II-III
G = G3;

%% Init mesh
% [p,t] = init_gmsh_hexRVE_sym(DiamOuter,DiamInner,InclusionType,...
%     hmax,material,elemtype,TOL_g,callmode);
[p,t] = init_gmsh_hexRVE_mirrored(DiamOuter,DiamInner,InclusionType,...
    hmax,material,elemtype,TOL_g,callmode);

%% Construct tying conditions
nelem = size(t,2);
ndof = length(p(:));

% Boundary segments
theta0 = -pi/2;
r = DiamOuter;
P0 = [0,0];
P1 = P0+r*[cos(theta0+0*pi/3),sin(theta0+0*pi/3)];
P2 = P0+r*[cos(theta0+1*pi/3),sin(theta0+1*pi/3)];
P3 = P0+r*[cos(theta0+2*pi/3),sin(theta0+2*pi/3)];
P4 = P0+r*[cos(theta0+3*pi/3),sin(theta0+3*pi/3)];
P5 = P0+r*[cos(theta0+4*pi/3),sin(theta0+4*pi/3)];
P6 = P0+r*[cos(theta0+5*pi/3),sin(theta0+5*pi/3)];

P11 = P0+r/2*[cos(theta0+0*pi/3),sin(theta0+0*pi/3)];
P12 = P0+r/2*[cos(theta0+1*pi/3),sin(theta0+1*pi/3)];
P13 = P0+r/2*[cos(theta0+2*pi/3),sin(theta0+2*pi/3)];
P14 = P0+r/2*[cos(theta0+3*pi/3),sin(theta0+3*pi/3)];
P15 = P0+r/2*[cos(theta0+4*pi/3),sin(theta0+4*pi/3)];
P16 = P0+r/2*[cos(theta0+5*pi/3),sin(theta0+5*pi/3)];

id6 = find(abs(p(1,:)-P1(1))+abs(p(2,:)-P1(2))<TOL_g); % bottom centre
id2 = find(abs(p(1,:)-P2(1))+abs(p(2,:)-P2(2))<TOL_g); % bottom right
id3 = find(abs(p(1,:)-P3(1))+abs(p(2,:)-P3(2))<TOL_g); % top right
id4 = find(abs(p(1,:)-P4(1))+abs(p(2,:)-P4(2))<TOL_g); % top centre
id5 = find(abs(p(1,:)-P5(1))+abs(p(2,:)-P5(2))<TOL_g); % top left
id1 = find(abs(p(1,:)-P6(1))+abs(p(2,:)-P6(2))<TOL_g); % bottom left

id11 = find(abs(p(1,:)-P11(1))+abs(p(2,:)-P11(2))<TOL_g);
id12 = find(abs(p(1,:)-P12(1))+abs(p(2,:)-P12(2))<TOL_g);
id13 = find(abs(p(1,:)-P13(1))+abs(p(2,:)-P13(2))<TOL_g);
id14 = find(abs(p(1,:)-P14(1))+abs(p(2,:)-P14(2))<TOL_g);
id15 = find(abs(p(1,:)-P15(1))+abs(p(2,:)-P15(2))<TOL_g);
id16 = find(abs(p(1,:)-P16(1))+abs(p(2,:)-P16(2))<TOL_g);

IDF = [id1;id2;id4]; % only three periodicity control points (for mode computation)

% Gamma_1
theta = -pi/3;
Rot = [cos(theta) -sin(theta)
    sin(theta) cos(theta)];
tp = Rot*[p(1,:);p(2,:)];
IDGamma_1 = find(tp(1,:)<-(DiamOuter*sqrt(3)/2)+TOL_g & tp(2,:)<DiamOuter-TOL_g)';
[~,ids] = sort(p(1,IDGamma_1));
IDGamma_1 = IDGamma_1(ids);
IDGamma_1 = IDGamma_1(2:end-1);

% IDGamma_2
theta = pi/3;
Rot = [cos(theta) -sin(theta)
    sin(theta) cos(theta)];
tp = Rot*[p(1,:);p(2,:)];
IDGamma_2 = find(tp(1,:)>(DiamOuter*sqrt(3)/2)-TOL_g & abs(tp(2,:))<DiamOuter-TOL_g);
[~,ids] = sort(p(1,IDGamma_2));
IDGamma_2 = IDGamma_2(ids)';
IDGamma_2 = IDGamma_2(1:end-1);

% Gamma_3
IDGamma_3 = find(p(1,:)>(DiamOuter*sqrt(3)/2)-TOL_g & abs(p(2,:))<DiamOuter-TOL_g)';
[~,ids] = sort(p(2,IDGamma_3));
IDGamma_3 = IDGamma_3(ids);
IDGamma_3 = IDGamma_3(2:end);

% Gamma_4
theta = -pi/3;
Rot = [cos(theta) -sin(theta)
    sin(theta) cos(theta)];
tp = Rot*[p(1,:);p(2,:)];
IDGamma_4 = find(tp(1,:)>(DiamOuter*sqrt(3)/2)-TOL_g & tp(2,:)<DiamOuter-TOL_g)';
[~,ids] = sort(p(1,IDGamma_4));
IDGamma_4 = IDGamma_4(ids);
IDGamma_4 = IDGamma_4(2:end-1);

% IDGamma_5
theta = pi/3;
Rot = [cos(theta) -sin(theta)
    sin(theta) cos(theta)];
tp = Rot*[p(1,:);p(2,:)];
IDGamma_5 = find(tp(1,:)<-(DiamOuter*sqrt(3)/2)+TOL_g & abs(tp(2,:))<DiamOuter-TOL_g);
[~,ids] = sort(p(1,IDGamma_5));
IDGamma_5 = IDGamma_5(ids)';
IDGamma_5 = IDGamma_5(1:end-1);

% Gamma_6
IDGamma_6 = find(p(1,:)<-(DiamOuter*sqrt(3)/2)+TOL_g & abs(p(2,:))<DiamOuter-TOL_g)';
[~,ids] = sort(p(2,IDGamma_6));
IDGamma_6 = IDGamma_6(ids);
IDGamma_6 = IDGamma_6(2:end);

IDGamma = unique([IDF;IDGamma_1;IDGamma_2;IDGamma_3;IDGamma_4;IDGamma_5;IDGamma_6]);

% Dependency matrix
IC = zeros(2*3*(length(IDGamma_1)+length(IDGamma_2)+length(IDGamma_3)),1);
JC = zeros(size(IC));
SC = zeros(size(IC));
DepIndices = zeros(2*(length(IDGamma_1)+length(IDGamma_2)+length(IDGamma_3)),1);
% Dependencies for Gamma_1 and Gamma_4
for i = 1:length(IDGamma_1)
    % x-direction
    IC((i-1)*6+1:i*6-3) = (2*i-1)*[1,1,1];
    JC((i-1)*6+1:i*6-3) = [2*IDGamma_4(i)-1,2*id4-1,2*id1-1];
    SC((i-1)*6+1:i*6-3) = [1,-1,1];
    % y-direction
    IC((i-1)*6+1+3:i*6) = 2*i*[1,1,1];
    JC((i-1)*6+1+3:i*6) = [2*IDGamma_4(i),2*id4,2*id1];
    SC((i-1)*6+1+3:i*6) = [1,-1,1];
    % Assign dependent indices to ordering of C
    DepIndices(2*i-1) = 2*IDGamma_1(i)-1;
    DepIndices(2*i) = 2*IDGamma_1(i);
    % x-direction
    ICpbc((i-1)*4+1:i*4-2) = (2*i-1)*[1,1];
    JCpbc((i-1)*4+1:i*4-2) = [2*IDGamma_1(i)-1,2*IDGamma_4(i)-1];
    SCpbc((i-1)*4+1:i*4-2) = [1,-1];
    % y-direction
    ICpbc((i-1)*4+1+2:i*4) = 2*i*[1,1];
    JCpbc((i-1)*4+1+2:i*4) = [2*IDGamma_1(i),2*IDGamma_4(i)];
    SCpbc((i-1)*4+1+2:i*4) = [1,-1];
end
% Dependencies for Gamma_2 and Gamma_5
for j = 1:length(IDGamma_2)
    % x-direction
    IC((i+j-1)*6+1:(i+j)*6-3) = (2*(i+j)-1)*[1,1,1];
    JC((i+j-1)*6+1:(i+j)*6-3) = [2*IDGamma_5(j)-1,2*id4-1,2*id2-1];
    SC((i+j-1)*6+1:(i+j)*6-3) = [1,-1,1];
    % y-direction
    IC((i+j-1)*6+1+3:(i+j)*6) = 2*(i+j)*[1,1,1];
    JC((i+j-1)*6+1+3:(i+j)*6) = [2*IDGamma_5(j),2*id4,2*id2];
    SC((i+j-1)*6+1+3:(i+j)*6) = [1,-1,1];
    % Assign dependent indices to ordering of C
    DepIndices(2*(i+j)-1) = 2*IDGamma_2(j)-1;
    DepIndices(2*(i+j)) = 2*IDGamma_2(j);
end
% Dependencies for Gamma_3 and Gamma_6
for k = 1:length(IDGamma_3)
    % x-direction
    IC((i+j+k-1)*6+1:(i+j+k)*6-3) = (2*(i+j+k)-1)*[1,1,1];
    JC((i+j+k-1)*6+1:(i+j+k)*6-3) = [2*IDGamma_6(k)-1,2*id2-1,2*id1-1];
    SC((i+j+k-1)*6+1:(i+j+k)*6-3) = [1,1,-1];
    % y-direction
    IC((i+j+k-1)*6+1+3:(i+j+k)*6) = 2*(i+j+k)*[1,1,1];
    JC((i+j+k-1)*6+1+3:(i+j+k)*6) = [2*IDGamma_6(k),2*id2,2*id1];
    SC((i+j+k-1)*6+1+3:(i+j+k)*6) = [1,1,-1];
    % Assign dependent indices to ordering of C
    DepIndices(2*(i+j+k)-1) = 2*IDGamma_3(k)-1;
    DepIndices(2*(i+j+k)) = 2*IDGamma_3(k);
end
C = sparse(IC,JC,SC,2*(length(IDGamma_1)+length(IDGamma_2)+length(IDGamma_3)),ndof);

% Code numbers
xy = G*p;
DBCIndices = [2*IDF-1;2*IDF]; % code numbers for constrained nodes
DBCValues = [xy(1,IDF)';xy(2,IDF)']; % prescribed displacements for constrained nodes
IndIndices = setdiff(1:ndof,DepIndices)'; % all independent code numbers
FreeIndices = setdiff(IndIndices,DBCIndices); % all free code numbers
DBC2Ind = zeros(size(DBCIndices));
for i = 1:length(DBCIndices)
    DBC2Ind(i) = find(DBCIndices(i)==IndIndices);
end
FIIndices = (1:length(IndIndices))';
FIIndices(DBC2Ind) = [];

%% Perform lineraized buckling analysis
if strcmp(BifurcationType,'imperfection')
    fprintf('Linearized buckling analysis... '),t_start_1 = tic;
    % Take the first loading step and compute stiffness matrices
    dt = 1e-3;
    tDBCValues = 0*DBCValues;
    dtDBCValues = dt*DBCValues;
    txy = dt*G*p;
    [u,Niter,minDiag,status] = solve_r(p,t,material,ngauss,zeros(ndof,1),...
        DBCIndices,DBC2Ind,tDBCValues,dtDBCValues,IndIndices,DepIndices,...
        FreeIndices,FIIndices,C,TOL_r,MaxNiter,maxNumThreads);
    [~,~,~,K1,~] = grad_hess_c(p,t,material,ngauss,0*u(FreeIndices),...
        DBCIndices,DBC2Ind,0*DBCValues,IndIndices,...
        DepIndices,FreeIndices,FIIndices,C,maxNumThreads);
    [~,~,~,K2,~] = grad_hess_c(p,t,material,ngauss,u(FreeIndices),...
        DBCIndices,DBC2Ind,tDBCValues+dtDBCValues,IndIndices,...
        DepIndices,FreeIndices,FIIndices,C,maxNumThreads);
    [V,D] = eigs(K2,K1,10,'sm'); % linearized buckling analysis
    [~,id] = min(diag(D));
    % Plot the first mode
    phi = zeros(size(u));
    phi(FreeIndices) = V(:,id); % scale the eigenvector for plotting purposes
    phi(DepIndices) = C(:,IndIndices)*phi(IndIndices);
    rm = p(:)+DiamInner/3*phi/norm(phi,'inf');
    if strcmp(callmode,'v')
        handle = figure(100);clf,hold all,axis equal;
        plot(rm(1:2:end),rm(2:2:end),'.k');
        set(handle,'name','Linearized buckling mode','numbertitle','off');
    end
    fprintf('in %g s\n\n',toc(t_start_1));
    
    % IMPERFECTION
    if strcmp(BifurcationType,'imperfection')
        p(:) = p(:)+1e-4*CellSize/max(abs(mode1))*mode1;
    end
end

%% Solve for system's response
fprintf('Solving mechanics...\n'),t_start_1 = tic;
fprintf('%d step, %d Nwtn it., %g s\n',1,0,0);
nTimeSteps = 100; % number of time steps
Time = zeros(1,3);
timeIter = zeros(size(Time));
U = zeros(2*size(p,2),length(Time)); % u for all time steps
Nbifur = 0; % number of bifurcation trials
time = 0;
alldtimes = 1/nTimeSteps*ones(size(Time));
dtime = 2*alldtimes(1);
i = 2;
skip = 0;
while time<1
    t_start_2 = tic;
    success = 0;
    Ntrials = 0;
    while ~success
        Ntrials = Ntrials+1;
        % Half the time increment
        dtime = dtime/2;
        if dtime<1/2^10
            fprintf('\n\n\n');
            warning('Step halving failed, dtime < TOL. Continue.');
            fprintf('\n\n\n');
            skip = 1;
            break;
        end
        
        % Get boundary conditions
        tDBCValues = time*DBCValues;
        dtDBCValues = dtime*DBCValues;
        
        % SOLVE FOR u
        [u,Niter,minDiag,status1] = solve_r(p,t,material,ngauss,U(:,i-1),...
            DBCIndices,DBC2Ind,tDBCValues,dtDBCValues,IndIndices,DepIndices,...
            FreeIndices,FIIndices,C,TOL_r,MaxNiter,maxNumThreads);
        
        % Store potentially correct converged minimizing values
        U(:,i) = u;
        if status1==1
            success = 1;
        end
        % Test bifurcation
        if minDiag<0 && success==1
            % Get current Hessian
            t_bif = tic;
            [~,~,~,K1,~] = grad_hess_c(p,t,material,ngauss,U(FreeIndices,i),...
                DBCIndices,DBC2Ind,(time+dtime)*DBCValues,IndIndices,...
                DepIndices,FreeIndices,FIIndices,C,maxNumThreads);
            [V,D] = eigs(K1,10,'sm'); % linearized buckling analysis
            [~,id] = min(diag(D));
            eigval = D(id,id);
            phi = zeros(size(U(:,i)));
            phi(FreeIndices) = V(:,id); % scale the eigenvector for plotting purposes
            phi(DepIndices) = C(:,IndIndices)*phi(IndIndices);
            rm = p(:)+DiamInner/3*phi/norm(phi,'inf');
            if strcmp(callmode,'v')
                handle = figure(100);clf,hold all,axis equal;
                plot(rm(1:2:end),rm(2:2:end),'.k');
                set(handle,'name',['Bifurcatoin mode ',num2str(1),', eigval1 = ',...
                    num2str(D(id,id))],'numbertitle','off');
            end
            phi = 1e-3*CellSize/norm(phi,'inf')*phi; % scale the eigenvector
            % If the eigenvalue is negative
            if D(id,id)<TOL_g
                fprintf('Unstable configuration encountered, eigval1 = %g. Updating...\n',D(id,id));
                % Solve for u again, try multiple times
                jbif = 1;
                while minDiag<0 && jbif<11
                    tphi = (jbif*(jbif-1)+1)*phi;
                    [u,Niter,minDiag,status2] = solve_r(p,t,material,ngauss,U(:,i)+tphi,...
                        DBCIndices,DBC2Ind,tDBCValues+dtDBCValues,0*DBCValues,...
                        IndIndices,DepIndices,FreeIndices,FIIndices,C,TOL_r,MaxNiter,...
                        maxNumThreads);
                    jbif = jbif+1;
                end
                if jbif<11 && status2==1
                    success = 1;
                else
                    success = 0;
                end
            end
            fprintf('Time consumed %g s\n',toc(t_bif));
        end
        if success==0
            fprintf('Halve the time step.\n');
        end
    end
    if skip==1
        break;
    end
    % Rewrite the minimizing value (could change due to bifurcation)
    U(:,i) = u;
    
    % PRINT ITERATION MESSAGE
    fprintf('%d step, %d Nwtn it., %g s\n',i,Niter,toc(t_start_2));
    timeIter(i) = Niter;
    if strcmp(callmode,'v')
        % Plot system's current configuration
        handle = figure(3);clf,hold all,axis equal,box on;
        set(handle,'name','Deformed configuration','numbertitle','off');
        r = reshape(p(:)+u,size(p));
        plot_mesh(handle,r,t,elemtype,[0,0,1]);
        plot(r(1:2:end),r(2:2:end),'.k');
        drawnow;
    end
    
    % Save time and proceed to the next time step
    time = time+dtime;
    Time(i) = time;
    alldtimes(i) = dtime;
    if Ntrials==1
        dtime = 2*1.5*alldtimes(i-1); % increase the time step if everything went well
        dtime = min(dtime,2*alldtimes(1));
    else
        dtime = 2*alldtimes(i-1); % else take the previous time increment
    end
    if time+dtime>1
        dtime = 2*(1-time);
    end
    i = i+1;
end

% Print solution statistics
fprintf('E[Niter] %g\n',mean(timeIter));
fprintf('Max[Niter] %g\n',max(timeIter));
fprintf('Time consumed %g s\n\n',toc(t_start_1));

%% Compute fluctuation's averages
txy = Time(end)*G*p;
phi = U(:,end)-txy(:);
gradvi = [1,1];
meanPhi = build_kinematic_averages(p,t,ngauss,gradvi,phi,maxNumThreads);
phi(1:2:end) = phi(1:2:end)-meanPhi(1);
phi(2:2:end) = phi(2:2:end)-meanPhi(2);
% Make sure that [0,0] is the centre of gravity of tp
[~,~,~,meanp] = build_kinematic_averages(p,t,ngauss,gradvi,ones(size(phi)),maxNumThreads);
tp = [p(1,:)-meanp(1);p(2,:)-meanp(2)];
[m1phi,m2phi,m3phi,m4phi] = build_kinematic_averages(tp,t,ngauss,gradvi,phi,maxNumThreads)

%% Draw results
if strcmp(callmode,'v')
    
    % Compute energy evolution and macroscopic stress, strain, and stiffness
    param = Time*loadMult; % parametrize macroscopic stresses by macroscopic nominal strain
    W = zeros(size(Time));
    P = zeros(4,length(Time));
    S = zeros(4,length(Time));
    D = zeros(2,2,2,2,length(Time));
    V0 = 3*sqrt(3)/2*DiamOuter^2;
    for j = 1:length(Time)
        [W(j),f] = build_grad_hess_TLF2d(p,t,material,ngauss,U(:,j),maxNumThreads);
        % Macroscopic first Piola-Kirchhoff stress
        for k = 1:length(IDGamma)
            P(:,j) = P(:,j)+1/V0*reshape(f([2*IDGamma(k)-1,2*IDGamma(k)])*p(:,IDGamma(k))',4,1); % macro first Piola-Kirchhoff stress
        end
    end
    W = W/V0;
    handle = figure(6);clf,box on;
    plot(param,W),xlabel('$F_{22}$'),ylabel('$W$'),axis tight;
    set(handle,'name','Energy evolution','numbertitle','off');
    handle = figure(7);clf,hold all,box on;
    plot(param,P(1,:),'r');
    plot(param,P(2,:),'g');
    plot(param,P(3,:),'b');
    plot(param,P(4,:),'m');
    legend('$P_{11}$','$P_{21}$','$P_{12}$','$P_{22}$');
    xlabel('$F_{22}$'),ylabel('$P_{ij}$'),axis tight;
    
    % Plot periodic deformed shape
    figure(8);clf;hold all;axis equal;
    a = max(p(1,:));
    b = max(p(2,:))/2;
    pdef = reshape(p(:)+U(:,end),size(p));pdef = [pdef(1,:);pdef(2,:)];pdeplot(pdef,[],t);
    plot(pdef(1,:),pdef(2,:),'.r');
    pdef = reshape(p(:)+U(:,end),size(p));pdef = [pdef(1,:)+2*a*(1+G(1,1));pdef(2,:)];pdeplot(pdef,[],t);
    pdef = reshape(p(:)+U(:,end),size(p));pdef = [pdef(1,:)-2*a*(1+G(1,1));pdef(2,:)];pdeplot(pdef,[],t);
    pdef = reshape(p(:)+U(:,end),size(p));pdef = [pdef(1,:)+a*(1+G(1,1));pdef(2,:)+(3*b)*(1+G(2,2))];pdeplot(pdef,[],t);
    pdef = reshape(p(:)+U(:,end),size(p));pdef = [pdef(1,:)-a*(1+G(1,1));pdef(2,:)-(3*b)*(1+G(2,2))];pdeplot(pdef,[],t);
    pdef = reshape(p(:)+U(:,end),size(p));pdef = [pdef(1,:)+a*(1+G(1,1));pdef(2,:)-(3*b)*(1+G(2,2))];pdeplot(pdef,[],t);
    pdef = reshape(p(:)+U(:,end),size(p));pdef = [pdef(1,:)-a*(1+G(1,1));pdef(2,:)+(3*b)*(1+G(2,2))];pdeplot(pdef,[],t);
end

%% Write data for ParaView
ParaView_export_2d('RVE_G4',Time,p,t,elemtype,ngauss,U,...
    material,TOL_g,maxNumThreads,1)
