%% Compile all the *.mex files
% 0 - compile for release, 1 - compile for debugging
debugMode = 0;
pathEigen = '/home/ondrej/Software/eigen';
clc;

%% Compiling the source codes
disp('Compiling mex files:');
disp('----------------------');
cd mex;
delete('*.mexa64');
delete('*.mexw64');
delete('*.pdb');
string = dir('./src/interface/*.cpp');
if debugMode == 0
    if ispc
        for i = 1:length(string)
            disp(['compiling ',string(i).name]);
            eval(['mex ./src/interface/',string(i).name,' ./src/myfem/src/myfem.cpp -largeArrayDims COMPFLAGS="/openmp $COMPFLAGS" -I./src/myfem/include -I',pathEigen]);
            fprintf('\n');
        end
    elseif isunix
        for i = 1:length(string)
            disp(['compiling ',string(i).name]);
            eval(['mex ./src/interface/',string(i).name,' ./src/myfem/src/myfem.cpp -largeArrayDims CXXOPTIMFLAGS="-O3 -fwrapv -DNDEBUG" CXXFLAGS="\$CXXFLAGS -fopenmp -Wall -std=c++14" LDFLAGS="\$LDFLAGS -fopenmp" -I./src/myfem/include -I',pathEigen]);
            fprintf('\n');
        end
    end
elseif debugMode == 1
    if ispc
        for i = 1:length(string)
            disp(['compiling ',string(i).name]);
            eval(['mex -v -g ./src/interface/',string(i).name,' ./src/myfem/src/myfem.cpp -largeArrayDims -I./src/myfem/include -I',pathEigen]);
            fprintf('\n');
        end
    elseif isunix
        for i = 1:length(string)
            disp(['compiling ',string(i).name]);
            eval(['mex -v -g ./src/interface/',string(i).name,' ./src/myfem/src/myfem.cpp -largeArrayDims CXXFLAGS="\$CXXFLAGS -Wall" -I./src/myfem/include -I',pathEigen]);
            fprintf('\n');
        end
    end
end
clear debugMode;
cd ..;
