#
#	FINITE ELEMENT LIBRARY WITH MATLAB INTERFACE by Ondrej Rokos
#
#	Generator command for Linux (for Windows, replace make with used build system, e.g. VS)
#		mkdir build
#		cd build
# 		cmake -DBUILD_PARSER=ON -DCMAKE_BUILD_TYPE=Release -DPROVIDED_EIGEN_PATH=path_to_Eigen_folder ..
#		make
#

cmake_minimum_required(VERSION 3.10)

project(MEXfunctions
	VERSION 0.1.0
	DESCRIPTION "Finite element library and MEX interfaces"
	LANGUAGES CXX C
)

option(BUILD_PARSER "Compile GMSH parser (requires direct support for std::filesystem)" OFF)


# -------------------------------------------------------------------
# Check for MATLAB presence
# 	+ hack to override CMake FindMatlab module with modified one 
# 	(following https://gitlab.kitware.com/cmake/cmake/issues/19382)
# -------------------------------------------------------------------
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake")

find_package(Matlab)
if (NOT MATLAB_FOUND)
    message(FATAL_ERROR "Matlab instance has not been found.")
endif()


# -------------------------------------------------------------------
# Download and link external libraries
# -------------------------------------------------------------------
add_subdirectory(extern)


# -------------------------------------------------------------------
# Core library
# -------------------------------------------------------------------
add_subdirectory(src/myfem)


# -------------------------------------------------------------------
# Matlab interfaces (and helper functions)
# -------------------------------------------------------------------
add_subdirectory(src/interface)
if (BUILD_PARSER)
	add_subdirectory(src/parser)
endif()
