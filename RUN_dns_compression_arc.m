%% Clear workspace
clc; % clear command line
close all;
warning('on');
matlabrc; % restore MATLAB path, etc.
path(genpath([pwd,'/mfiles_mech']),path); % add path to mfiles folder containing all *.m files related to mechanics
path([pwd,'/mex'],path); % add path to mex folder containing all *.mex files
path([pwd,'/gmsh'],path); % add path to gmsh folder containing gmsh.exe and corresponding IO files

%% Solve statics
% Specify maximum number of threads used in mex files by OpenMP
maxNumThreads = 1;

% Specify name of file for results
fileName = 'global';

% Call mode of functions: 's' - silent mode, 'v' - verbose mode
callmode = 'v';

% Specify geometry
CellSize = 9.97; % size of a rectangular primitive cell
Diameter = 8.67; % diameter of the hole inside primitive cell
Ncell = 2;
SizeX = 2*CellSize/2; % specifies half size of the domain along x-axis (assumes center in [0,0])
SizeY = 4*CellSize/2; % specifies half size of the domain along x-axis (assumes center in [0,0])

% Material = [SW,m1,m2,m3,m4,m5,kappa,thickness], C = F'*F, I1 = trace(C), I2 = 0.5*(tr(C)^2-tr(C^2)), J = det(F)
% SW = 0: no material (hole)
% SW = 1: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+1/2*kappa(ln(J))^2 (OOFEM)
% SW = 2: W(F) = m1(I1-3)+m2(I1-3)^2-2m1*ln(J)+1/2*kappa(J-1)^2 (Bertoldi, Boyce)
% SW = 3: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+m3(I1/J^{2/3}-3)*(I2/J^{4/3}-3)+m4(I1/J^{2/3}-3)^2+m5(I1/J^{2/3}-3)^3+9/2*kappa(J^{1/3}-1)^2 (Jamus, Green, Simpson)
% SW = 4: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+m3(I1/J^{2/3}-3)*(I2/J^{4/3}-3)+m4(I1/J^{2/3}-3)^2+m5(I2/J^{2/3}-3)^2+kappa*(J-1)^2 (five-term Mooney-Rivlin)
% SW = 5: W(F) = 0.5*(0.5*(C-I)*(m1*)*0.5*(C-I)) (linear elastic material)
material = [2,0.55e3,0.3e3,0,0,0,55e3,1 % for the surrounding matrix
    0,1e-6,0,0,0,0,1e-6,1]; % for circular inclusions

% Prescribed target displacement
STRETCH = -0.1;
TargetStretch = 0.75*STRETCH;

% Mesh properties
hmax = CellSize/5; % maximum element's size

% Choose element type
elemtype = 9; % 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9, 36 = Q16

% Choose Gauss integration rule (the mesh is homogeneous and the same integration rule for all elements is used)
% triangles: ngauss = 1, 3 (interior), -3 (midedge), 4, 6, 7
% quadrangles: ngauss = 1, 4, 9, 16, 25
switch elemtype
    case 2 % linear triangle
        ngauss = 1;
    case 3 % four node quadrangle
        ngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 9 % quadratic triangle
        ngauss = 3;
    case 16 % eight node quadrangle
        ngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 10 % nine node quadrangle
        ngauss = 3 * 3; % product of two one-dimensional Gauss integration rules
    case 21 % cubic triangle
        ngauss = 6;
    case 36 % cubic quadrangle
        ngauss = 4 * 4;
    otherwise
        error('Wrong element type chosen.');
end

% Tolerances
SolverType = 1; % 0 - direct backslash, 1 - direct ldlt, 2 - pcg with diagonal preconditioner
TOL_g = 1e-10; % geometric tolerance; distance < TOL_g is treated as zero
TOL_r = 1e-3; % elasticity solver realtive tolerance

% Maximum number of Newton iterations
MaxNiter = 20;

% Specify number of time steps
nTimeSteps = 101;

% Specify shifts
nshifts = 2;
SHIFTSX = CellSize*linspace(0,1,nshifts);
SHIFTSX = SHIFTSX(1:end-1);
SHIFTSY = CellSize*linspace(0,1,nshifts);
SHIFTSY = SHIFTSY(1:end-1);

% Density of sampling points
PointDensity = 100; % number of points per CellSize for sampling F22 for subsequent averaging along x

%% Loop over all scale ratios
time_total = tic;

% Loop over all shifts
count = 0;
for idShiftX = 1:length(SHIFTSX)
    for idShiftY = 1:length(SHIFTSY)
        count = count+1;
        
        % Get current shift
        Shift = [SHIFTSX(idShiftX);SHIFTSY(idShiftY)];
        
        % Get shift
        fprintf('\n\n==============================================\nShift (%d, %d)/(%d, %d)\n',idShiftX,idShiftY,length(SHIFTSX),length(SHIFTSY));
        fprintf('==============================================\n\n');
        
        % Get inclusions
        k = 0;
        NcellX = ceil(2*SizeX/CellSize);
        NcellY = ceil(2*SizeY/CellSize);
        for i = 1:NcellX+1
            for j = 1:NcellY+1
                k = k+1;
                inclusions(k).p = [(i-NcellX/2-1.5)*CellSize;...
                    (j-NcellY/2-1.5)*CellSize]+Shift;
                inclusions(k).r = Diameter/2;
            end
        end
        % Get rid of those inclusions that lie fully outside the specimen
        coords = [inclusions(:).p];
        inclusions(abs(abs(coords(1,:))-Diameter/2)>SizeX+TOL_g | abs(abs(coords(2,:))-Diameter/2)>SizeY+TOL_g) = [];
        if strcmp(callmode,'v')
            handle = figure(1);clf,hold all,axis equal,box on;
            plot(SizeX*[-1 1 1 -1 -1],SizeY*[-1 -1 1 1 -1],'k','linewidth',2);
            phi = linspace(0,2*pi,100);
            for i = 1:length(inclusions)
                plot(inclusions(i).p(1)+inclusions(i).r*cos(phi),...
                    inclusions(i).p(2)+inclusions(i).r*sin(phi),'k',...
                    'linewidth',2);
            end
            set(handle,'name','Inclusions','numbertitle','off');
        end
        
        % Init mesh
        [p,t] = init_gmsh_mve_opencascade(SizeX,SizeY,[0;0],...
            inclusions,hmax,material,elemtype,ngauss,TOL_g,callmode,maxNumThreads);
        t(end,:) = 1;
        
        % Prescribe boundary conditions
        nelem = size(t,2);
        ndof = length(p(:));
        
        % Boundary segments
        IDGamma_1 = find(p(2,:)<-SizeY+TOL_g)';
        IDGamma_3 = find(p(2,:)>SizeY-TOL_g)';
        [~,ids] = sort(p(1,IDGamma_1));
        IDGamma_1 = IDGamma_1(ids);
        [~,ids] = sort(p(1,IDGamma_3));
        IDGamma_3 = IDGamma_3(ids);
        
        % Code numbers
        DBCIndices = [2*IDGamma_1-1;2*IDGamma_1;
            2*IDGamma_3-1;2*IDGamma_3]; % code numbers for constrained nodes
        DBCValues = [zeros(size(IDGamma_1));-STRETCH*SizeY*ones(size(IDGamma_1));
            zeros(size(IDGamma_3));STRETCH*SizeY*ones(size(IDGamma_3))]; % prescribed displacements for constrained nodes
        FreeIndices = setdiff(1:ndof,DBCIndices)'; % all free code numbers
        
        % Solve for system's response
        fprintf('Solving mechanics...\n'),t_start_1 = tic;
        fprintf('%d step, %d Nwtn it., Lambda %g, in %g s\n',1,0,0,0);
        
        Lambda = zeros(3,1);
        U = zeros(2*size(p,2),length(Lambda)); % u for all time steps
        timeIter = zeros(size(Lambda));
        % Initialize arc length
        G = [0 0;0 STRETCH];
        utarget = G*p;
        allLarc = norm(utarget)/nTimeSteps*ones(size(Lambda)); % typical arc lengths for all time steps
        i = 2;
        skip = 0;
        testBifurcation = 1;
        while Lambda(i-1) < 1
            t_start_2 = tic;
            success = 0;
            if i==2
                Larc = 2*allLarc(i-1);
            else
                if Ntrials==1
                    Larc = 2*1.25*allLarc(i-1); % increase the time step if everything went well previously
                    Larc = min(Larc,2*allLarc(1));
                else
                    Larc = 2*allLarc(i-1); % else take the previous time step
                end
            end
            Ntrials = 0;
            while ~success
                Ntrials = Ntrials+1;
                % Half the time increment
                Larc = Larc/2;
                if Larc<1e-6
                    fprintf('\n\n\n');
                    warning('Step halving failed, Larc < TOL. Continue.');
                    fprintf('\n\n\n');
                    skip = 1;
                    break;
                end
                % SOLVE FOR u
                if i>2
                    du = U(:,i-1)-U(:,i-2); % arc-length direction
                else
                    du = utarget;
                end
                [u,Niter,Lambda(i),minDiag,status1] = solve_arc(p,t,material,ngauss,U(:,i-1),...
                    DBCIndices,DBCValues,FreeIndices,du(FreeIndices),TOL_r,Larc,Lambda(i-1),...
                    MaxNiter,'twostep',+1,maxNumThreads);
                % Store potentially correct converged minimizing values
                U(:,i) = u;
                if status1==1
                    success = 1;
                end
                % Test bifurcation
                if minDiag<0 && success && testBifurcation
                    % Get the lowest eigenvalue
                    fprintf('Perform bifurcation analysis...\n');
                    t_bif = tic;
                    [~,~,K1,~] = grad_hess(p,t,material,ngauss,u(FreeIndices),...
                        DBCIndices,Lambda(i)*DBCValues,FreeIndices,maxNumThreads);
                    [V,D] = eigs(K1,5,'sm');
                    [~,id] = min(diag(D));
                    v = V(:,id);
                    phi = zeros(size(u));
                    phi(FreeIndices) = v; % scale the eigenvector for plotting purposes
                    phi = phi/norm(phi);
                    if strcmp(callmode,'v')
                        rm = p(:)+Diameter/3*phi/max(abs(phi));
                        handle = figure(100);clf,hold all,axis equal;
                        plot(rm(1:2:end),rm(2:2:end),'.k');
                        set(handle,'name',['Bifurcatoin mode ',num2str(1),', eigval1 = ',...
                            num2str(D(id,id))],'numbertitle','off');
                    end
                    fprintf('eig1 = %g\n',D(id,id));
                    phi = 1e-3*CellSize/max(abs(phi))*phi;
                    % If the eigenvalue is negative
                    if D(id,id)<1e-3
                        fprintf('Unstable configuration encountered. Updating...\n');
                        % Solve for u again, try multiple times
                        jbif = 1;
                        while minDiag<0 && jbif<11
                            tphi = (jbif*(jbif-1)+1)*phi;
                            [u,Niter,lambda2,minDiag,status2] = solve_arc(p,t,material,ngauss,U(:,i)+tphi,...
                                DBCIndices,DBCValues,FreeIndices,phi(FreeIndices),TOL_r,Larc,Lambda(i),MaxNiter,...
                                'twostep',+1,maxNumThreads);
                            jbif = jbif+1;
                            if ~status2
                                break; % if solver fails, continue right away
                            end
                        end
                        % Rewrite the minimizing value (could change due to bifurcation)
                        U(:,i) = u;
                        Lambda(i) = lambda2;
                        success = 1;
                        if jbif>=11
                            fprintf('jbif >= 11.\n');
                            success = 0;
                        end
                        if status2==0
                            fprintf('Perturbation failed.\n');
                            success = 0;
                        end
                    end
                    fprintf('Time consumed %g s\n',toc(t_bif));
                end
                if success==0
                    fprintf('Halve Larc.\n');
                end
            end
            if skip==1
                break;
            end
            
            % PRINT ITERATION MESSAGE
            fprintf('%d step, %d Nwtn it., Lambda %g, in %g s\n',i,Niter,Lambda(i),toc(t_start_2));
            timeIter(i) = Niter;
            if strcmp(callmode,'v')
                % Plot system's current configuration
                handle = figure(3);clf,hold all,axis equal,box on;
                set(handle,'name','Deformed configuration','numbertitle','off');
                r = reshape(p(:)+u,size(p));
                plot_mesh(handle,r,t,elemtype,[0,0,1]);
                plot(r(1:2:end),r(2:2:end),'.k');
                drawnow;
            end
            
            % Test that wrong direction is not followed for a wrong root
            if Lambda(i)<0.25*max(Lambda)
                fprintf('The system is unloading. Stop.\n');
                break;
            end
            
            % Proceed to the next time step
            allLarc(i) = Larc;
            i = i+1;
        end
        Time = Lambda(1:min(size(U,2),length(Lambda)));
        
        % Print solution statistics
        fprintf('E[Niter] %g\n',mean(timeIter));
        fprintf('Max[Niter] %g\n',max(timeIter));
        fprintf('Time consumed %g s\n\n',toc(t_start_1));
        
        % Get effective stress
        energy = zeros(size(Time));
        stress = zeros(size(Time));
        for j = 1:length(Time)
            [energy(j),f] = build_grad_hess_TLF2d(p,t,material,ngauss,U(:,j),maxNumThreads); % elastic energy of the entire RVE and internal forces
            stress(j) = sum(f(2*IDGamma_1))/(2*SizeX);  % average stress P22
        end
    end
end

%% Save nominal stress and finalize
fprintf('Saving workspace...');t_save = tic;
% save(['../input/stress_',fileName,'_N',num2str(Ncell),'.mat'],'STRESS');
save('workspace','-v7.3');
fprintf('in %g s\n',toc(t_save));

fprintf('\n\n\n==============================================\nTotal time consumed %g\n',toc(time_total));
fprintf('==============================================\n\n\n\n');
