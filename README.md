# mm4mm: MicroMorphic homogenization For Multiscale Metamaterials

`mm4mm` is a Matlab implementation of a micromorphic computational homogenization scheme for elastomeric mechanical metamaterials using a standard Newton method. More details on the theory development can be found in [Rokoš et al., JMPS, 2019](https://doi.org/10.1016/j.jmps.2018.08.019) with further theoretical extensions discussed in [Rokoš et al., EML, 2020](https://doi.org/10.1016/j.eml.2020.100708) and numerical implementation elaborated in [Bree et al., CMAME, 2020](https://doi.org/10.1016/j.cma.2020.113333). Finally, discretization and the proper choice of elements has been investigated in [Rokoš et al., AMSES, 2020](https://doi.org/10.1186/s40323-020-00152-7). The implementation includes Direct Numerical Solution (DNS) as well as micromorhpic formulation (MM). Finite element techniques are used to discretize and solve the resulting governing equations in both cases.

![alt mm results](micromorphic_motivation.png "Direct numerical simulations and homogenized solutions")


## Required packages

The implementation links with the following external packages/tools:

* [GMSH (meshing, required for certain *.m files)](http://gmsh.info/);
* [Eigen (linear algebra, required during MEX compilation)](http://eigen.tuxfamily.org/index.php?title=Main_Page);
* [CMake (compilation, optional)](https://cmake.org/).


## Compilation

Before execution of any of the `RUN_*.m` files, compilation of all `mex` files is required. This can be achieved in two ways:

1. Using CMake to build `mex` files with the standard set of commands. For Linux, type
    ```
    cd mex
    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release
    make
    ```
    CMake automatically downloads the latest version of the Eigen library. Alternatively, you can provide a path to pre-installed Eigen on your computer
    ```
    cd mex
    mkdir build
    cd build
    cmake -DPROVIDED_EIGEN_PATH="your_path_to_eigen" -DCMAKE_BUILD_TYPE=Release
    make
    ```
    On Windows, we recommend using either Visual Studio 2019 Community or VS Code with CMake Tools extension, which both support CMake projects.

1. The second option is to compile all `mex` files directly in Matlab. To this end, a C/C++ compiler needs to be installed and linked to Matlab, see [Matlab supported compilers](https://www.mathworks.com/support/compilers.html) for available options (note that Matlab's support often lacks behind the latest compilers). The MinGW option works well for Windows; the Xcode has been tested for Mac; the standard gcc works well for Linux. To check that the compiler has been properly linked with Matlab, execute `mex -setup` in the Matlab command prompt. 
Once a compiler is linked properly, change `pathEigen = 'your_path_to_eigen';` variable string in `compile_mex.m` to point to Eigen headers on your machine. Finally, perform the compilation executing
    ```
    compile_mex
    ```
    in Matlab from the `mm4mm` folder.


## Meshing
For correct execution of discretization scripts, `gmsh` needs to be added as a system path variable, i.e. to be callable from the command prompt.


## Contact
Ondřej Rokoš, TU/e, [o.rokos@tue.nl](mailto:o.rokos@tue.nl) 

Martin Doškář, CTU, [martin.doskar@fsv.cvut.cz](mailto:martin.doskar@fsv.cvut.cz)
