% Clear workspace
clc; % clear command line
clear all;
close all;
warning('on');
matlabrc; % restore MATLAB path, etc.
path(genpath([pwd,'/mfiles_mech']),path); % add path to mfiles folder containing all *.m files related to mechanics
path([pwd,'/mex'],path); % add path to mex folder containing all *.mex files
path([pwd,'/gmsh'],path); % add path to gmsh folder containing gmsh.exe and corresponding IO files

%% Specify inputs

% Specify maximum number of threads used in mex files by OpenMP
maxNumThreads = 1;

% Call mode of functions: 's' - silent mode, 'v' - verbose mode
callmode = 's';

% RVE type (find further options inside get_rve_data function)
% 'rect_square' rectangular RVE, square packing, circles
% 'rect_hexa' rectangular RVE, hexagonal packing, circles/hexagons
% 'skew_hexa' skewed RVE, hexagonal packing, circles/hexagons
% 'hexa_hexa' hexagonal RVE, hexagonal packing, circles/hexagons
rvetype = 'hexa_hexa';
inclusiontype = 'circle'; % 'circle', 'hexagon'
CellSize = 1; % size of the primitive cell
if strcmp(rvetype,'rect_square')
    DiamOuter = 0.85*CellSize;
    DiamInner = DiamOuter;
    mhmax = CellSize/5;
else
    DiamOuter = CellSize*1.6; % diameter of the outer circle
    if strcmp(inclusiontype,'hexagon')
        const = 1-0.1*tan(pi/6); % hexagon, for w/l = 0.1
        DiamInner = const*DiamOuter; % diameter of the inner circle
        mhmax = (DiamOuter-DiamInner)/2; % maximum element's size
    elseif strcmp(inclusiontype,'circle')
        const = 0.8; % circle
        DiamInner = const*DiamOuter; % diameter of the inner circle
        mhmax = (DiamOuter-DiamInner)/2; % maximum element's size
    end
end

% Type of the long-range correlated mode
% 'analytic' (analytical approximation)
% 'linbuckling' (linear buckling analysis)
% 'truebuckling' (buckling analysis at the bifurcation point)
% 'deformed' (deformed shape solved by Newton)
modetype = 'analytic';

% Choose mode shape between pattern and mode (applies only for modetype = 'deformed')
modeshape = 'mode'; % 'pattern', 'mode'

% Material = [SW,m1,m2,m3,m4,m5,kappa,thickness], C = F'*F, I1 = trace(C), I2 = 0.5*(tr(C)^2-tr(C^2)), J = det(F)
% SW = 0: no material (hole)
% SW = 1: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+1/2*kappa(ln(J))^2 (OOFEM)
% SW = 2: W(F) = m1(I1-3)+m2(I1-3)^2-2m1*ln(J)+1/2*kappa(J-1)^2 (Bertoldi, Boyce)
% SW = 3: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+m3(I1/J^{2/3}-3)*(I2/J^{4/3}-3)+m4(I1/J^{2/3}-3)^2+m5(I1/J^{2/3}-3)^3+9/2*kappa(J^{1/3}-1)^2 (Jamus, Green, Simpson)
% SW = 4: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+m3(I1/J^{2/3}-3)*(I2/J^{4/3}-3)+m4(I1/J^{2/3}-3)^2+m5(I2/J^{2/3}-3)^2+kappa*(J-1)^2 (five-term Mooney-Rivlin)
% SW = 5: W(F) = 0.5*(0.5*(C-I)*(m1*)*0.5*(C-I)) (linear elastic material)
material = [2,0.55e3,0.3e3,0,0,0,55e3,1 % for the surrounding matrix
    0,1e-6,0,0,0,0,1e-6,1]; % for circular inclusions

% Choose m-element type
melemtype = 9; % 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9, 36 = Q16

% Choose m-Gauss integration rule (the mesh is homogeneous and the same integration rule for all elements is used)
% triangles: ngauss = 1, 3 (interior), -3 (midedge), 4, 6, 7
% quadrangles: ngauss = 1, 4, 9, 16, 25
switch melemtype
    case 2 % linear triangle
        mngauss = 1;
    case 3 % four node quadrangle
        mngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 9 % quadratic triangle
        mngauss = 3;
    case 16 % eight node quadrangle
        mngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 10 % nine node quadrangle
        mngauss = 3 * 3; % product of two one-dimensional Gauss integration rules
    case 21 % cubic triangle
        mngauss = 6;
    case 36 % cubic quadrangle
        mngauss = 4 * 4;
    otherwise
        error('Wrong element type chosen.');
end

% Tolerances
TOL_x = 1e-3; % M-minimization algorithm tolerance (dofs)
TOL_f = 1e-3; % M-minimization algorithm tolerance (gradient)
TOL_r = 1e-3; % m-minimization algorithm tolerance (gradient)
MaxNiter = 30; % maximum number of m-Newton interations
TOL_g = 1e-10; % geometric tolerance; distance < TOL_g is treated as zero

%% Get RVE data
time_total = tic;
global DATA; % make DATA global, so its contents can be adjusted inside fminunc during iterative calls
nmodes = get_rve_data(CellSize,rvetype,inclusiontype,DiamInner,DiamOuter,...
    mhmax,modetype,modeshape,material,melemtype,mngauss,MaxNiter,TOL_g,TOL_r,...
    maxNumThreads,callmode);

%% Get macroscopic discretization and boundary conditions
% Specify inputs for M-solver
mbctype = 'pbc'; % 'closed', 'now', 'pbc', 'dbc'
Melemtype = 3; % 2 = T3, 9 = T6, 3 = Q4, 16 = Q8
switch Melemtype
    case 2 % linear triangle
        Mngauss = 3;
    case 3 % four node quadrangle
        Mngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 9 % quadratic triangle
        Mngauss = 6;
    case 16 % eight node quadrangle
        Mngauss = 3 * 3; % product of two one-dimensional Gauss integration rules
    otherwise
        error('Wrong M-element type chosen.');
end

% Get macroscopic mesh
Mhmax = 8*CellSize; % maximum element's size at the macroscale
NcellY = 16;
MSizeX = NcellY*CellSize/2;
MSizeY = NcellY*CellSize/2;
% [pM,tM] = init_rtin(MSizeX,MSizeY,MSizeY/2048,2*Mhmax,TOL_g,material,...
%     callmode,maxNumThreads);
[pM,tM] = init_gmsh_gdic(MSizeX,MSizeY,Mhmax,Melemtype,Mngauss,material,...
    TOL_g,callmode,maxNumThreads);
% [pM,tM] = init_gmsh_mve_opencascade(MSizeX,MSizeY,[0;0],...
%     [],Mhmax,material,Melemtype,Mngauss,TOL_g,callmode,maxNumThreads);
Mnnode = size(pM,2);

% Get M-boundary conditions
loadMult = 1/10;
G1 = loadMult*[0 0;
    0 -1]; % excites pattern I
G2 = loadMult*[-1 0;
    0 -0.5]; % excites pattern II
G3 = loadMult*[-1 0;
    0 -1]; % excites pattern III
G4 = loadMult*[-3 0;
    0 -2]; % gamma = 3/2, loadMult = 50 corresponds to time switching (case 1)
G5 = loadMult*[-3 0;
    0 -4]; % gamma = 3/2, loadMult = 100 corresponds to time switching (case 2)

% Periodicity and prescribed deformation gradient
G = G1;
% Get two corner points
Mid1 = find(pM(1,:)<-MSizeX+TOL_g & pM(2,:)<-MSizeY+TOL_g); % bottom left
Mid2 = find(pM(1,:)>MSizeX-TOL_g & pM(2,:)<-MSizeY+TOL_g); % bottom right
Mid3 = find(pM(1,:)>MSizeX-TOL_g & pM(2,:)>MSizeY-TOL_g); % top right
Mid4 = find(pM(1,:)<-MSizeX+TOL_g & pM(2,:)>MSizeY-TOL_g); % top left
MIDF = [Mid1;Mid2;Mid4];
MIDGamma_1 = find(abs(pM(1,:))<MSizeX-TOL_g & pM(2,:)<-MSizeY+TOL_g);
MIDGamma_2 = find(pM(1,:)>MSizeX-TOL_g & abs(pM(2,:))<MSizeY-TOL_g);
MIDGamma_3 = find(abs(pM(1,:))<MSizeX-TOL_g & pM(2,:)>MSizeY-TOL_g);
MIDGamma_4 = find(pM(1,:)<-MSizeX+TOL_g & abs(pM(2,:))<MSizeY-TOL_g);
[~,Mids] = sort(pM(1,MIDGamma_1));
MIDGamma_1 = MIDGamma_1(Mids);
[~,Mids] = sort(pM(1,MIDGamma_3));
MIDGamma_3 = MIDGamma_3(Mids);
[~,Mids] = sort(pM(2,MIDGamma_2));
MIDGamma_2 = MIDGamma_2(Mids);
[~,Mids] = sort(pM(2,MIDGamma_4));
MIDGamma_4 = MIDGamma_4(Mids);
% Dependency matrix
MC = zeros((2+nmodes)*(length(MIDGamma_1)+length(MIDGamma_2)),(2+nmodes)*Mnnode);
DepIndices = zeros(2*nmodes+(2+nmodes)*(length(MIDGamma_1)+length(MIDGamma_2)),1);
% Dependencies for vertical boundary segments
it = 1;
for i = 1:length(MIDGamma_2)
    % v0 x direction
    MC(it,[2*MIDGamma_4(i)-1,2*Mid2-1,2*Mid1-1]) = [1,1,-1];
    DepIndices(it) = 2*MIDGamma_2(i)-1;
    it = it+1;
    % v0 y direction
    MC(it,[2*MIDGamma_4(i),2*Mid2,2*Mid1]) = [1,1,-1];
    DepIndices(it) = 2*MIDGamma_2(i);
    it = it+1;
    % vi fields
    for j = 1:nmodes
        MC(it,(1+j)*Mnnode+MIDGamma_4(i)) = 1;
        DepIndices(it) = (1+j)*Mnnode+MIDGamma_2(i);
        it = it+1;
    end
end
% Dependencies for horizontal boundary segments
for i = 1:length(MIDGamma_3)
    % v0 x direction
    MC(it,[2*MIDGamma_1(i)-1,2*Mid4-1,2*Mid1-1]) = [1,1,-1];
    DepIndices(it) = 2*MIDGamma_3(i)-1;
    it = it+1;
    % v0 y direction
    MC(it,[2*MIDGamma_1(i),2*Mid4,2*Mid1]) = [1,1,-1];
    DepIndices(it) = 2*MIDGamma_3(i);
    it = it+1;
    % v1 field
    for j = 1:nmodes
        MC(it,(1+j)*Mnnode+MIDGamma_1(i)) = 1;
        DepIndices(it) = (1+j)*Mnnode+MIDGamma_3(i);
        it = it+1;
    end
end
% v0 field: link Mid3 to Mid2
MC(it,[2*Mid2-1,2*Mid4-1,2*Mid1-1]) = [1,1,-1];
DepIndices(it) = 2*Mid3-1;
it = it+1;
MC(it,[2*Mid2,2*Mid4,2*Mid1]) = [1,1,-1];
DepIndices(it) = 2*Mid3;
it = it+1;
% vi fields: link Mid2 with Mid1, Mid3 with Mid1, and Mid4 with Mid1
for j = 1:nmodes
    MC(it,(1+j)*Mnnode+Mid1) = 1;
    DepIndices(it) = (1+j)*Mnnode+Mid2;
    it = it+1;
    MC(it,(1+j)*Mnnode+Mid1) = 1;
    DepIndices(it) = (1+j)*Mnnode+Mid3;
    it = it+1;
    MC(it,(1+j)*Mnnode+Mid1) = 1;
    DepIndices(it) = (1+j)*Mnnode+Mid4;
    it = it+1;
end
MC = sparse(MC);

% Prescribe deformation
xy = G*pM(:,MIDF);

% Code numbers
DBCIndices = [2*MIDF-1;2*MIDF]; % code numbers for constrained nodes
DBCValues = [xy(1,:)';xy(2,:)']; % prescribed displacements for constrained nodes
IndIndices = setdiff(1:(2+nmodes)*Mnnode,DepIndices)'; % all independent code numbers
FreeIndices = setdiff(IndIndices,DBCIndices); % all free code numbers
DBC2Ind = zeros(size(DBCIndices));
for i = 1:length(DBCIndices)
    DBC2Ind(i) = find(DBCIndices(i)==IndIndices);
end
FIIndices = (1:length(IndIndices))';
FIIndices(DBC2Ind) = [];

% USE NEWTON SOLVER AND TIME INCREMENTATION
fprintf('Solving mechanics...\n'),t_start_1 = tic;
fprintf('%d step, %d Nwtn it., Time %g, in %g s\n',1,0,0,0);
% Solve for the evolution process of the system
nTimeSteps = 51;
Time = zeros(1,3);
timeIter = zeros(size(Time));
R = zeros((2+nmodes)*Mnnode,length(Time));
DATA.U = zeros(length(DATA.p(:)),size(tM,2)*Mngauss);
F = zeros((2+nmodes)*Mnnode,length(Time));
P22 = zeros(length(Time),1);
En = zeros(length(Time),1);
Nbifur = 0; % number of bifurcation trials
time = 0;
alldtimes = 1/nTimeSteps*ones(size(Time));
i = 2;
skip = 0;
while time<=1
    t_start_2 = tic;
    success = 0;
    if i==2
        dtime = 2*alldtimes(i-1);
    else
        if Ntrials==1
            dtime = 2*1.5*alldtimes(i-1); % increase the time step if everything went well previously
            dtime = min(dtime,2*alldtimes(1));
        else
            dtime = 2*alldtimes(i-1); % else take the previous time step
        end
    end
    Ntrials = 0;
    while ~success
        Ntrials = Ntrials+1;
        % Halve the time increment
        dtime = dtime/2;
        if dtime<1e-9
            fprintf('\n\n\n');
            warning('Step halving failed, dtime < TOL. Continue.');
            fprintf('\n\n\n');
            skip = 1;
            break;
        end
        % Get boundary conditions
        tDBCValues = time*DBCValues;
        dtDBCValues = dtime*DBCValues;
        % SOLVE FOR u
        [xsol,Niter,minDiag,status1] = solve_rC_micromorphic(pM,tM,Mngauss,R(FreeIndices,i-1),...
            tDBCValues,dtDBCValues,FreeIndices,DBCIndices,DepIndices,IndIndices,FIIndices,DBC2Ind,...
            MC,maxNumThreads,mbctype,TOL_r,nmodes,Mnnode,MaxNiter,'s');
        % Reconstruct the solution
        xr = zeros((2+nmodes)*Mnnode,1);
        xr(DBCIndices) = tDBCValues+dtDBCValues;
        xr(FreeIndices) = xsol;
        xr(DepIndices) = MC(:,IndIndices)*xr(IndIndices);
        % Store potentially correct converged minimizing values
        R(:,i) = xr;
        if status1==1
            success = 1;
        end
        % Test bifurcation
        if minDiag<0 && success==1
            % Get the lowest eigenvalue
            fprintf('Perform bifurcation analysis...\n');
            t_bif = tic;
            [~,~,K] = energy_micromorphic_2d(R(:,i),pM,tM,Mngauss,[],[],...
                (1:2*Mnnode)',[],[],(2*Mnnode+1:(2+nmodes)*Mnnode)',maxNumThreads,mbctype,0,'s','full');
            K1 = K(IndIndices,IndIndices)+K(IndIndices,DepIndices)*MC(:,IndIndices)+...
                MC(:,IndIndices)'*K(DepIndices,IndIndices)+...
                MC(:,IndIndices)'*K(DepIndices,DepIndices)*MC(:,IndIndices);
            K1 = K1(FIIndices,FIIndices);
            [V,D] = eigs(K1,5,'sm');
            [~,id] = min(diag(D));
            phi = V(:,id);
            % Reconstruct buckling mode
            v = zeros((2+nmodes)*Mnnode,1);
            v(FreeIndices) = phi;
            v(DepIndices) = MC(:,IndIndices)*v(IndIndices);
            if strcmp(callmode,'v')
                plot_results_micromorphic_2d(v,1,nmodes,Melemtype,Mngauss,...
                    material,pM,tM,Mnnode,1000,TOL_g,maxNumThreads);
            end
            fprintf('eig1 = %g\n',D(id,id));
            phi = 1e-1*CellSize/max(abs(phi))*phi;
            % If the eigenvalue is negative
            if D(id,id)<TOL_g
                fprintf('Unstable configuration encountered. Updating...\n');
                % Solve for u again, try multiple times
                jbif = 1;
                while minDiag<0 && jbif<11
                    tphi = (jbif*(jbif-1)+1)*phi;
                    [xsol,Niter,minDiag,status2] = solve_rC_micromorphic(pM,tM,Mngauss,R(FreeIndices,i)+tphi,...
                        tDBCValues+dtDBCValues,0*DBCValues,FreeIndices,DBCIndices,DepIndices,IndIndices,FIIndices,DBC2Ind,...
                        MC,maxNumThreads,mbctype,TOL_r,nmodes,Mnnode,MaxNiter,'s');
                    jbif = jbif+1;
                    if ~status2
                        break; % if solver fails, continue right away
                    end                    
                end
                % Reconstruct the solution
                xr = zeros((2+nmodes)*Mnnode,1);
                xr(DBCIndices) = tDBCValues+dtDBCValues;
                xr(FreeIndices) = xsol;
                xr(DepIndices) = MC(:,IndIndices)*xr(IndIndices);
                % Rewrite the minimizing value (could change due to bifurcation)
                R(:,i) = xr;
                success = 1;
                if jbif>=11
                    fprintf('jbif >= 11.\n');
                    success = 0;
                end
                if status2==0
                    fprintf('Perturbation failed.\n');
                    success = 0;
                end                
            end
            fprintf('Time consumed %g s\n',toc(t_bif));
        end
        if success==0
            fprintf('Halve the time step.\n');
        end
    end
    if skip==1
        break;
    end
    
    % PRINT ITERATION MESSAGE
    fprintf('%d step, %d Nwtn it., Time %g, in %g s\n',i,Niter,time,toc(t_start_2));
    timeIter(i) = Niter;
    if strcmp(callmode,'v')
        % Plot system's current configuration
        handle = figure(3);clf,hold all,axis equal,box on;
        set(handle,'name','Deformed configuration','numbertitle','off');
        r = reshape(pM(:)+R(1:2*Mnnode,i),size(pM));
        plot_mesh(handle,r,tM,Melemtype,[0,0,1]);
        plot(r(1:2:end),r(2:2:end),'.k');
        drawnow;
    end
    
    % Sample energy and forces
    [En(i),F(:,i)] = energy_micromorphic_2d(R(:,i),pM,tM,Mngauss,[],[],(1:2*Mnnode)',...
        [],[],(2*Mnnode+1:(2+nmodes)*Mnnode)',maxNumThreads,mbctype,0,'s','full');
    P22(i) = sum(F(2*MIDGamma_3,i))/(2*MSizeX);

    % Save time and proceed to the next time step
    time = time+dtime;
    Time(i) = time;
    alldtimes(i) = dtime;
    if Ntrials==1
        dtime = 2*1.5*alldtimes(i-1); % increase the time step if everything went well
        dtime = min(dtime,2*alldtimes(1));
    else
        dtime = 2*alldtimes(i-1); % else take the previous time increment
    end
    if time+dtime>1
        dtime = 2*(1-time);
    end
    i = i+1;
end
fprintf('%d step, %d Nwtn it., Time %g, in %g s\n',i,Niter,time,toc(t_start_2));

% Print solution statistics
fprintf('E[Niter] %g\n',mean(timeIter));
fprintf('Max[Niter] %g\n',max(timeIter));
fprintf('Time consumed %g s\n\n',toc(t_start_1));

%% Plot solution
idstep = length(Time);
plot_results_micromorphic_2d(R,idstep,nmodes,Melemtype,Mngauss,material,pM,...
    tM,Mnnode,1000,TOL_g,maxNumThreads);

% Plot time evolutions of vi fields for the periodic setting
figure(134);clf;hold all;axis tight;box on;grid on;
for i = 1:nmodes
    plot(abs(R((1+i)*Mnnode+1,:)),'linewidth',1)
    stringlegend{i} = ['$v_',num2str(i),'$'];
end
xlabel('time t');ylabel('$v_i(t)$');
% legend(stringlegend,'orientation','horizontal','location','north');
% ylim([-0.5,6]);
% ylim([-0.5,4]);
% set(gcf,'Position',[1000,500,250,200]);
% legend boxoff;

%% Save some data
% save('workspace','-v7.3');

fprintf('\n\n==============================================\nTotal time consumed %g\n',toc(time_total));
fprintf('==============================================\n\n');
