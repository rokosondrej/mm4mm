%% Clear workspace
clc; % clear command line
close all;
warning('on');
matlabrc; % restore MATLAB path, etc.
path(genpath([pwd,'/mfiles_mech']),path); % add path to mfiles folder containing all *.m files related to mechanics
path([pwd,'/mex'],path); % add path to mex folder containing all *.mex files
path([pwd,'/gmsh'],path); % add path to gmsh folder containing gmsh.exe and corresponding IO files

%% Specify inputs

% Specify maximum number of threads used in mex files by OpenMP
maxNumThreads = 1;

% Call mode of functions: 's' - silent mode, 'v' - verbose mode
callmode = 's';

% RVE type (find further options inside get_rve_data function)
% 'rect_square' rectangular RVE, square packing, circles
% 'rect_hexa' rectangular RVE, hexagonal packing, circles/hexagons
% 'skew_hexa' skewed RVE, hexagonal packing, circles/hexagons
% 'hexa_hexa' hexagonal RVE, hexagonal packing, circles/hexagons
rvetype = 'hexa_hexa';
inclusiontype = 'circle'; % 'circle', 'hexagon'
CellSize = 1; % size of the primitive cell
if strcmp(rvetype,'rect_square')
    DiamOuter = 0.85*CellSize;
    DiamInner = DiamOuter;
    mhmax = CellSize/5;
else
    DiamOuter = CellSize*1.6; % diameter of the outer circle
    if strcmp(inclusiontype,'hexagon')
        const = 1-0.1*tan(pi/6); % hexagon, for w/l = 0.1
        DiamInner = const*DiamOuter; % diameter of the inner circle
        mhmax = (DiamOuter-DiamInner)/2; % maximum element's size
    elseif strcmp(inclusiontype,'circle')
        const = 0.8; % circle
        DiamInner = const*DiamOuter; % diameter of the inner circle
        mhmax = (DiamOuter-DiamInner)/2; % maximum element's size
    end
end

% Type of the long-range correlated mode
% 'analytic' (analytical approximation)
% 'linbuckling' (linear buckling analysis)
% 'truebuckling' (buckling analysis at the bifurcation point)
% 'deformed' (deformed shape solved by Newton)
modetype = 'analytic';

% Choose mode shape between pattern and mode (applies only for modetype = 'deformed')
modeshape = 'mode'; % 'pattern', 'mode'

% Material = [SW,m1,m2,m3,m4,m5,kappa,thickness], C = F'*F, I1 = trace(C), I2 = 0.5*(tr(C)^2-tr(C^2)), J = det(F)
% SW = 0: no material (hole)
% SW = 1: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+1/2*kappa(ln(J))^2 (OOFEM)
% SW = 2: W(F) = m1(I1-3)+m2(I1-3)^2-2m1*ln(J)+1/2*kappa(J-1)^2 (Bertoldi, Boyce)
% SW = 3: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+m3(I1/J^{2/3}-3)*(I2/J^{4/3}-3)+m4(I1/J^{2/3}-3)^2+m5(I1/J^{2/3}-3)^3+9/2*kappa(J^{1/3}-1)^2 (Jamus, Green, Simpson)
% SW = 4: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+m3(I1/J^{2/3}-3)*(I2/J^{4/3}-3)+m4(I1/J^{2/3}-3)^2+m5(I2/J^{2/3}-3)^2+kappa*(J-1)^2 (five-term Mooney-Rivlin)
% SW = 5: W(F) = 0.5*(0.5*(C-I)*(m1*)*0.5*(C-I)) (linear elastic material)
material = [2,0.55e3,0.3e3,0,0,0,55e3,1 % for the surrounding matrix
    0,1e-6,0,0,0,0,1e-6,1]; % for circular inclusions

% Choose m-element type
melemtype = 9; % 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9, 36 = Q16

% Choose m-Gauss integration rule (the mesh is homogeneous and the same integration rule for all elements is used)
% triangles: ngauss = 1, 3 (interior), -3 (midedge), 4, 6, 7
% quadrangles: ngauss = 1, 4, 9, 16, 25
switch melemtype
    case 2 % linear triangle
        mngauss = 1;
    case 3 % four node quadrangle
        mngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 9 % quadratic triangle
        mngauss = 3;
    case 16 % eight node quadrangle
        mngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 10 % nine node quadrangle
        mngauss = 3 * 3; % product of two one-dimensional Gauss integration rules
    case 21 % cubic triangle
        mngauss = 6;
    case 36 % cubic quadrangle
        mngauss = 4 * 4;
    otherwise
        error('Wrong element type chosen.');
end

% Tolerances
TOL_x = 1e-3; % M-minimization algorithm tolerance (dofs)
TOL_f = 1e-3; % M-minimization algorithm tolerance (gradient)
TOL_r = 1e-3; % m-minimization algorithm tolerance (gradient)
MaxNiter = 30; % maximum number of m-Newton interations
TOL_g = 1e-10; % geometric tolerance; distance < TOL_g is treated as zero

%% Get RVE data
time_total = tic;
global DATA; % make DATA global, so its contents can be adjusted inside fminunc during iterative calls
nmodes = get_rve_data(CellSize,rvetype,inclusiontype,DiamInner,DiamOuter,...
    mhmax,modetype,modeshape,material,melemtype,mngauss,MaxNiter,TOL_g,TOL_r,...
    maxNumThreads,callmode);

%% Get macroscopic discretization and boundary conditions
% Specify inputs for M-solver
% Mbctype = 1: periodicity and prescribed deformation gradient
% Mbctype = 2: finite specimen, top and bottom horizontal edges fixed
% Mbctype = 3: finite specimen, entire boundary fixed
% Mbctype = 4: cruciform example for mode mixing
Mbctype = 2;
mbctype = 'pbc'; % 'closed', 'now', 'pbc', 'dbc'
Melemtype = 9; % 2 = T3, 9 = T6, 3 = Q4, 16 = Q8
switch Melemtype % should correspond to mass matrix integration rule
    case 2 % linear triangle
        Mngauss = 3;
    case 3 % four node quadrangle
        Mngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 9 % quadratic triangle
        Mngauss = 6;
    case 16 % eight node quadrangle
        Mngauss = 3 * 3; % product of two one-dimensional Gauss integration rules
    otherwise
        error('Wrong M-element type chosen.');
end

% Get BCs and other constraints
if Mbctype==4
    STRETCH = -0.05;
    Mhmax = 10*CellSize; % maximum element's size at the macroscale
    if strcmp(rvetype,'rect_hexa') || strcmp(rvetype,'skew_hexa') || strcmp(rvetype,'hexa_hexa') % hexagonal stacking
        MSizeX = 20*sqrt(3)*CellSize; % half size of the x-domain
        MSizeY = 20*sqrt(3)*CellSize; % half size of the y-domain
    elseif strcmp(rvetype,'rect_square')
        MSizeX = 20*2*CellSize; % half size of the x-domain
        MSizeY = 20*2*CellSize; % half size of the y-domain
    end
    % [pM,tM] = init_gmsh_mode_mixing(MSizeX,MSizeY,[],Mhmax,material,...
    %     Melemtype,Mngauss,TOL_g,'circle',callmode,maxNumThreads);
    [pM,tM] = init_gmsh_mm_smoothcruciform(MSizeX,MSizeY,[],Mhmax,material,...
        Melemtype,Mngauss,TOL_g,'circle',callmode,maxNumThreads);

    % Get boundary segments
    MIDGamma_1 = find(pM(2,:)<min(pM(2,:))+TOL_g)';
    MIDGamma_2 = find(pM(1,:)>max(pM(1,:))-TOL_g)';
    MIDGamma_3 = find(pM(2,:)>max(pM(2,:))-TOL_g)';
    MIDGamma_4 = find(pM(1,:)<min(pM(1,:))+TOL_g)';
else
    Mhmax = 8*CellSize; % maximum element's size at the macroscale
    NcellY = 16;
    MSizeX = NcellY*CellSize/2;
    MSizeY = NcellY*CellSize/2;
    % [pM,tM] = init_rtin(MSizeX,MSizeY,MSizeY/2048,2*Mhmax,TOL_g,material,...
    %     callmode,maxNumThreads);
    [pM,tM] = init_gmsh_gdic(MSizeX,MSizeY,Mhmax,Melemtype,Mngauss,material,...
        TOL_g,callmode,maxNumThreads);
    % [pM,tM] = init_gmsh_mve_opencascade(MSizeX,MSizeY,[0;0],...
    %     [],Mhmax,material,Melemtype,Mngauss,TOL_g,callmode,maxNumThreads);
    
    % Get boundary segments
    MIDGamma_1 = find(pM(2,:)<-MSizeY+TOL_g)';
    MIDGamma_2 = find(pM(1,:)>MSizeX-TOL_g & abs(pM(2,:))<MSizeY-TOL_g)';
    MIDGamma_3 = find(pM(2,:)>MSizeY-TOL_g)';
    MIDGamma_4 = find(pM(1,:)<-MSizeX+TOL_g & abs(pM(2,:))<MSizeY-TOL_g)';
end
Mnnode = size(pM,2);
% Get M-boundary conditions
loadMult = 5/100;
G1 = loadMult*[0 0;
    0 -1]; % excites pattern I
G2 = loadMult*[-1 0;
    0 -0.3]; % excites pattern II
G3 = loadMult*[-1 0;
    0 -1]; % excites pattern III
G4 = loadMult*[-1 0;
    0 -1.15]; % temporal pattern switching mode I/mode III (circles)
G5 = loadMult*[-1 0;
    0 -0.95]; % temporal pattern switching mode II/mode III (circles)
switch Mbctype
    case 1 % periodicity and prescribed deformation gradient
        G = G4;
        % Get two corner points
        Mid1 = find(pM(1,:)<-MSizeX+TOL_g & pM(2,:)<-MSizeY+TOL_g); % bottom left
        Mid2 = find(pM(1,:)>MSizeX-TOL_g & pM(2,:)<-MSizeY+TOL_g); % bottom right
        [~,Mid3] = min(abs(pM(1,:))+abs(pM(2,:))); % two points for RBM
        [~,Mid4] = min(abs(pM(1,:)-MSizeX/2)+abs(pM(2,:)));
        % Tying constraints for v0
        MIDGamma_1 = find(pM(2,:)<-MSizeY+TOL_g);
        MIDGamma_2 = find(pM(1,:)>MSizeX-TOL_g & pM(2,:)<MSizeY-TOL_g);
        MIDGamma_3 = find(pM(2,:)>MSizeY-TOL_g);
        MIDGamma_4 = find(pM(1,:)<-MSizeX+TOL_g & pM(2,:)<MSizeY-TOL_g);
        [~,Mids] = sort(pM(1,MIDGamma_1));
        MIDGamma_1 = MIDGamma_1(Mids);
        [~,Mids] = sort(pM(1,MIDGamma_3));
        MIDGamma_3 = MIDGamma_3(Mids);
        [~,Mids] = sort(pM(2,MIDGamma_2));
        MIDGamma_2 = MIDGamma_2(Mids);
        [~,Mids] = sort(pM(2,MIDGamma_4));
        MIDGamma_4 = MIDGamma_4(Mids);
        % Dependency matrix
        MC = zeros((2+nmodes)*(length(MIDGamma_1)+length(MIDGamma_2)),(2+nmodes)*Mnnode);
        % Dependencies for vertical boundary segments
        it = 1;
        for i = 1:length(MIDGamma_2)
            % v0 x direction
            MC(it,[2*MIDGamma_4(i)-1,2*MIDGamma_2(i)-1]) = [1,-1];
            it = it+1;
            % v0 y direction
            MC(it,[2*MIDGamma_4(i),2*MIDGamma_2(i)]) = [1,-1];
            it = it+1;
            % vi fields
            for j = 1:nmodes
                MC(it,[(1+j)*Mnnode+MIDGamma_4(i),(1+j)*Mnnode+MIDGamma_2(i)]) = [1,-1];
                it = it+1;
            end
        end
        % Dependencies for vertical boundary segments
        for i = 1:length(MIDGamma_3)
            % v0 x direction
            MC(it,[2*MIDGamma_1(i)-1,2*MIDGamma_3(i)-1]) = [1,-1];
            it = it+1;
            % v0 y direction
            MC(it,[2*MIDGamma_1(i),2*MIDGamma_3(i)]) = [1,-1];
            it = it+1;
            % v1 field
            for j = 1:nmodes
                MC(it,[(1+j)*Mnnode+MIDGamma_1(i),(1+j)*Mnnode+MIDGamma_3(i)]) = [1,-1];
                it = it+1;
            end
        end
        % Make MC sparse
        MC = sparse(MC);
        % Prescribe deformation
        xy = G*pM;
        MB = MC*[xy(:);zeros(nmodes*Mnnode,1)];
        DBCIndv0 = [2*Mid3-1;2*Mid3;2*Mid4]; % RBM implemented through equality constraints
        DBCValv0 = [xy(1,Mid3);xy(2,Mid3);xy(2,Mid4)];
        DBCIndvi = []; % no constraints for vi, only periodicity
        DBCValvi = [];
    case 2 % finite specimen, top and bottom horizontal edges fixed
        G = G1;
        MIDGamma = unique([MIDGamma_1;MIDGamma_3]);
        DBCIndvi = [];
        for i = 1:nmodes
            DBCIndvi = [DBCIndvi;(i+1)*Mnnode+MIDGamma];
        end
        DBCValvi = 0*DBCIndvi;
        xy = G*pM;
        DBCIndv0 = [2*MIDGamma-1;2*MIDGamma];
        DBCValv0 = [xy(1,MIDGamma)';xy(2,MIDGamma)'];
    case 3 % finite specimen, entire boundary fixed
        G = G1;
        MIDGamma = unique([MIDGamma_1;MIDGamma_2;MIDGamma_3;MIDGamma_4]);
        DBCIndvi = [];
        for i = 1:nmodes
            DBCIndvi = [DBCIndvi;(i+1)*Mnnode+MIDGamma];
        end
        DBCValvi = 0*DBCIndvi;
        xy = G*pM;
        DBCIndv0 = [2*MIDGamma-1;2*MIDGamma];
        DBCValv0 = [xy(1,MIDGamma)';xy(2,MIDGamma)'];
    case 4 % cruciform example for mode mixing
        MIDGamma = unique([MIDGamma_1;MIDGamma_2;MIDGamma_3;MIDGamma_4]);
        DBCIndvi = [];
        for i = 1:nmodes
            DBCIndvi = [DBCIndvi;(i+1)*Mnnode+MIDGamma];
        end
        DBCValvi = 0*DBCIndvi;
        DBCIndv0 = [2*MIDGamma_1-1;2*MIDGamma_1;
            2*MIDGamma_2-1;2*MIDGamma_2;
            2*MIDGamma_3-1;2*MIDGamma_3;
            2*MIDGamma_4-1;2*MIDGamma_4];
        uD = 2*STRETCH*MSizeX;
        DBCValv0 = [zeros(size(MIDGamma_1));-1*uD*ones(size(MIDGamma_1));
            uD*ones(size(MIDGamma_2));zeros(size(MIDGamma_2));
            zeros(size(MIDGamma_3));1*uD*ones(size(MIDGamma_3));
            -uD*ones(size(MIDGamma_4));zeros(size(MIDGamma_4))];
    otherwise
        error('Wrong M-bctype chosen.');
end
FreeIndv0 = setdiff(1:2*Mnnode,DBCIndv0)';
FreeIndvi = setdiff(2*Mnnode+1:(2+nmodes)*Mnnode,DBCIndvi)';

% Minimize the energy
% Solve the response in a time-stepping manner
Time = linspace(0,1,21);
R = zeros((2+nmodes)*Mnnode,length(Time));
DATA.U = zeros(length(DATA.p(:)),size(tM,2)*Mngauss);
F = zeros((2+nmodes)*Mnnode,length(Time));
Fdbc = zeros(length([DBCIndv0;DBCIndvi]),length(Time));
P22 = zeros(length(Time),1);
En = zeros(length(Time),1);
for i = 2:length(Time)
    t_step = tic;
    DATA.itimestep = 1; % store time increment data for stabilization
    DATA.Niter = 1; % store Newton iteration data for stabilization
    
    % Print time step message
    fprintf('\n\n\n==============================================\n');
    fprintf('Time step %g/%g\n',i,length(Time));
    fprintf('==============================================\n');
    
    % Minimize the ensemble averaged energy and get internal forces at the solution
    if Mbctype==1
        clear optionscon;
        optionscon = optimoptions('fmincon','algorithm','trust-region-reflective','Display',...
            'iter-detailed','MaxFunEvals',1e6,'MaxIter',1e4,'TolX',0,'TolFun',0,...
            'GradObj','on','Hessian','on','OutputFcn',...
            @(x,optimValues,state)outfun(x,optimValues,state,TOL_x,TOL_f));
        
        % Get initial guess
        tDBCValv0 = Time(i-1)*DBCValv0;
        dtDBCValv0 = (Time(i)-Time(i-1))*DBCValv0;
        tDBCValvi = Time(i-1)*DBCValvi;
        dtDBCValvi = (Time(i)-Time(i-1))*DBCValvi;
        tMB = Time(i)*MB;
        [~,f,H] = energy_micromorphic_2d(R([FreeIndv0;FreeIndvi],i-1),pM,tM,Mngauss,DBCIndv0,tDBCValv0,...
            FreeIndv0,DBCIndvi,tDBCValvi,FreeIndvi,maxNumThreads,mbctype,0,'s','full');
        EH = [H([FreeIndv0;FreeIndvi],[FreeIndv0;FreeIndvi]),MC(:,[FreeIndv0;FreeIndvi])'
            MC(:,[FreeIndv0;FreeIndvi]),sparse(size(MC,1),size(MC,1))];
        EG = [f([FreeIndv0;FreeIndvi])+H([FreeIndv0;FreeIndvi],[DBCIndv0;DBCIndvi])*[dtDBCValv0;dtDBCValvi];
            MC*R(:,i-1)-tMB];
        dxinit = -EH\EG;
        xinit = R([FreeIndv0;FreeIndvi],i-1) + dxinit(1:end-size(MC,1));
        
        % Add some perturbation to vi fields
        if norm(R(2*Mnnode:end,i-1))<0.1
            txinit = zeros((2+nmodes)*Mnnode,1);
            txinit([FreeIndv0;FreeIndvi]) = xinit;
            txinit(DBCIndv0) = tDBCValv0+dtDBCValv0; % dofs corresponding to v0
            txinit(DBCIndvi) = tDBCValvi+dtDBCValvi; % dofs corresponding to vi
            for j = 1:nmodes % :nmodes
                txinit((1+j)*Mnnode+1:(2+j)*Mnnode) = 0.1;
            end
            xinit = txinit([FreeIndv0;FreeIndvi]);
        end
        
        % Correct for the influence of w
        % Matlab's framework
        [xsol,res,exitFlag,output] = fmincon(@(x)energy_micromorphic_2d(x,...
            pM,tM,Mngauss,DBCIndv0,tDBCValv0+dtDBCValv0,FreeIndv0,DBCIndvi,...
            tDBCValvi+dtDBCValvi,FreeIndvi,maxNumThreads,mbctype,0,'s','free'),xinit,...
            [],[],MC(:,[FreeIndv0;FreeIndvi]),tMB,[],[],[],optionscon);
    else
        clear optionsun;
        optionsun = optimoptions('fminunc','algorithm','trust-region','Display',...
            'iter-detailed','MaxFunEvals',1e6,'MaxIter',1e4,'TolX',0,'TolFun',0,...
            'GradObj','on','Hessian','on','OutputFcn',...
            @(x,optimValues,state)outfun(x,optimValues,state,TOL_x,TOL_f));
        
        % Get initial guess
        tDBCValv0 = Time(i-1)*DBCValv0;
        dtDBCValv0 = (Time(i)-Time(i-1))*DBCValv0;
        tDBCValvi = Time(i-1)*DBCValvi;
        dtDBCValvi = (Time(i)-Time(i-1))*DBCValvi;
        [~,f,H] = energy_micromorphic_2d(R([FreeIndv0;FreeIndvi],i-1),pM,tM,Mngauss,DBCIndv0,tDBCValv0,...
            FreeIndv0,DBCIndvi,tDBCValvi,FreeIndvi,maxNumThreads,mbctype,0,'s','full');
        xinit = R([FreeIndv0;FreeIndvi],i-1)-H([FreeIndv0;FreeIndvi],[FreeIndv0;FreeIndvi])\...
            (f([FreeIndv0;FreeIndvi])+H([FreeIndv0;FreeIndvi],[DBCIndv0;DBCIndvi])*[dtDBCValv0;dtDBCValvi]);
        
        % Add some perturbation to vi fields
        if norm(R(2*Mnnode:end,i-1))<0.1
            txinit = zeros((2+nmodes)*Mnnode,1);
            txinit([FreeIndv0;FreeIndvi]) = xinit;
            txinit(DBCIndv0) = tDBCValv0+dtDBCValv0; % dofs corresponding to v0
            txinit(DBCIndvi) = tDBCValvi+dtDBCValvi; % dofs corresponding to vi
            for j = 1:nmodes
                % txinit((1+j)*Mnnode+1:(2+j)*Mnnode) = 0.1*0.5*(cos(2*pi*pM(2,:)/(2*MSizeY))+1);
                txinit((1+j)*Mnnode+1:(2+j)*Mnnode) = 1;
            end
            xinit = txinit([FreeIndv0;FreeIndvi]);
        end
        
        % Correct for the influence of w
        % Matlab's framework
        [xsol,res,exitFlag,output] = fminunc(@(x)energy_micromorphic_2d(x,...
            pM,tM,Mngauss,DBCIndv0,tDBCValv0+dtDBCValv0,FreeIndv0,DBCIndvi,...
            tDBCValvi+dtDBCValvi,FreeIndvi,maxNumThreads,mbctype,0,'s','free'),...
            xinit,optionsun);
    end
    [En(i),F(:,i)] = energy_micromorphic_2d(xsol,pM,tM,Mngauss,DBCIndv0,...
        tDBCValv0+dtDBCValv0,FreeIndv0,DBCIndvi,tDBCValvi+dtDBCValvi,FreeIndvi,...
        maxNumThreads,mbctype,0,'s','full');
    
    % Reconstruct the solution
    xr = zeros((2+nmodes)*Mnnode,1);
    xr([FreeIndv0;FreeIndvi]) = xsol;
    xr(DBCIndv0) = tDBCValv0+dtDBCValv0; % dofs corresponding to v0
    xr(DBCIndvi) = tDBCValvi+dtDBCValvi; % dofs corresponding to vi
    
    % Store some results
    R(:,i) = xr;
    P22(i) = sum(F(2*MIDGamma_3,i))/(2*MSizeX);
    fprintf('Increment computed in %g seconds\n',toc(t_step));
end

%% Plot the solution
idstep = length(Time);
plot_results_micromorphic_2d(R,idstep,nmodes,Melemtype,Mngauss,material,pM,...
    tM,Mnnode,250,TOL_g,maxNumThreads);

% Plot time evolutions of vi fields for the periodic setting
if Mbctype==1
    figure(134);clf;hold all;axis tight;box on;grid on;
    temp = R((1+1)*Mnnode+1:end,:);
    normConst = norm(temp(:),'inf');
    param = -Time*G(2,2); % parameter for all plots
    for i = 1:nmodes
        vi = abs(R((1+i)*Mnnode+1,:));
        if i==1
            plot(param,vi/normConst,'linewidth',1,'color',[0 0.45 0.75])
            plot(param(1:10:end),vi(1:10:end)/normConst,'+','color',[0 0.45 0.75],'linewidth',1);
        elseif i==2
            plot(param,vi/normConst,'linewidth',1,'color',[0.85 0.33 0.1])
            plot(param(1:10:end),vi(1:10:end)/normConst,'o','color',[0.85 0.33 0.1],'linewidth',1);
        elseif i==3
            plot(param,vi/normConst,'linewidth',1,'color',[0.93 0.69 0.13])
            plot(param(1:10:end),vi(1:10:end)/normConst,'.','color',[0.93 0.69 0.13],'linewidth',1,'markersize',10);
        end
        stringlegend{i} = ['$v_',num2str(i),'$'];
    end
    xlabel('$-\varepsilon_{22}$ [-]');ylabel('$\widehat{v}_i$ [-]');
    xlim([min(param),max(param)]);ylim([0,1]);
    % legend(stringlegend,'orientation','horizontal','location','north');
    % ylim([-0.5,6]);
    % ylim([-0.5,4]);
    % set(gcf,'Position',[1000,500,250,200]);
    % legend boxoff;
end

%% Save some data
% save('workspace','-v7.3');

fprintf('\n\n==============================================\nTotal time consumed %g\n',toc(time_total));
fprintf('==============================================\n\n');
