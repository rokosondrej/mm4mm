%% Clear workspace
clc; % clear command line
close all;
warning('on');
matlabrc; % restore MATLAB path, etc.
path(genpath([pwd,'/mfiles_mech']),path); % add path to mfiles folder containing all *.m files related to mechanics
path([pwd,'/mex'],path); % add path to mex folder containing all *.mex files
path([pwd,'/gmsh'],path); % add path to gmsh folder containing gmsh.exe and corresponding IO files

%% Specify inputs

% Specify maximum number of threads used in mex files by OpenMP
maxNumThreads = 1;

% Call mode of functions: 's' - silent mode, 'v' - verbose mode
callmode = 'v';

% RVE type (find further options inside get_rve_data function)
% 'rect_square' rectangular RVE, square packing, circles
% 'rect_hexa' rectangular RVE, hexagonal packing, circles/hexagons
% 'skew_hexa' skewed RVE, hexagonal packing, circles/hexagons
% 'hexa_hexa' hexagonal RVE, hexagonal packing, circles/hexagons
rvetype = 'rect_square';
inclusiontype = 'circle'; % 'circle', 'hexagon'
CellSize = 1; % size of the primitive cell
if strcmp(rvetype,'rect_square')
    DiamOuter = 0.85;
    DiamInner = DiamOuter;
    mhmax = CellSize/5; % CellSize/10
else
    DiamOuter = CellSize*1.6; % diameter of the outer circle
    if strcmp(inclusiontype,'hexagon')
        const = 1-0.1*tan(pi/6); % hexagon, for w/l = 0.1
        DiamInner = const*DiamOuter; % diameter of the inner circle
        mhmax = (DiamOuter-DiamInner)/2; % maximum element's size
    elseif strcmp(inclusiontype,'circle')
        const = 0.8; % circle
        DiamInner = const*DiamOuter; % diameter of the inner circle
        mhmax = (DiamOuter-DiamInner)/2; % maximum element's size
    end
end

% Type of the long-range correlated mode
% 'analytic' (analytical approximation)
% 'linbuckling' (linear buckling analysis)
% 'truebuckling' (buckling analysis at the bifurcation point)
% 'deformed' (deformed shape solved by Newton)
modetype = 'analytic';

% Choose mode shape between pattern and mode (applies only for modetype = 'deformed')
modeshape = 'mode'; % 'pattern', 'mode'

% Material = [SW,m1,m2,m3,m4,m5,kappa,thickness], C = F'*F, I1 = trace(C), I2 = 0.5*(tr(C)^2-tr(C^2)), J = det(F)
% SW = 0: no material (hole)
% SW = 1: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+1/2*kappa(ln(J))^2 (OOFEM)
% SW = 2: W(F) = m1(I1-3)+m2(I1-3)^2-2m1*ln(J)+1/2*kappa(J-1)^2 (Bertoldi, Boyce)
% SW = 3: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+m3(I1/J^{2/3}-3)*(I2/J^{4/3}-3)+m4(I1/J^{2/3}-3)^2+m5(I1/J^{2/3}-3)^3+9/2*kappa(J^{1/3}-1)^2 (Jamus, Green, Simpson)
% SW = 4: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+m3(I1/J^{2/3}-3)*(I2/J^{4/3}-3)+m4(I1/J^{2/3}-3)^2+m5(I2/J^{2/3}-3)^2+kappa*(J-1)^2 (five-term Mooney-Rivlin)
% SW = 5: W(F) = 0.5*(0.5*(C-I)*(m1*)*0.5*(C-I)) (linear elastic material)
material = [2,0.55e3,0.3e3,0,0,0,55e3,1 % for the surrounding matrix
    0,1e-6,0,0,0,0,1e-6,1]; % for circular inclusions

% Choose m-element type
melemtype = 9; % 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9, 36 = Q16

% Choose m-Gauss integration rule (the mesh is homogeneous and the same integration rule for all elements is used)
% triangles: ngauss = 1, 3 (interior), -3 (midedge), 4, 6, 7
% quadrangles: ngauss = 1, 4, 9, 16, 25
switch melemtype
    case 2 % linear triangle
        mngauss = 1;
    case 3 % four node quadrangle
        mngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 9 % quadratic triangle
        mngauss = 3;
    case 16 % eight node quadrangle
        mngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 10 % nine node quadrangle
        mngauss = 3 * 3; % product of two one-dimensional Gauss integration rules
    case 21 % cubic triangle
        mngauss = 6;
    case 36 % cubic quadrangle
        mngauss = 4 * 4;
    otherwise
        error('Wrong element type chosen.');
end

% Tolerances
TOL_x = 1e-3; % M-minimization algorithm tolerance (dofs)
TOL_f = 1e-3; % M-minimization algorithm tolerance (gradient)
TOL_r = 1e-3; % m-minimization algorithm tolerance (gradient)
MaxNiter = 30; % maximum number of m-Newton interations
TOL_g = 1e-10; % geometric tolerance; distance < TOL_g is treated as zero

%% Get RVE data
time_total = tic;
global DATA; % make DATA global, so its contents can be adjusted inside fminunc during iterative calls
nmodes = get_rve_data(CellSize,rvetype,inclusiontype,DiamInner,DiamOuter,...
    mhmax,modetype,modeshape,material,melemtype,mngauss,MaxNiter,TOL_g,TOL_r,...
    maxNumThreads,callmode);

%% Get macroscopic discretization and boundary conditions
% Specify inputs for M-solver
% Mbctype = 1: global local buckling
% Mbctype = 2: cruciform example
Mbctype = 1;
mbctype = 'pbc'; % 'closed', 'now', 'pbc', 'dbc'
Melemtype = 9; % 2 = T3, 9 = T6, 3 = Q4, 16 = Q8
switch Melemtype % should correspond to mass matrix integration rule
    case 2 % linear triangle
        Mngauss = 1;
    case 3 % four node quadrangle
        Mngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 9 % quadratic triangle
        Mngauss = 6;
    case 16 % eight node quadrangle
        Mngauss = 3 * 3; % product of two one-dimensional Gauss integration rules
    otherwise
        error('Wrong M-element type chosen.');
end

% Get BCs and other constraints
switch Mbctype
    case 1 % global local buckling
        Mhmax = 2*CellSize; % 1*CellSize % maximum element's size at the macroscale
        MSizeX = 6*CellSize/2;
        MSizeY = 14*CellSize/2; % 14*CellSize/2
        % [pM,tM] = init_rtin(MSizeX,MSizeY,MSizeY/2048,2*Mhmax,TOL_g,material,...
        %     callmode,maxNumThreads);
        [pM,tM] = init_gmsh_gdic(MSizeX,MSizeY,Mhmax,Melemtype,Mngauss,material,...
            TOL_g,callmode,maxNumThreads);
        % [pM,tM] = init_gmsh_mve_opencascade(MSizeX,MSizeY,[0;0],...
        %     [],Mhmax,material,Melemtype,Mngauss,TOL_g,callmode,maxNumThreads);
        
        % Get boundary segments
        MIDGamma_1 = find(pM(2,:)<-MSizeY+TOL_g)';
        MIDGamma_2 = find(pM(1,:)>MSizeX-TOL_g & abs(pM(2,:))<MSizeY-TOL_g)';
        MIDGamma_3 = find(pM(2,:)>MSizeY-TOL_g)';
        MIDGamma_4 = find(pM(1,:)<-MSizeX+TOL_g & abs(pM(2,:))<MSizeY-TOL_g)';
        Mnnode = size(pM,2);
        
        % Finite specimen, top and bottom horizontal edges fixed
        G = [0 0;
            0 -0.1];
        MIDGamma = unique([MIDGamma_1;MIDGamma_3]);
        DBCIndvi = [];
        for i = 1:nmodes
            DBCIndvi = [DBCIndvi;(i+1)*Mnnode+MIDGamma];
        end
        DBCValvi = 0*DBCIndvi;
        xy = G*pM;
        DBCIndv0 = [2*MIDGamma-1;2*MIDGamma];
        DBCValv0 = [xy(1,MIDGamma)';xy(2,MIDGamma)'];
    case 2 % cruciform example
        STRETCH = -0.1;
        Mhmax = 10*CellSize; % maximum element's size at the macroscale
        if strcmp(rvetype,'rect_hexa') || strcmp(rvetype,'skew_hexa') || strcmp(rvetype,'hexa_hexa') % hexagonal stacking
            MSizeX = 20*sqrt(3)*CellSize; % half size of the x-domain
            MSizeY = 20*sqrt(3)*CellSize; % half size of the y-domain
        elseif strcmp(rvetype,'rect_square')
            MSizeX = 20*2*CellSize; % half size of the x-domain
            MSizeY = 20*2*CellSize; % half size of the y-domain
        end
        % [pM,tM] = init_gmsh_mode_mixing(MSizeX,MSizeY,[],Mhmax,material,...
        %     Melemtype,Mngauss,TOL_g,'circle',callmode,maxNumThreads);
        [pM,tM] = init_gmsh_mm_smoothcruciform(MSizeX,MSizeY,[],Mhmax,material,...
            Melemtype,Mngauss,TOL_g,'circle',callmode,maxNumThreads);
        
        % Get boundary segments
        MIDGamma_1 = find(pM(2,:)<min(pM(2,:))+TOL_g)';
        MIDGamma_2 = find(pM(1,:)>max(pM(1,:))-TOL_g)';
        MIDGamma_3 = find(pM(2,:)>max(pM(2,:))-TOL_g)';
        MIDGamma_4 = find(pM(1,:)<min(pM(1,:))+TOL_g)';
        Mnnode = size(pM,2);
        
        % Cruciform specimen, top, bottom, and both side edges fixed
        MIDGamma = unique([MIDGamma_1;MIDGamma_2;MIDGamma_3;MIDGamma_4]);
        DBCIndvi = [];
        for i = 1:nmodes
            DBCIndvi = [DBCIndvi;(i+1)*Mnnode+MIDGamma];
        end
        DBCValvi = 0*DBCIndvi;
        DBCIndv0 = [2*MIDGamma_1-1;2*MIDGamma_1;
            2*MIDGamma_2-1;2*MIDGamma_2;
            2*MIDGamma_3-1;2*MIDGamma_3;
            2*MIDGamma_4-1;2*MIDGamma_4];
        uD = 2*STRETCH*MSizeX;
        DBCValv0 = [zeros(size(MIDGamma_1));-1*uD*ones(size(MIDGamma_1));
            uD*ones(size(MIDGamma_2));zeros(size(MIDGamma_2));
            zeros(size(MIDGamma_3));1*uD*ones(size(MIDGamma_3));
            -uD*ones(size(MIDGamma_4));zeros(size(MIDGamma_4))];
    otherwise
        error('Wrong M-bctype chosen.');
end
FreeIndv0 = setdiff(1:2*Mnnode,DBCIndv0)';
FreeIndvi = setdiff(2*Mnnode+1:(2+nmodes)*Mnnode,DBCIndvi)';

% USE NEWTON SOLVER AND TIME INCREMENTATION
fprintf('Solving mechanics...\n'),t_start_1 = tic;
fprintf('%d step, %d Nwtn it., Time %g, %g s\n',1,0,0,0);
% Solve for the evolution process of the system
nTimeSteps = 51;
Time = zeros(1,3);
timeIter = zeros(size(Time));
R = zeros((2+nmodes)*Mnnode,length(Time));
DATA.U = zeros(length(DATA.p(:)),size(tM,2)*Mngauss);
DATA.Astab = zeros(4,4,size(tM,2)*Mngauss); % stabilization constitutive stiffnesses
DATA.Cstab = zeros(2*nmodes,2*nmodes,size(tM,2)*Mngauss);
F = zeros((2+nmodes)*Mnnode,length(Time));
Fdbc = zeros(length([DBCIndv0;DBCIndvi]),length(Time));
P22 = zeros(length(Time),1);
En = zeros(length(Time),1);
Nbifur = 0; % number of bifurcation trials
time = 0;
alldtimes = 1/nTimeSteps*ones(size(Time));
i = 2;
skip = 0;
while time<=1
    DATA.itimestep = i; % store time increment data for stabilization
    
    % Run time iteration
    t_start_2 = tic;
    success = 0;
    if i==2
        dtime = 2*alldtimes(i-1);
    else
        if Ntrials==1
            dtime = 2*1.25*alldtimes(i-1); % increase the time step if everything went well previously
            dtime = min(dtime,2*alldtimes(1));
        else
            dtime = 2*alldtimes(i-1); % else take the previous time step
        end
    end
    Ntrials = 0;
    while ~success
        Ntrials = Ntrials+1;
        % Half the time increment
        dtime = dtime/2;
        if dtime<1e-6
            fprintf('\n\n\n');
            warning('Step halving failed, dtime < TOL. Continue.');
            fprintf('\n\n\n');
            skip = 1;
            break;
        end
        % Get boundary conditions
        tDBCValv0 = time*DBCValv0;
        dtDBCValv0 = dtime*DBCValv0;
        tDBCValvi = time*DBCValvi;
        dtDBCValvi = dtime*DBCValvi;
        % SOLVE FOR u
        [xsol,Niter,minDiag,status1] = solve_r_micromorphic(pM,tM,Mngauss,...
            R([FreeIndv0;FreeIndvi],i-1),DBCIndv0,tDBCValv0,dtDBCValv0,[],FreeIndv0,DBCIndvi,...
            tDBCValvi,dtDBCValvi,FreeIndvi,maxNumThreads,mbctype,TOL_r,MaxNiter,'s','free');
        % Reconstruct the solution
        xr = zeros((2+nmodes)*Mnnode,1);
        xr([FreeIndv0;FreeIndvi]) = xsol;
        xr(DBCIndv0) = tDBCValv0+dtDBCValv0; % dofs corresponding to v0
        xr(DBCIndvi) = tDBCValvi+dtDBCValvi; % dofs corresponding to vi
        % Store potentially correct converged minimizing values
        R(:,i) = xr;
        if status1==1
            success = 1;
        end
        % Test bifurcation
        if minDiag<0 && success==1
            % Get the lowest eigenvalue
            fprintf('Perform bifurcation analysis...\n');
            t_bif = tic;
            [~,~,K1] = energy_micromorphic_2d(R([FreeIndv0;FreeIndvi],i),pM,tM,...
                Mngauss,DBCIndv0,tDBCValv0+dtDBCValv0,FreeIndv0,DBCIndvi,tDBCValvi+dtDBCValvi,...
                FreeIndvi,maxNumThreads,mbctype,0,'s','free');
            [V,D] = eigs(K1,5,'sm');
            [~,id] = min(diag(D));
            phi = V(:,id);
            % Reconstruct buckling mode
            v = zeros((2+nmodes)*Mnnode,1);
            v([FreeIndv0;FreeIndvi]) = phi;
            if strcmp(callmode,'v')
                plot_results_micromorphic_2d(v,1,nmodes,Melemtype,Mngauss,...
                    material,pM,tM,Mnnode,1000,TOL_g,maxNumThreads);
            end
            fprintf('eig1 = %g\n',D(id,id));
            phi = 1e-1*CellSize/max(abs(phi))*phi;
            % If the eigenvalue is negative
            if D(id,id)<TOL_g
                fprintf('Unstable configuration encountered. Updating...\n');
                % Solve for u again, try multiple times
                jbif = 1;
                while minDiag<0 && jbif<11
                    tphi = (jbif*(jbif-1)+1)*phi;
                    [xsol,Niter,minDiag,status2] = solve_r_micromorphic(pM,tM,Mngauss,...
                        R([FreeIndv0;FreeIndvi],i)+tphi,DBCIndv0,tDBCValv0+dtDBCValv0,0*DBCValv0,[],FreeIndv0,DBCIndvi,...
                        tDBCValvi+dtDBCValvi,0*DBCValvi,FreeIndvi,maxNumThreads,mbctype,TOL_r,MaxNiter,'s','free');
                    jbif = jbif+1;
                    if ~status2
                        break; % if solver fails, continue right away
                    end
                end
                % Reconstruct the solution
                xr = zeros((2+nmodes)*Mnnode,1);
                xr([FreeIndv0;FreeIndvi]) = xsol;
                xr(DBCIndv0) = tDBCValv0+dtDBCValv0; % dofs corresponding to v0
                xr(DBCIndvi) = tDBCValvi+dtDBCValvi; % dofs corresponding to vi
                % Rewrite the minimizing value (could change due to bifurcation)
                R(:,i) = xr;
                success = 1;
                if jbif>=11
                    fprintf('jbif >= 11.\n');
                    success = 0;
                end
                if status2==0
                    fprintf('Perturbation failed.\n');
                    success = 0;
                end
            end
            fprintf('Time consumed %g s\n',toc(t_bif));
        end
        if success==0
            fprintf('Halve the time step.\n');
        end
    end
    if skip==1
        break;
    end
    
    % PRINT ITERATION MESSAGE
    fprintf('%d step, %d Nwtn it., Time %g, %g s\n',i,Niter,time,toc(t_start_2));
    timeIter(i) = Niter;
    if strcmp(callmode,'v')
        % Plot system's current configuration
        handle = figure(3);clf,hold all,axis equal,box on;
        set(handle,'name','Deformed configuration','numbertitle','off');
        r = reshape(pM(:)+R(1:2*Mnnode,i),size(pM));
        plot_mesh(handle,r,tM,Melemtype,[0,0,1]);
        plot(r(1:2:end),r(2:2:end),'.k');
        drawnow;
    end
    
    % Sample energy and forces
    [En(i),F(:,i)] = energy_micromorphic_2d(R([FreeIndv0;FreeIndvi],i),pM,tM,Mngauss,DBCIndv0,...
        tDBCValv0+dtDBCValv0,FreeIndv0,DBCIndvi,tDBCValvi+dtDBCValvi,FreeIndvi,...
        maxNumThreads,mbctype,0,'s','full');
    P22(i) = sum(F(2*MIDGamma_3,i))/(2*MSizeX);
    
    % Save time and proceed to the next time step
    time = time+dtime;
    Time(i) = time;
    alldtimes(i) = dtime;
    if Ntrials==1
        dtime = 2*1.5*alldtimes(i-1); % increase the time step if everything went well
        dtime = min(dtime,2*alldtimes(1));
    else
        dtime = 2*alldtimes(i-1); % else take the previous time increment
    end
    if time+dtime>1
        dtime = 2*(1-time);
    end
    i = i+1;
end
fprintf('%d step, %d Nwtn it., Time %g, %g s\n',i,Niter,time,toc(t_start_2));

% Print solution statistics
fprintf('E[Niter] %g\n',mean(timeIter));
fprintf('Max[Niter] %g\n',max(timeIter));
fprintf('Time consumed %g s\n\n',toc(t_start_1));

%% Plot the solution
idstep = length(Time);
plot_results_micromorphic_2d(R,idstep,nmodes,Melemtype,Mngauss,material,pM,...
    tM,Mnnode,1000,TOL_g,maxNumThreads);

%% Save workspace
% fprintf('Saving workspace...');t_save = tic;
% save('workspace','-v7.3');
% fprintf('in %g s\n',toc(t_save));

fprintf('\n\n==============================================\nTotal time consumed %g\n',toc(time_total));
fprintf('==============================================\n\n');
