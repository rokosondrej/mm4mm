function ParaView_export_2d(fileName,Time,p,t,elemtype,ngauss,U,material,...
    TOL_g,maxNumThreads,SW)

fprintf('Exporting to vtk... ');t_export = tic;
if ~exist('ParaView','dir')
    mkdir('ParaView');
end
for timeStep = 1:length(Time)
    fin = fopen(['ParaView/',fileName,'_',num2str(timeStep,'%.3d'),'.vtu'],'w');
    
    % Beginning of the file
    fprintf(fin,'<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n');
    fprintf(fin,'<UnstructuredGrid>\n');
    
    % Write points
    fprintf(fin,'<Piece NumberOfPoints="%d" NumberOfCells="%d">\n',size(p,2),size(t,2));
    fprintf(fin,'<Points>\n');
    fprintf(fin,' <DataArray type="Float64" NumberOfComponents="3" format="ascii"> ');
    fprintf(fin,'%e %e %e ',[p;zeros(1,size(p,2))]);
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</Points>\n');
    
    % Write cells
    fprintf(fin,'<Cells>\n');
    fprintf(fin,' <DataArray type="Int32" Name="connectivity" format="ascii"> ');
    switch elemtype
        case 2 % linear triangle
            fprintf(fin,'%d %d %d ',(t(1:end-1,:)-1)); % 0-offset indices
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="Int32" Name="offsets" format="ascii"> ');
            fprintf(fin,'%d ',3:3:3*size(t,2));
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="UInt8" Name="types" format="ascii"> ');
            fprintf(fin,'%d ',5*ones(1,size(t,2)));
        case 3 % four node quadrangle
            fprintf(fin,'%d %d %d %d ',(t(1:end-1,:)-1)); % 0-offset indices
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="Int32" Name="offsets" format="ascii"> ');
            fprintf(fin,'%d ',4:4:4*size(t,2));
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="UInt8" Name="types" format="ascii"> ');
            fprintf(fin,'%d ',9*ones(1,size(t,2)));
        case 9 % quadratic triangle
            fprintf(fin,'%d %d %d %d %d %d ',(t(1:end-1,:)-1)); % 0-offset indices
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="Int32" Name="offsets" format="ascii"> ');
            fprintf(fin,'%d ',6:6:6*size(t,2));
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="UInt8" Name="types" format="ascii"> ');
            fprintf(fin,'%d ',22*ones(1,size(t,2)));
        case 16 % eight node quadrangle
            fprintf(fin,'%d %d %d %d %d %d %d %d ',(t(1:end-1,:)-1)); % 0-offset indices
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="Int32" Name="offsets" format="ascii"> ');
            fprintf(fin,'%d ',8:8:8*size(t,2));
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="UInt8" Name="types" format="ascii"> ');
            fprintf(fin,'%d ',23*ones(1,size(t,2)));
        case 10 % nine node quadrangle (vtk write as 8-node quadrangle)
            fprintf(fin,'%d %d %d %d %d %d %d %d ',(t(1:8,:)-1)); % 0-offset indices
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="Int32" Name="offsets" format="ascii"> ');
            fprintf(fin,'%d ',8:8:8*size(t,2));
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="UInt8" Name="types" format="ascii"> ');
            fprintf(fin,'%d ',23*ones(1,size(t,2)));
        case 21 % cubic triangle (vtk write as 3-node triangles)
            fprintf(fin,'%d %d %d ',(t(1:3,:)-1)); % 0-offset indices
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="Int32" Name="offsets" format="ascii"> ');
            fprintf(fin,'%d ',3:3:3*size(t,2));
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="UInt8" Name="types" format="ascii"> ');
            fprintf(fin,'%d ',5*ones(1,size(t,2)));
        case 36 % cubic quadrangle (vtk write as 4-node quadrangle)
            fprintf(fin,'%d %d %d %d ',(t(1:4,:)-1)); % 0-offset indices
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="Int32" Name="offsets" format="ascii"> ');
            fprintf(fin,'%d ',4:4:4*size(t,2));
            fprintf(fin,'</DataArray>\n');
            fprintf(fin,' <DataArray type="UInt8" Name="types" format="ascii"> ');
            fprintf(fin,'%d ',9*ones(1,size(t,2)));
    end
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</Cells>\n');
    
    % Write point data
    fprintf(fin,'<PointData Scalars="" Vectors="" Tensors="">\n');
    fprintf(fin,' <DataArray type="Float64" Name="DisplacementVector" NumberOfComponents="3" format="ascii"> ');
    fprintf(fin,'%e %e %e ',[U(1:2:end,timeStep)';U(2:2:end,timeStep)';0*U(1:2:end,timeStep)']);
    fprintf(fin,'</DataArray>\n');
    fprintf(fin,'</PointData>\n');
    
    % Write cell data
    if SW==1
        [~,~,E,S,~,~] = sample_UES_2d(p,t,material,ngauss,U(:,timeStep),[],[],...
            TOL_g,1,maxNumThreads);
        % Get det(F) = sqrt(det(C))
        C = [2*E(1,:)+1;2*E(2,:)+1;E(3,:)];
        detF = (C(1,:).*C(2,:)-C(3,:).^2).^0.5;
        % Write data
        fprintf(fin,'<CellData Scalars="" Vectors="" Tensors="">\n');
        fprintf(fin,' <DataArray type="Float64" Name="material" NumberOfComponents="1" format="ascii"> ');
        fprintf(fin,'%e ',t(end,:));
        fprintf(fin,'</DataArray>\n');
        fprintf(fin,' <DataArray type="Float64" Name="S" NumberOfComponents="3" format="ascii"> ');
        fprintf(fin,'%e %e %e ',S);
        fprintf(fin,'</DataArray>\n');
        fprintf(fin,' <DataArray type="Float64" Name="E" NumberOfComponents="3" format="ascii"> ');
        fprintf(fin,'%e %e %e ',E);
        fprintf(fin,'</DataArray>\n');
        fprintf(fin,' <DataArray type="Float64" Name="J" NumberOfComponents="1" format="ascii"> ');
        fprintf(fin,'%e ',detF);
        fprintf(fin,'</DataArray>\n');
        fprintf(fin,'</CellData>\n');
    elseif SW==0
        fprintf(fin,'<CellData Scalars="" Vectors="" Tensors="">\n');
        fprintf(fin,' <DataArray type="Float64" Name="material" NumberOfComponents="1" format="ascii"> ');
        fprintf(fin,'%e ',t(end,:));
        fprintf(fin,'</DataArray>\n');
        fprintf(fin,'</CellData>\n');
    end
    
    % Close the VTK file
    fprintf(fin,'</Piece>\n');
    fprintf(fin,'</UnstructuredGrid>\n');
    fprintf(fin,'</VTKFile>');
    
    % Close the file
    fclose(fin);
end
fprintf('in %g s\n',toc(t_export));

end