function stop = outfun(x,optimValues,state,TOL_x,TOL_f)
stop = false;

switch state
    case 'init'
        % Do nothing
        
    case 'iter'
        % Check convergence criteria
        if ~isempty(optimValues.stepsize)
            if optimValues.stepsize<TOL_x && optimValues.firstorderopt<TOL_f ||... % normal convergence situation
                    optimValues.stepsize<1e-13 || optimValues.firstorderopt<1e-13 % exceptional situation
                stop = true;
            end
        end
        
    case 'done'
        % Do nothing
        
    otherwise
        % Do nothing
        
end
end
