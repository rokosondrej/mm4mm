function [u2,Niter,minDiag,status] = solve_rC_micromorphic(pM,tM,Mngauss,u1,...
    tDBCValues,dtDBCValues,FreeIndices,DBCIndices,DepIndices,IndIndices,FIIndices,DBC2Ind,...
    MC,maxNumThreads,mbctype,TOL_r,nmodes,Mnnode,maxMiter,callmode)

% Solve for u2, a vector of free degrees of freedom, using standard constrained Newton algorithm
u2 = u1;
status = 1;
eps_M = 1;
Niter = 0;
alpha = 1;
while eps_M > TOL_r
    Niter = Niter + 1;
    
    % Sample energy, gradient, and Hessian
    xr = zeros((2+nmodes)*Mnnode,1);
    if Niter == 1
        % Get configuration
        xr(DBCIndices) = tDBCValues;
        xr(FreeIndices) = u2;
        xr(DepIndices) = MC(:,IndIndices)*xr(IndIndices);
        % Get gradient and Hessian
        [~,f,K] = energy_micromorphic_2d(xr,pM,tM,Mngauss,[],[],...
            (1:2*Mnnode)',[],[],(2*Mnnode+1:(2+nmodes)*Mnnode)',maxNumThreads,mbctype,0,callmode,'full');
        % Condense out M-periodicity
        f_r = f(IndIndices) + MC(:,IndIndices)'*f(DepIndices);
        K_r = K(IndIndices,IndIndices)+K(IndIndices,DepIndices)*MC(:,IndIndices)+...
            MC(:,IndIndices)'*K(DepIndices,IndIndices)+...
            MC(:,IndIndices)'*K(DepIndices,DepIndices)*MC(:,IndIndices);
        K_rDBC = K_r(FIIndices,DBC2Ind);
        K_r = K_r(FIIndices,FIIndices);
        f_r = f_r(FIIndices);
        % Extended forces
        Ef = f_r+K_rDBC*dtDBCValues;
    else
        % Get configuration
        xr(DBCIndices) = tDBCValues+dtDBCValues;
        xr(FreeIndices) = u2;
        xr(DepIndices) = MC(:,IndIndices)*xr(IndIndices);
        % Get gradient and Hessian
        [~,f,K] = energy_micromorphic_2d(xr,pM,tM,Mngauss,[],[],...
            (1:2*Mnnode)',[],[],(2*Mnnode+1:(2+nmodes)*Mnnode)',maxNumThreads,mbctype,0,callmode,'full');
        % Condense out M-periodicity
        f_r = f(IndIndices) + MC(:,IndIndices)'*f(DepIndices);
        K_r = K(IndIndices,IndIndices)+K(IndIndices,DepIndices)*MC(:,IndIndices)+...
            MC(:,IndIndices)'*K(DepIndices,IndIndices)+...
            MC(:,IndIndices)'*K(DepIndices,DepIndices)*MC(:,IndIndices);
        K_r = K_r(FIIndices,FIIndices);
        Ef = f_r(FIIndices);
    end
    
    % Compute increment using LDL'
    [L,D,P] = ldl(K_r,'vector');
    minDiag = min(full(diag(D)));
    duM = 0*Ef;
    duM(P) = -(L'\(D\(L\Ef(P))));
    u2 = u2 + alpha*duM;
    
    % Update error
    eps_M = norm(Ef,'inf');
    if norm(u2-u1)>0
        eps_M = eps_M + norm(duM,'inf')/norm(u2-u1);
    end
    
    % Check the number of iterations
    if Niter>maxMiter
        % warning('Max number of Newton iterations exceeded, stop.');
        status = 0; % false for fail
        minDiag = 1;
        u2 = u1;
        break;
    end
    
    % Check too large increments
    if norm(alpha*duM)>norm(pM(:)) || isnan(eps_M)
        u2 = zeros(size(u1));
        status = 0;
        minDiag = 1;
        disp('Too large increment encountered. halve time step.')
        return;
    end
end
