function [u2,Niter,lambda,minDiag,status] = solve_arc(p,t,material,ngauss,u1,...
    DBCIndices,DBCValues,FreeIndices,phi,TOL_r,larc1,lambda1,MaxNiter,solver,...
    sgn,maxNumThreads)

% Solve for x, a vector of free degrees of freedom, using standard arc-length algorithm
status = 1; % true for success
x = u1(FreeIndices);
x0 = x;
lambda = lambda1;
compute = 1;
larc = larc1;
eps_r = 1+TOL_r;
alpha = 1;
Niter = 0;
switch solver
    case 'twostep'
        while eps_r > TOL_r
            if compute
                Niter = Niter+1; % count only those iterations that require system update
                [~,f_r,K_r,K_rDBC] = grad_hess(p,t,material,ngauss,x,DBCIndices,...
                    lambda*DBCValues,FreeIndices,maxNumThreads);
                
                % Compute individual increments using LDL'
                [L,D,P] = ldl(K_r,'vector');
                diagD = diag(D);
                minDiag = full(min(diagD));
                if min(abs(diagD))/max(abs(diagD))<1e-14
                    id = find(abs(diagD)/max(abs(diagD))<1e-14);
                    diagD(id) = diagD(id)+TOL_r; % stabilize whenever necessary
                    D = spdiags(diagD,0,length(diagD),length(diagD));
                    fprintf('regularizing the Hessian...\n');
                end
                lhs = 0*[x,x];
                f_r1 = K_rDBC*DBCValues;
                lhs(P,:) = -(L'\(D\(L\([f_r(P),f_r1(P)]))));
            end
            dxt = alpha*lhs(:,1);
            dxF = lhs(:,2);
            
            % Parameters for the arc-length method
            a = dxF'*dxF;
            b = 2*(x+dxt-u1(FreeIndices))'*dxF;
            c = (x+dxt-u1(FreeIndices))'*(x+dxt-u1(FreeIndices))-larc^2;
            dlambda1 = (-b+sqrt(b^2-4*a*c))/(2*a);
            dlambda2 = (-b-sqrt(b^2-4*a*c))/(2*a);
            
            % Choose increment direction
            cth1 = sgn*dxF'*(dxt+dlambda1*dxF)/(norm(dxF)*norm(dxt+dlambda1*dxF));
            cth2 = sgn*dxF'*(dxt+dlambda2*dxF)/(norm(dxF)*norm(dxt+dlambda2*dxF));
            if cth1>cth2
                dlambda = real(dlambda1);
            else
                dlambda = real(dlambda2);
            end
            if ~isreal(dlambda1) || ~isreal(dlambda2)
                if alpha>1e-3
                    alpha = alpha/2;
                    compute = 0; % do not update increments, only change alpha
                    fprintf('Arc-length complex root, alpha = %g.\n',alpha);
                else
                    fprintf('alpha < 1e-3. Stop.\n');
                    x = x0;
                    lambda = lambda1;
                    status = 0;
                    break;
                end
            else
                % When real root found, try to increase alpha again
                alpha = min(2*alpha,1);
                compute = 1; % update increments
                if alpha~=1
                    fprintf('Arc-length complex root, alpha = %g.\n',alpha);
                end
                
                % Construct the increment
                dx = dxt+dlambda*dxF;
                lambda = lambda+dlambda;
                x = x+dx;
                
                % Update the error
                eps_r = norm(f_r) + norm(dx)/norm(x-u1(FreeIndices));
            end
            
            % Check the number of iterations
            if Niter>=MaxNiter
                % disp('Max number of Newton iterations exceeded, stop.');
                status = 0; % false for fail
                lambda = lambda1;
                x = x0;
                break;
            end
        end
        
    case 'onestep'
        while eps_r > TOL_r
            Niter = Niter+1;
            [~,f_r,K_r,K_rDBC] = grad_hess(p,t,material,ngauss,x,DBCIndices,...
                lambda*DBCValues,FreeIndices,maxNumThreads);
            
            % The predictor step
            if Niter == 1
                [L,D,P] = ldl(K_r,'vector');
                f_r1 = K_rDBC*DBCValues;
                dx = 0*x;
                dx(P) = -L'\(D\(L\f_r1(P)));
                lambda = lambda+sgn*larc/norm(dx);
                dx = sgn*larc*dx/norm(dx);
                x = x+dx;
            else
                dx = zeros(length(du)-1,1);
            end
            
            % Get conditions and derivatives
            F = 0*u1; % no External forces
            g = (x-u1(FreeIndices))'*(x-u1(FreeIndices))-larc^2;
            h = 2*(x-u1(FreeIndices));
            f = K_rDBC*DBCValues-F(FreeIndices);
            w = 0;
            
            % Solve the extended system
            solvertype = 'sm';
            switch solvertype
                case 'direct'
                    % Use directly non-symmetric Jacobian
                    Hu = [K_r f
                        h' w];
                    fu = [f_r-lambda*F(FreeIndices);g];
                    du = -Hu\fu;
                case 'sm'
                    % Use the Sherman--Morrison formula (assume homogeneous DBCs)
                    lhs = -K_r\[f_r-lambda*F(FreeIndices),K_rDBC*DBCValues-F(FreeIndices)];
                    dI = lhs(:,1);
                    dII = lhs(:,2);
                    du = [dI;g]-1/(h'*dII-w)*[(h'*dI+g)*dII;h'*dI+g*(1+h'*dII-w)];
                otherwise
                    error('Wrong solvertype chosen.');
            end
            
            % Update system state
            lambda = lambda+du(end);
            x = x+du(1:end-1);
            
            % Update the error
            eps_r = norm(f_r) + norm(dx)/norm(x-u1(FreeIndices)) + abs(g)/larc;
            
            % Check the number of iterations
            if Niter>=MaxNiter
                fprintf('Max number of Newton iterations exceeded, stop.\n');
                x = x0;
                lambda = lambda1;
                status = 0;
                break;
            end
            
            % Check too large increments
            if norm(dx)>norm(p(:)) || isnan(eps_r)
                x = x0;
                status = 0;
                lambda = lambda1;
                break;
            end
        end
        [~,~,K_r,~] = grad_hess(p,t,material,ngauss,x,DBCIndices,...
            lambda*DBCValues,FreeIndices,maxNumThreads);
        [~,D,~] = ldl(K_r,'vector');
        minDiag = full(min(diag(D)));
end

% Reconstruct converged u2-vector from x
u2 = zeros(size(u1));
u2(DBCIndices) = lambda*DBCValues;
u2(FreeIndices) = x;

end
