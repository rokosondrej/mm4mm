function [u2,Niter,minDiag,status] = minimize_r(p,t,material,u1,DBCIndices,tDBCValues,...
    dtDBCValues,Fext,FreeIndices,TOL_r,MaxNiter,ngauss,maxNumThreads,SolverType)

% Solve for x - a vector of free degrees of freedom, using standard Newton algorithm
status = 1; % true for success
alpha = 1;
x = u1(FreeIndices);
x0 = x;
eps_r = 1+TOL_r;
Niter = 0;
while eps_r > TOL_r
    Niter = Niter+1;
    
    if Niter == 1
        [u,G,H,H_DBC] = grad_hess(p,t,material,ngauss,x,DBCIndices,...
            tDBCValues,FreeIndices,maxNumThreads);
        G = G+H_DBC*dtDBCValues;
    else
        [u,G,H,~] = grad_hess(p,t,material,ngauss,x,DBCIndices,...
            tDBCValues+dtDBCValues,FreeIndices,maxNumThreads);
    end
    if ~isempty(Fext)
        G = G-Fext(FreeIndices); % add applied forces
    end
    
    % Solve the system
    switch SolverType
        case 0
            % Use Matlab's backslash
            minDiag = 1;
            dx = -H\G;
        case 1
            % Use ldl decomposition for stability
            [L,D,P] = ldl(H,'vector');
            minDiag = min(full(diag(D)));
            dx = 0*x;
            dx(P) = -(L'\(D\(L\G(P))));
        case 2
            % Use pcg for large 3D systems
            minDiag = 1;
            M = spdiags(diag(H),0,size(H,1),size(H,1));
            [dx,flag,relres,iter,resvec] = pcg(H,-G,TOL_r,2000,M);
            if flag~=0
                fprintf(sprintf('pcg has not converged to given tolerance.\nrelres = %g.\n',relres));
            end
        otherwise
            error('Wrong SolverType chosen.');
    end
    
    % Construct the increment
    x = x+alpha*dx;
    
    % Update the error
    eps_r = norm(G,'inf') + norm(dx,'inf');;
    if norm(tDBCValues+dtDBCValues)>0
        eps_r = eps_r + norm(dx)/norm(tDBCValues+dtDBCValues);
    end    
    
    % Check the number of iterations
    if Niter>=MaxNiter
        % disp('Max number of Newton iterations exceeded, stop.');
        status = 0; % false for fail
        x = x0;
        break;
    end
    
    % Check too large increments
    if norm(alpha*dx)>norm(p(:)) || isnan(eps_r)
        % disp('Too large increment encountered. Restart with damping.')
        x = x0;
        eps_r = 1+TOL_r;
        Niter = 0;
        alpha = alpha/2;
        if alpha<1/2^2
            % disp('Damped Newton diverges. Stop.')
            x = x0;
            status = 0; % false for fail
            break;
        end
    end
end

% Reconstruct converged u2-vector from x
u2 = zeros(size(u1));
u2(DBCIndices) = tDBCValues+dtDBCValues;
u2(FreeIndices) = x;

end
