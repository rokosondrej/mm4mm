function [u2,Niter,minDiag,status,lambda] = solve_rC(p,t,material,u1,DBCIndices,...
    tDBCValues,dtDBCValues,FreeIndices,C,B,TOL_r,MaxNiter,ngauss,maxNumThreads)

% Solve for x - a vector of free degrees of freedom, using standard Newton algorithm
status = 1; % true for success
x = u1(FreeIndices);
x0 = x;
eps_r = 1+TOL_r;
Niter = 0;
alpha = 1;
while eps_r > TOL_r
    Niter = Niter+1;
    if Niter == 1 && ~isempty(dtDBCValues)
        [u,G,H,H_DBC] = grad_hess(p,t,material,ngauss,x,DBCIndices,...
            tDBCValues,FreeIndices,maxNumThreads);
        EG = [G+H_DBC*dtDBCValues;C*u-B];
    else
        [u,G,H,~] = grad_hess(p,t,material,ngauss,x,DBCIndices,...
            tDBCValues+dtDBCValues,FreeIndices,maxNumThreads);
        EG = [G;C*u-B];
    end
    
    % Introduce constraints, use primal-dual formulation, assembly extended quatities
    EH = [H,C(:,FreeIndices)'
        C(:,FreeIndices),sparse(size(C,1),size(C,1))];
    
    % Solve the system
    [L,D,P] = ldl(EH,'vector');
    if length(find(diag(D)<0))==size(C,1)
        minDiag = 1;
    else
        minDiag = -1;
    end
    du = 0*x;
    du(P) = -(L'\(D\(L\EG(P))));
    % du = -EH\EG;
    dx = du(1:end-size(C,1));
    lambda = du(end-size(C,1)+1:end);
    x = x + alpha*dx;
    
    % Update the error
    eps_r = norm(G+C(:,FreeIndices)'*lambda) + ...
        norm(alpha*dx); % /norm(x-u1(FreeIndices));
    
    % Check the number of iterations
    if Niter>=MaxNiter
        % disp('Max number of Newton iterations exceeded, stop.');
        status = 0; % false for fail
        x = x0;
        break;
    end
    
    % Check too large increments
    if norm(alpha*dx,'inf')>norm(p(:),'inf')
        % disp('Too large increment encountered. Restart with damping.')
        x = x0;
        eps_r = 1+TOL_r;
        Niter = 0;
        alpha = alpha/2;
        if alpha<1/2
            % disp('Damped Newton diverges. Stop.')
            x = x0;
            status = 0; % false for fail
            break;
        end
    end
end

% Reconstruct converged u2-vector from x
u2 = zeros(size(u1));
u2(DBCIndices) = tDBCValues+dtDBCValues;
u2(FreeIndices) = x;

end
