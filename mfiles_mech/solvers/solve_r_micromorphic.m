function [u2,Niter,minDiag,status] = solve_r_micromorphic(pM,tM,Mngauss,u1,...
    DBCIndv0,tDBCValv0,dtDBCValv0,tFext,FreeIndv0,DBCIndvi,tDBCValvi,dtDBCValvi,FreeIndvi,...
    maxNumThreads,mbctype,TOL_M,maxMiter,callmode,GradHessType)

% Solve for u2, a vector of free degrees of freedom, using standard Newton algorithm
global DATA;
u2 = u1;
status = 1;
eps_M = 1;
Niter = 0;
alpha = 1;
while eps_M > TOL_M
    Niter = Niter + 1;
    DATA.Niter = Niter; % store iteration info for stabilization
    
    % Sample energy, gradient, and Hessian
    if Niter==1
        [~,f,K,~,Kdbc] = energy_micromorphic_2d(u2,pM,tM,Mngauss,DBCIndv0,tDBCValv0,...
            FreeIndv0,DBCIndvi,tDBCValvi,FreeIndvi,maxNumThreads,mbctype,0,callmode,GradHessType);
        f = f+Kdbc*[dtDBCValv0;dtDBCValvi];
    else
        [~,f,K] = energy_micromorphic_2d(u2,pM,tM,Mngauss,DBCIndv0,...
            tDBCValv0+dtDBCValv0,FreeIndv0,DBCIndvi,tDBCValvi+dtDBCValvi,FreeIndvi,...
            maxNumThreads,mbctype,0,callmode,GradHessType);
    end
    if ~isempty(tFext)
        f = f-tFext([FreeIndv0;FreeIndvi]); % add applied forces
    end
    
    % Compute increment using LDL'
    [L,D,P] = ldl(K,'vector');
    minDiag = min(full(diag(D)));
    duM = 0*f;
    duM(P) = -(L'\(D\(L\f(P))));
    u2 = u2 + alpha*duM;
    
    % Update error
    eps_M = norm(f,'inf') + norm(duM,'inf')/norm(u2-u1);
    
    % Check the number of iterations
    if Niter>maxMiter
        % warning('Max number of Newton iterations exceeded, stop.');
        status = 0; % false for fail
        minDiag = 1;
        u2 = u1;
        break;
    end
    
    % Check too large increments
    if norm(alpha*duM)>norm(pM(:)) || isnan(eps_M)
        u2 = zeros(size(u1));
        status = 0;
        minDiag = 1;
        disp('Too large increment encountered. halve time step.')
        return;
    end
end

end
