function [u2,Niter,minDiag,status] = solve_r(p,t,material,ngauss,u1,...
    DBCIndices,DBC2Ind,tDBCValues,dtDBCValues,IndIndices,DepIndices,...
    FreeIndices,FIIndices,C,TOL_r,MaxNiter,maxNumThreads)

% Solve for x, a vector of free degrees of freedom, using standard Newton algorithm
status = 1; % true for success
x = u1(FreeIndices);
x0 = x;
eps_r = 1+TOL_r;
Niter = 0;
alpha = 1;
while eps_r > TOL_r
    Niter = Niter+1;
    if Niter == 1
        [u,f_r,~,K_r,K_rDBC] = grad_hess_c(p,t,material,ngauss,x,...
            DBCIndices,DBC2Ind,tDBCValues,IndIndices,...
            DepIndices,FreeIndices,FIIndices,C,maxNumThreads);
        G = f_r+K_rDBC*dtDBCValues;
    else
        [u,f_r,~,K_r,~] = grad_hess_c(p,t,material,ngauss,x,...
            DBCIndices,DBC2Ind,tDBCValues+dtDBCValues,IndIndices,...
            DepIndices,FreeIndices,FIIndices,C,maxNumThreads);
        G = f_r;
    end
    
    % Compute individual increments using LDL'
    [L,D,P] = ldl(K_r,'vector');
    minDiag = min(full(diag(D)));
    dx = 0*x;
    dx(P) = -(L'\(D\(L\G(P))));
    x = x + alpha*dx;
    
    % Update the error
    eps_r = norm(G) + norm(alpha*dx)/norm(x-u1(FreeIndices));
    
    % Check the number of iterations
    if Niter>=MaxNiter
        % warning('Max number of Newton iterations exceeded, stop.');
        status = 0; % false for fail
        x = x0;
        break;
    end
    
    % Check too large increments
    if norm(alpha*dx)>norm(p(:)) || isnan(eps_r)
        % warning('Too large increment encountered. Restart with damping.')
        x = x0;
        eps_r = 1+TOL_r;
        Niter = 0;
        alpha = alpha/2;
        if alpha<1/2^2
            % warning('Damped Newton diverges. Stop.')
            x = x0;
            status = 0; % false for fail
            break;
        end
    end
end

% Reconstruct converged u2-vector from x
u2 = zeros(size(u1));
u2(DBCIndices) = tDBCValues+dtDBCValues;
u2(FreeIndices) = x;
u2(DepIndices) = C(:,IndIndices)*u2(IndIndices);

end
