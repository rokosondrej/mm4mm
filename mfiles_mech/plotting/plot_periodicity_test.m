function plot_periodicity_test(handle,DATA,p,t,w)

% Get w
figure(handle);
pdef = reshape(p(:)+w,size(p));

% Plot individual RVEs
if strcmp(DATA.rvetype,'rect_square') || strcmp(DATA.rvetype,'rect_hexa')
    % Test periodicity of w for rect_square and rect_hexa
    CellSize = DATA.CellSize;
    tpdef = [pdef(1,:)+2*CellSize;pdef(2,:)+0];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
    tpdef = [pdef(1,:)-2*CellSize;pdef(2,:)+0];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
    tpdef = [pdef(1,:)+0;pdef(2,:)+2*CellSize];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
    tpdef = [pdef(1,:)+0;pdef(2,:)-2*CellSize];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
    tpdef = [pdef(1,:)-2*CellSize;pdef(2,:)+2*CellSize];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
    tpdef = [pdef(1,:)-2*CellSize;pdef(2,:)-2*CellSize];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
    tpdef = [pdef(1,:)+2*CellSize;pdef(2,:)+2*CellSize];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
    tpdef = [pdef(1,:)+2*CellSize;pdef(2,:)-2*CellSize];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
elseif strcmp(DATA.rvetype,'skew_hexa') || strcmp(DATA.rvetype,'hexa_hexa')
    % Test periodicity of w for skew_hexa
    DiamOuter = 1.6;
    hx = DiamOuter/2*cos(pi/6)*2;
    tpdef = [pdef(1,:)+2*hx;pdef(2,:)+0];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
    tpdef = [pdef(1,:)-2*hx;pdef(2,:)+0];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
    tpdef = [pdef(1,:)+hx;pdef(2,:)+1.5*DiamOuter];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
    tpdef = [pdef(1,:)+hx;pdef(2,:)-1.5*DiamOuter];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
    tpdef = [pdef(1,:)-hx;pdef(2,:)+1.5*DiamOuter];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
    tpdef = [pdef(1,:)-hx;pdef(2,:)-1.5*DiamOuter];
    plot_mesh(handle,tpdef,t,elemtype,0.75*[0,1,0]);
else
    error('Wrong rvetype chosen.');
end

end
