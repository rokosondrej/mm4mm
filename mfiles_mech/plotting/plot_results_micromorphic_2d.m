function plot_results_micromorphic_2d(R,idstep,nmodes,...
    Melemtype,Mngauss,material,pM,tM,Mnnode,npoints,TOL_g,maxNumThreads)

% Get M-nodal values of all fields
v0 = R(1:2*Mnnode,idstep);
vi = zeros(Mnnode,nmodes);
for i = 1:nmodes
    vi(:,i) = R((i+1)*Mnnode+1:(i+2)*Mnnode,idstep);
end

% Interpolate quantities on a background mesh: v0
[x0,y0] = meshgrid(linspace(min(pM(1,:)),max(pM(1,:)),npoints),...
    linspace(min(pM(2,:)),max(pM(2,:)),npoints));
% Element contour
% tx = 0.5*MSizeX*linspace(-1,1,npoints);
% ty = 0.5*MSizeY*linspace(-1,1,npoints);
% x0 = [tx,0.5*MSizeX*ones(size(tx)),tx(end:-1:1),-0.5*MSizeX*ones(size(tx))];
% y0 = [-0.5*MSizeX*ones(size(ty)),ty,0.5*MSizeY*ones(size(ty)),ty(end:-1:1)];
% Interpolate at sampling points
[~,v0q,~,~,~,~] = sample_UFP_2d(pM,tM,material,Mngauss,v0,x0(:),y0(:),...
    TOL_g,0,maxNumThreads); % v0 field
tv0x = reshape(v0q(1:2:end),size(x0));
tv0y = reshape(v0q(2:2:end),size(x0));

% Interpolate quantities on a background mesh: vi
tvi = zeros([size(x0),nmodes]);
for i = 1:nmodes
    tvin = zeros(size(v0));
    tvin(1:2:end) = vi(:,i);
    tvin(2:2:end) = vi(:,i);
    [~,viq,~,~,~,~] = sample_UFP_2d(pM,tM,material,Mngauss,tvin,x0(:),y0(:),...
        TOL_g,0,maxNumThreads); % vi field
    tvi(:,:,i) = reshape(viq(1:2:end),size(x0));
end

% Z-shift for contours
ids = find(~isnan(tv0x) & ~isnan(tv0y) & ~isnan(tvi(:,:,1)));
zshift = max([norm(tv0x(ids),'inf'),norm(tv0y(ids),'inf'),norm(tvi(ids),'inf')]);

% Plot v0x
handlev0x = figure(101);clf;box on;grid on;hold all;
surf(x0+0*tv0x,y0+0*tv0y,tv0x,'facecolor','interp','edgecolor','interp');
xlabel('$X_1$');ylabel('$X_2$');zlabel('$v_{0x}(\vec{X})$');
set(handlev0x,'name','v0x','numbertitle','off');
colorbar;
colormap diverging_map;
% Plot contours
curve = contourc(x0(1,:),y0(:,1),tv0x,6);
id = 1;
while id<length(curve) % loop over all contours
    
    % Extract one contour
    xc = curve(1,id+1:id+curve(2,id));
    yc = curve(2,id+1:id+curve(2,id));
    
    % Plot the contour
    plot3(xc,yc,zshift*ones(size(xc)),'--','color',0.4*[1,1,1]);
    
    % Proceed to the next contour
    id = id+1+curve(2,id);
end

% Plot reference mesh
% plot_mesh(handlev0x,pM,tM,Melemtype,0.75*[1,1,1]);
% Plot deformed mesh
% pdef = reshape(pM(:)+v0,size(pM));
% plot_mesh(handlev0x,pdef,tM,Melemtype,0.5*[1,1,1]);
view([0,0,1]);axis tight;axis equal;title('$v_{0x}$'); % view(3);
drawnow;

% Plot v0y
handlev0y = figure(102);clf,box on,grid on,hold all;
surf(x0+0*tv0x,y0+0*tv0y,tv0y,'facecolor','interp','edgecolor','interp');
xlabel('$X_1$');ylabel('$X_2$');zlabel('$v_{0y}(\vec{X})$');
set(handlev0y,'name','v0y','numbertitle','off');
colorbar;
colormap diverging_map;
% Plot contours
curve = contourc(x0(1,:),y0(:,1),tv0y,6);
id = 1;
while id<length(curve) % loop over all contours
    
    % Extract one contour
    xc = curve(1,id+1:id+curve(2,id));
    yc = curve(2,id+1:id+curve(2,id));
    
    % Plot the contour
    plot3(xc,yc,zshift*ones(size(xc)),'--','color',0.4*[1,1,1]);
    
    % Proceed to the next contour
    id = id+1+curve(2,id);
end
% Plot reference mesh
% plot_mesh(handle,pM,tM,Melemtype,0.75*[1,1,1]);
% Plot deformed mesh
% plot_mesh(handle,pdef,tM,Melemtype,[0,0,1]);
view([0,0,1]);axis tight;axis equal;title('$v_{0y}$'); % view(3);
drawnow;

% Plot vi
for i = 1:nmodes
    handlevi = figure(102+i);clf,box on,grid on,hold all;
    surf(x0+0*tv0x,y0+0*tv0y,tvi(:,:,i),'facecolor','interp','edgecolor','interp');
    xlabel('$X_1$');ylabel('$X_2$');zlabel(['$v_',num2str(i),'(\vec{X})$']);
    set(handlevi,'name',['v',num2str(i)],'numbertitle','off');
    colorbar;
    colormap diverging_map;
    % Plot contours
    curve = contourc(x0(1,:),y0(:,1),tvi(:,:,i),6);
    id = 1;
    while id<length(curve) % loop over all contours
        
        % Extract one contour
        xc = curve(1,id+1:id+curve(2,id));
        yc = curve(2,id+1:id+curve(2,id));
        
        % Plot the contour
        plot3(xc,yc,zshift*ones(size(xc)),'--','color',0.4*[1,1,1]);
        
        % Proceed to the next contour
        id = id+1+curve(2,id);
    end
    % Plot reference mesh
    % plot_mesh(handlevi,pM,tM,Melemtype,0.75*[1,1,1]);
    % Plot deformed mesh
    % plot_mesh(handlevi,pdef,tM,Melemtype,[0,0,1]);
    view([0,0,1]);axis tight;axis equal;title(['$v_',num2str(i),'$']); % view(3);
    drawnow;
end
end
