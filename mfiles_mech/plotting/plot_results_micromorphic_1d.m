function plot_results_micromorphic_1d(MSizeY,R,idstep,nmodes,Melemtype,...
    pM,tM,Mnnode,TOL_g)

% Get M-nodal values of all fields
v0 = R(1:Mnnode,idstep);
vi = zeros(Mnnode,nmodes);
for i = 1:nmodes
    vi(:,i) = R(i*Mnnode+1:(i+1)*Mnnode,idstep);
end

% Linear elements
if Melemtype==1
    
    % Plot v0y
    handlev0x = figure(101);clf,box on,grid on,hold all;
    plot(v0,pM);
    plot(0*pM,pM,'k');
    plot(0*pM,pM,'.k');
    xlabel('$v_{0y}(X_2)$');ylabel('$X_2$');
    set(handlev0x,'name','v0y','numbertitle','off');
    drawnow;
    
    % Plot vi
    for i = 1:nmodes
        handlevi = figure(101+i);clf,box on,grid on,hold all;
        plot(vi(:,i),pM);
        plot(0*pM,pM,'k');
        plot(0*pM,pM,'.k');
        xlabel(['$v_',num2str(i),'(X_2)$']);ylabel('$X_2$');
        set(handlevi,'name',['v',num2str(i)],'numbertitle','off');
        drawnow;
    end
    
    % Quadratic elements
elseif Melemtype==2
    
    % Interpolate quantities on a background mesh: v0
    x0 = MSizeY*linspace(-1,1,1000);
    [tv0x,~] = sample_UFP_1d(pM,tM,v0,x0,TOL_g); % v0 field
    
    % Interpolate quantities on a background mesh: vi
    tvi = zeros([length(x0),nmodes]);
    for i = 1:nmodes
        [tvi(:,i),~] = sample_UFP_1d(pM,tM,vi(:,i),x0,TOL_g); % v1 field
    end
    
    % Plot v0x
    handlev0x = figure(101);clf,box on,grid on,hold all;
    plot(tv0x,x0);
    plot(0*pM,pM,'k');
    plot(0*pM,pM,'.k');
    xlabel('$v_0(X_2)$');ylabel('$X_2$');
    set(handlev0x,'name','v0y','numbertitle','off');
    drawnow;
    
    % Plot F_22 = v0x'+1
    handlev0x = figure(102);clf,box on,grid on,hold all;
    plot(gradient(tv0x,x0)+1,x0);
    plot(ones(size(pM)),pM,'k');
    plot(ones(size(pM)),pM,'.k');
    xlabel('$F_{22}(X_2)$');ylabel('$X_2$');
    set(handlev0x,'name','F22','numbertitle','off');
    drawnow;
    
    % Plot vi
    for i = 1:nmodes
        handlevi = figure(102+i);clf,box on,grid on,hold all;
        plot(tvi(:,i),x0);
        plot(0*pM,pM,'k');
        plot(0*pM,pM,'.k');
        xlabel(['$v_',num2str(i),'$']);ylabel('$X_2$');
        set(handlevi,'name',['v',num2str(i)],'numbertitle','off');
        drawnow;
    end
end

end
