function handle = plot_mesh(handle,p,t,elemtype,color)

% Get numbering
switch elemtype
    
    % 2D element types
    case 2 % T3
        ids = [1 2 3 1];
    case 3 % Q4
        ids = [1 2 3 4 1];
    case 9 % T6
        ids = [1 4 2 5 3 6 1];
    case 10 % Q9
        ids = [1 5 2 6 3 7 4 8 1];
    case 16 % Q8
        ids = [1 5 2 6 3 7 4 8 1];
    case 21 % T10
        ids = [1 4 5 2 6 7 3 8 9 1];
    case 36 % Q16
        ids = [1 5 6 2 7 8 3 9 10 4 11 12 1];
        
        % 3D element types
    case 4 % Tet4
        ids = [1 2 3 1 4 2 3 4];
    case 5 % Hexa8
        ids = [1 2 3 4 1 5 6 2 6 7 3 7 8 4 8 5];
    case 11 % Tet10
        ids = [1 5 2 6 3 7 1 8 4 10 2 6 3 9 4];
    case 12 % Hexa27
        ids = [1 9 2 12 3 14 4 10 1 11 5 17 6 13 2 13 6 19 7 15 3 15 7 20 8 16 4 16 8 18 5 11 1];
    case 17 % Hexa20
        ids = [1 9 2 12 3 14 4 10 1 11 5 17 6 13 2 13 6 19 7 15 3 15 7 20 8 16 4 16 8 18 5 11 1];
        
        % Unknown element
    otherwise
        error('Wrong element type chosen.');
end

% Plot mesh
if isempty(handle)
    handle = figure;
    hold all;
    axis equal;
    box on;
    grid on;
end
if elemtype==2 || elemtype==9
    % Use pdetool package for plotting
    h = pdeplot(p,[],t);
    h.Color = color;
elseif elemtype==4 || elemtype==11
    % Use pdetool package for plotting
    h = pdeplot3D(p,t);
    h.FaceAlpha = 0;
    h.EdgeColor = color;
    axis on;
elseif elemtype==3 || elemtype==10 || elemtype==16 || elemtype==36
    % Plot individual elements
    for j = 1:size(t,2)
        plot(p(1,t(ids,j)),p(2,t(ids,j)),'color',color);
    end
elseif elemtype==5 || elemtype==12 || elemtype==17
    % Plot individual elements
    for j = 1:size(t,2)
        plot3(p(1,t(ids,j)),p(2,t(ids,j)),p(3,t(ids,j)),'color',color);
    end
end
% Plot axes' labes
xlabel('$X_1$');ylabel('$X_2$');zlabel('$X_3$');

end
