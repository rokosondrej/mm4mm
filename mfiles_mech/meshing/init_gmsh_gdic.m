function [p,t] = init_gmsh_gdic(SizeX,SizeY,hmax,elemtype,ngauss,...
    material,TOL_g,callmode,maxNumThreads)

% Initialize mesh
fprintf('Init mesh...\n');tic;
if ~isfolder('gmsh')
    mkdir('gmsh');
end
% Create gmsh input file and call gmsh
fin = fopen('gmsh/gdicmesh.geo','w');
% Define points of the specimen
fprintf(fin,'Point(1) = {%.15g, %.15g, %.15g};\n',[-SizeX,-SizeY,0]);
fprintf(fin,'Point(2) = {%.15g, %.15g, %.15g};\n',[SizeX,-SizeY,0]);
fprintf(fin,'Point(3) = {%.15g, %.15g, %.15g};\n',[SizeX,SizeY,0]);
fprintf(fin,'Point(4) = {%.15g, %.15g, %.15g};\n',[-SizeX,SizeY,0]);
% Define lines, specimen
fprintf(fin,'Line(1) = {1, 2};\n');
fprintf(fin,'Line(2) = {2, 3};\n');
fprintf(fin,'Line(3) = {3, 4};\n');
fprintf(fin,'Line(4) = {4, 1};\n');
% Define line loops
fprintf(fin,'Line Loop(1) = {1, 2, 3, 4};\n'); % RVE
% Define surfaces
fprintf(fin,'Plane Surface(1) = {1};\n');
% Require regular mesh
fprintf(fin,'Transfinite Line {1,3} = %d;\n',round(2*SizeX/hmax)+1);
fprintf(fin,'Transfinite Line {2,4} = %d;\n',round(2*SizeY/hmax)+1);
fprintf(fin,'Transfinite Surface (1);\n');
% Construct physical surface
fprintf(fin,'Physical Surface(1) = {1};\n');
fprintf(fin,'Mesh.Optimize = 1;\n');

% Set maximum element size
% fprintf(fin,'Mesh.CharacteristicLengthMax = %.15g;\n',hmax);
fprintf(fin,'Mesh.CharacteristicLengthMax = %.15g;\n',hmax);
fprintf(fin,'Mesh.CharacteristicLengthMin = %.15g;\n',hmax);
% fprintf(fin,'Mesh.FlexibleTransfinite = 1;\n');
% Define mesh refinement through background field
% fprintf(fin,'Field[1] = MathEval;\n');
% fprintf(fin,'Field[1].F = "%g";\n',hmax);
% fprintf(fin,'Background Field = 1;\n');

% fprintf(fin,'Mesh.HighOrderOptPrimSurfMesh = 1;\n');
fprintf(fin,'Mesh.HighOrderOptimize = 1;\n');
fprintf(fin,'Mesh.HighOrderNumLayers = 10;\n');
% fprintf(fin,'Mesh.HighOrderThresholdMin = 0.9;\n');
% fprintf(fin,'Mesh.HighOrderThresholdMax = 1.1;\n');
fprintf(fin,'Mesh.Smoothing = 10;\n');
% fprintf(fin,'Mesh.SmoothNormals = 1;\n');
% fprintf(fin,'Mesh.SmoothRatio = 2;\n');
fprintf(fin,'Mesh.MshFileVersion = 2.2;\n');
if elemtype==3 || elemtype==10 || elemtype==16 || elemtype==36 % quads
    fprintf(fin,'Recombine Surface "*";\n');
    if elemtype==16
        fprintf(fin,'Mesh.SecondOrderIncomplete=1;\n');
    end
    % Choose algorithm for quadrangle mesh
    fprintf(fin,'Mesh.Algorithm = 2;\n'); % 2 - automatic, 7 - BAMG, 8 - DelQuad, 9 - Packing of Parallelograms
else
    % Choose algorithm for triangular mesh
    fprintf(fin,'Mesh.Algorithm = 6;\n'); % 1 - MeshAdapt, 2 - automatic, 5 - Delaunay, 6 - Frontal, 7 - BAMG
end

% Set interpolation order, 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9
switch elemtype
    case 2
        elnodes = 3;
        elorder = 1;
    case 3
        elnodes = 4;
        elorder = 1;
    case 9
        elnodes = 6;
        elorder = 2;
    case 10
        elnodes = 9;
        elorder = 2;
    case 16
        elnodes = 8;
        elorder = 2;
    case 21
        elnodes = 8;
        elorder = 3;
    case 36
        elnodes = 16;
        elorder = 3;
    otherwise
        error('Wrong element type chosen.');
end
fclose(fin);

% Call gmsh
system(['gmsh gmsh/gdicmesh.geo -o gmsh/gdicmesh.msh ',...
    ' -order ',num2str(elorder),' -2 ']);

% Open mesh.msh and parse the output data
fout = fopen('gmsh/gdicmesh.msh','r');
while ~feof(fout)
    tline = fgetl(fout);
    if strcmp(tline,'$Nodes')
        npoints = str2num(fgets(fout));
        points = fscanf(fout,'%g',[4,npoints])';
    end
    if strcmp(tline,'$Elements')
        nelem = str2num(fgets(fout));
        elements = zeros(nelem,5+elnodes);
        for i = 1:nelem
            tline = fgetl(fout);
            numbers = str2num(tline);
            elements(i,1:length(numbers)) = numbers;
        end
    end
end
fclose('all');

% Delete files to prevent their reloading when unsuccesful call to gmsh made
delete('gmsh/gdicmesh.geo');
delete('gmsh/gdicmesh.msh');

% Create p and t matrices and proceed as usual.
p = points(:,2:3)';
t = elements(elements(:,2)==elemtype,6:end)';
if(find(elements(:,2)~=elemtype,1))
    warning('Mixed mesh generated, includes different kinds of elements.');
end

% Assign materials to elements
elnodes = size(t,1);
t = [t;ones(1,size(t,2))];
[Wq,Uq,Eq,Sq,Xq,Yq] = sample_UES_2d(p,t,material,ngauss,zeros(size(p(:))),[],[],...
    TOL_g,1,maxNumThreads);
Cg = [Xq;Yq];

% Plot the resulting mesh
if strcmp(callmode,'v')
    handle = figure(2);clf,hold all,axis equal,box on;
    plot_mesh(handle,p,t,elemtype,[0,0,1]);
    plot(p(1,:),p(2,:),'.k');
    plot(Cg(1,t(elnodes+1,:)==1),Cg(2,t(elnodes+1,:)==1),'.r');
    plot(Cg(1,t(elnodes+1,:)==2),Cg(2,t(elnodes+1,:)==2),'.g');
    set(handle,'name','Mesh and materials','numbertitle','off');
end
fprintf('Time consumed %g s\n\n',toc);

end
