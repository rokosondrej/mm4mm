function [p,t,SizeX,SizeY] = init_gmsh_rve_hexagonal_rectangle(DiamOuter,DiamInner,...
    InclusionType,hmax,material,elemtype,TOL_g,callmode)

% Initialize mesh
fprintf('Init mesh...\n');t_mesh = tic;
if ~isfolder('gmsh')
    mkdir('gmsh');
end
% Create gmsh input file
fin = fopen('gmsh/mvemesh.geo','w');
fprintf(fin,'SetFactory("OpenCASCADE");\n');

% Create a bounding box
theta0 = -pi/2;
SizeX = DiamOuter*cos(pi/6);
SizeY = 3*DiamOuter*sin(pi/6);
fprintf(fin,'Rectangle(1) = {%.15g, %.15g, 0, %.15g, %.15g};\n',[0,...
    -SizeY,SizeX,2*SizeY]);

% Create database of hexagons/circles
shift = [DiamOuter/4*sqrt(3)/2,DiamOuter/4*(sqrt(3)*sqrt(3)/2-2*sin(theta0+5*pi/3))-SizeY,0];
Circles(1).p = [0.5,-sqrt(3)/2,0]*DiamOuter*sqrt(3)/4 + shift;
Circles(2).p = [-0.5,sqrt(3)/2,0]*DiamOuter*sqrt(3)/4 + shift;
Circles(3).p = [1.5,sqrt(3)/2,0]*DiamOuter*sqrt(3)/4 + shift;
Circles(4).p = [0.5,3*sqrt(3)/2,0]*DiamOuter*sqrt(3)/4 + shift;
Circles(5).p = [-0.5,5*sqrt(3)/2,0]*DiamOuter*sqrt(3)/4 + shift;
Circles(6).p = [1.5,5*sqrt(3)/2,0]*DiamOuter*sqrt(3)/4 + shift;
Circles(7).p = [0.5,7*sqrt(3)/2,0]*DiamOuter*sqrt(3)/4 + shift;

% Symmetric part-needed just for material
Circles(8).p = [-Circles(1).p(1),Circles(1).p(2:end)];
Circles(9).p = [-Circles(2).p(1),Circles(2).p(2:end)];
Circles(10).p = [-Circles(3).p(1),Circles(3).p(2:end)];
Circles(11).p = [-Circles(4).p(1),Circles(4).p(2:end)];
Circles(12).p = [-Circles(5).p(1),Circles(5).p(2:end)];
Circles(13).p = [-Circles(6).p(1),Circles(6).p(2:end)];
Circles(14).p = [-Circles(7).p(1),Circles(7).p(2:end)];

% Plot the RVE
% phi = linspace(0,2*pi,1000);
% figure(93);clf;hold all;axis equal;
% plot(SizeX*[0,1,1,0,0],SizeY*[-1,-1,1,1,-1],'k');
% for i = 1:length(Circles)
%     x = Circles(i).p(1)+DiamOuter/2*cos(phi);
%     y = Circles(i).p(2)+DiamOuter/2*sin(phi);
%     plot(x,y,'k');
% end

% Write hexagons and holes
point = 4;
line = 4;
loop = 1;
surface = 1;
for i = 1:length(Circles) % hexagons
    
    % Define points
    theta0 = -pi/2;
    r = DiamOuter/2;
    P0 = Circles(i).p;
    P1 = P0+r*[cos(theta0+0*pi/3),sin(theta0+0*pi/3),0];
    P2 = P0+r*[cos(theta0+1*pi/3),sin(theta0+1*pi/3),0];
    P3 = P0+r*[cos(theta0+2*pi/3),sin(theta0+2*pi/3),0];
    P4 = P0+r*[cos(theta0+3*pi/3),sin(theta0+3*pi/3),0];
    P5 = P0+r*[cos(theta0+4*pi/3),sin(theta0+4*pi/3),0];
    P6 = P0+r*[cos(theta0+5*pi/3),sin(theta0+5*pi/3),0];
    fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+1,P1);
    fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+2,P2);
    fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+3,P3);
    fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+4,P4);
    fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+5,P5);
    fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+6,P6);
    
    % Define lines
    fprintf(fin,'Line(%i) = {%i, %i};\n',line+1,point+1,point+2);
    fprintf(fin,'Line(%i) = {%i, %i};\n',line+2,point+2,point+3);
    fprintf(fin,'Line(%i) = {%i, %i};\n',line+3,point+3,point+4);
    fprintf(fin,'Line(%i) = {%i, %i};\n',line+4,point+4,point+5);
    fprintf(fin,'Line(%i) = {%i, %i};\n',line+5,point+5,point+6);
    fprintf(fin,'Line(%i) = {%i, %i};\n',line+6,point+6,point+1);
    
    % Define line loops
    fprintf(fin,'Line Loop(%i) = {%i, %i, %i, %i, %i, %i};\n',loop+1,line+1,line+2,line+3,line+4,line+5,line+6);
    
    % Define surfaces
    fprintf(fin,'Plane Surface(%i) = {%i};\n',surface+1,loop+1);
    
    % Increment points, lines, line loops, and surfaces
    point = point+6;
    line = line+6;
    loop = loop+1;
    surface = surface+1;
end
for i = 1:length(Circles) % holes
    
    theta0 = -pi/2;
    r = DiamInner/2;
    P0 = Circles(i).p;
    if strcmp(InclusionType,'hexagon')
        
        % Define points
        P1 = P0+r*[cos(theta0+0*pi/3),sin(theta0+0*pi/3),0];
        P2 = P0+r*[cos(theta0+1*pi/3),sin(theta0+1*pi/3),0];
        P3 = P0+r*[cos(theta0+2*pi/3),sin(theta0+2*pi/3),0];
        P4 = P0+r*[cos(theta0+3*pi/3),sin(theta0+3*pi/3),0];
        P5 = P0+r*[cos(theta0+4*pi/3),sin(theta0+4*pi/3),0];
        P6 = P0+r*[cos(theta0+5*pi/3),sin(theta0+5*pi/3),0];
        fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+1,P1);
        fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+2,P2);
        fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+3,P3);
        fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+4,P4);
        fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+5,P5);
        fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+6,P6);
        
        % Define lines
        fprintf(fin,'Line(%i) = {%i, %i};\n',line+1,point+1,point+2);
        fprintf(fin,'Line(%i) = {%i, %i};\n',line+2,point+2,point+3);
        fprintf(fin,'Line(%i) = {%i, %i};\n',line+3,point+3,point+4);
        fprintf(fin,'Line(%i) = {%i, %i};\n',line+4,point+4,point+5);
        fprintf(fin,'Line(%i) = {%i, %i};\n',line+5,point+5,point+6);
        fprintf(fin,'Line(%i) = {%i, %i};\n',line+6,point+6,point+1);
        
        % Define line loops
        fprintf(fin,'Line Loop(%i) = {%i, %i, %i, %i, %i, %i};\n',loop+1,line+1,line+2,line+3,line+4,line+5,line+6);
        
        % Define surfaces
        fprintf(fin,'Plane Surface(%i) = {%i};\n',surface+1,loop+1);
    elseif strcmp(InclusionType,'circle')
        
        % Define surfaces
        fprintf(fin,'Disk(%d) = {%.15g, %.15g, %.15g, %.15g};\n',surface+1,P0,r);
    else
        error('Wrong inclusion type chosen.');
    end
    
    % Increment points, lines, line loops, and surfaces
    point = point+6;
    line = line+6;
    loop = loop+1;
    surface = surface+1;
end

% Create the resulting domain by subtracting the hole
if material(2,1)==0
    fprintf(fin,'f1() = BooleanFragments{ Surface{1}; }{ Surface{2:%d}; Delete; };\n',length(Circles)+1);
    fprintf(fin,'f2() = BooleanIntersection{ Surface{1}; Delete; }{ Surface{f1()}; Delete; };\n');
    fprintf(fin,'f3() = BooleanDifference{ Surface{f2()}; Delete; }{ Surface{%d:%d}; Delete; };\n',length(Circles)+2,surface);
else
    fprintf(fin,'f1() = BooleanFragments{ Surface{1}; }{ Surface{2:%d}; Delete; };\n',surface);
    fprintf(fin,'f2() = BooleanIntersection{ Surface{1}; Delete; }{ Surface{f1()}; Delete; };\n');
end

% Set maximum element size
fprintf(fin,'Mesh.CharacteristicLengthMax = %.15g;\n',hmax);

% Coherence (removes all duplicit elementary geometrical entities)
% fprintf(fin,'Coherence;\n');

% Mesh optimization
fprintf(fin,'Mesh.HighOrderOptimize = 1;\n');

% Mesh file format
fprintf(fin,'Mesh.MshFileVersion = 2.2;\n');

% Choose elements
if elemtype==3 || elemtype==10 || elemtype==16 || elemtype==36 % quads
    fprintf(fin,'Recombine Surface "*";\n');
    if elemtype==16
        fprintf(fin,'Mesh.SecondOrderIncomplete=1;\n');
    end
    % Choose algorithm for quadrangle mesh
    fprintf(fin,'Mesh.Algorithm = 2;\n'); % 2 - automatic, 7 - BAMG, 8 - DelQuad
else
    % Choose algorithm for triangular mesh
    fprintf(fin,'Mesh.Algorithm = 2;\n'); % 1 - MeshAdapt, 2 - automatic, 5 - Delaunay, 6 - Frontal, 7 - BAMG
end
switch elemtype % set interpolation order, 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9
    case 2
        elnodes = 3;
        elorder = 1;
    case 3
        elnodes = 4;
        elorder = 1;
    case 9
        elnodes = 6;
        elorder = 2;
    case 10
        elnodes = 9;
        elorder = 2;
    case 16
        elnodes = 8;
        elorder = 2;
    case 21
        elnodes = 8;
        elorder = 3;
    case 36
        elnodes = 16;
        elorder = 3;
    otherwise
        error('Wrong element type chosen.');
end
fprintf(fin,'Mesh.ElementOrder = %d;\n',elorder);
fclose(fin);

% Call gmsh
system('gmsh gmsh/mvemesh.geo -o gmsh/mvemesh.msh -2');

% Open mesh.msh and parse the output data
fout = fopen('gmsh/mvemesh.msh','r');
while ~feof(fout)
    tline = fgetl(fout);
    if strcmp(tline,'$Nodes')
        npoints = str2num(fgets(fout));
        points = fscanf(fout,'%g',[4,npoints])';
    end
    if strcmp(tline,'$Elements')
        nelem = str2num(fgets(fout));
        elements = zeros(nelem,5+elnodes);
        for i = 1:nelem
            tline = fgetl(fout);
            numbers = str2num(tline);
            elements(i,1:length(numbers)) = numbers;
        end
    end
end
fclose('all');

% Delete files to prevent their reloading when unsuccesful call to gmsh made
delete('gmsh/mvemesh.geo');
delete('gmsh/mvemesh.msh');

% Create p and t matrices and proceed
p1 = points(:,2:3)';
t1 = elements(elements(:,2)==elemtype,6:end)';
elemtypes = setdiff(elements(:,2),[1,8,15,26,elemtype]); % factor out 0D, 1D, and elemtype 2D element types
if(~isempty(elemtypes)) % if different type of elements found, throw a warning
    warning(sprintf('Mixed mesh generated.\nIncludes also different kinds of elements than required.'));
end

% Perform symmetry operations on the mesh
p2 = [-p1(1,:);p1(2,:)]; % mirror the top right unit cell w.r.t the vertical axis
t2 = t1+size(p1,2);

% Take care of mirrored elements: assure cw orientation in node numbering (otherwise Jdet<0 in FEM)
% figure(156);clf;hold all;axis equal;
% for i = 1:size(t1,1)
%     P = p1(:,t1(i,1));
%     plot(P(1),P(2),'.k');
%     text(P(1),P(2),num2str(i)); % check element numbering convention
% end
switch elemtype % set interpolation order, 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9, 36 = Q16
    case 2
        t2([2 3],:) = t2([3 2],:);
    case 3
        t2([2 3 4],:) = t2([4 3 2],:);
    case 9
        t2([2 3 4 6],:) = t2([3 2 6 4],:);
    case 10
        t2([2 3 4 5 6 7 8],:) = t2([4 3 2 8 7 6 5],:);
    case 16
        t2([2 3 4 5 6 7 8],:) = t2([4 3 2 8 7 6 5],:);
    case 21
        t2([2 3 4 5 6 7 8 9],:) = t2([3 2 9 8 7 6 5 4],:);
    case 36
        t2([2 3 4 5 6 7 8 9 10 11 12 13 14 15 16],:) = t2([4 3 2 12 11 10 9 8 7 6 5 13 16 15 14],:);
    otherwise
        error('Wrong element type chosen.');
end

% Global p and t matrices
p = [p1,p2];
t = [t1,t2];

% Remove duplicities in p and t
% Check uniqueness of points
idPoints = unique(t(:));
tempp = NaN(2,length(idPoints));
k = 1;
for j = 1:length(idPoints)
    P1 = p(:,idPoints(j));
    if isempty(find(abs(tempp(1,:)-P1(1))+abs(tempp(2,:)-P1(2))<TOL_g,1))
        tempp(:,k) = P1;
        k = k+1;
    end
end
tempp = tempp(:,1:k-1);

% Renumber t
temptr = NaN(size(t));
for j = 1:size(t,2)
    % Find points of triangles in t within p and tempp
    for i = 1:size(t,1)
        Pi = p(:,t(i,j));
        indi = find(abs(tempp(1,:)-Pi(1))+abs(tempp(2,:)-Pi(2))<TOL_g,1);
        if ~isempty(indi)
            temptr(i,j) = indi(1);
        else
            fprintf('tempp for temptr renumbering failed.\n');
        end
    end
end

% Assign cleared arrays for results
p = tempp;
tr = temptr;
t = [tr;ones(1,size(tr,2))]; % assign materials to elements
Xq = zeros(size(t,2),1);
Yq = zeros(size(t,2),1);
for i = 1:size(t,2)
    Cg = mean(p(:,t(1:end-1,i)),2);
    Xq(i) = Cg(1);
    Yq(i) = Cg(2);
end
inside = [];
for i = 1:length(Circles)
    
    theta0 = -pi/2;
    r = DiamInner/2;
    P0 = Circles(i).p;
    if strcmp(InclusionType,'hexagon')
        
        % Define polygon points
        P1 = P0+r*[cos(theta0+0*pi/3),sin(theta0+0*pi/3),0];
        P2 = P0+r*[cos(theta0+1*pi/3),sin(theta0+1*pi/3),0];
        P3 = P0+r*[cos(theta0+2*pi/3),sin(theta0+2*pi/3),0];
        P4 = P0+r*[cos(theta0+3*pi/3),sin(theta0+3*pi/3),0];
        P5 = P0+r*[cos(theta0+4*pi/3),sin(theta0+4*pi/3),0];
        P6 = P0+r*[cos(theta0+5*pi/3),sin(theta0+5*pi/3),0];
        P = [P1;P2;P3;P4;P5;P6;P1];
        incircle = find(inpolygon(Xq,Yq,P(:,1),P(:,2)));
    elseif strcmp(InclusionType,'circle')
        incircle = find((Xq-P0(1)).^2+(Yq-P0(2)).^2<r^2);
    end
    inside = union(inside,incircle);
end
t(end,inside) = 2;

% Plot the resulting mesh
if strcmp(callmode,'v')
    handle = figure(2);clf,hold all,axis equal,box on;
    plot_mesh(handle,p,t,elemtype,[0,0,1]);
    plot(p(1,:),p(2,:),'.k');
    plot(Xq(t(end,:)==1),Yq(t(end,:)==1),'.r');
    plot(Xq(t(end,:)==2),Yq(t(end,:)==2),'.g');
    set(handle,'name','Mesh and materials','numbertitle','off');
end
fprintf('Time consumed %.g s\n\n',toc(t_mesh));

end
