function inclusions = init_inclusions(SizeX,SizeY,NcellX,NcellY,...
    CellSize,Diameter,callmode)

%% Construct inclusions
fprintf('Init inclusions... ');
tic;

% Get centers of unit cells
inclusions(1:NcellX*NcellY) = struct('p',[0;0],'r',0);
k = 0;
for i = 1:NcellX
    for j = 1:NcellY
        k = k+1;
        inclusions(k).p = [-NcellX/2*CellSize+(i-0.5)*CellSize;...
            -NcellY/2*CellSize+(j-0.5)*CellSize];
        inclusions(k).r = Diameter/2;
    end
end

%% Plot inclusions
if strcmp(callmode,'v')
    handle = figure(1);clf,hold all,axis equal,box on;
    plot(SizeX*[-1 1 1 -1 -1],SizeY*[-1 -1 1 1 -1],'k','linewidth',2);
    phi = linspace(0,2*pi,100);
    for i = 1:length(inclusions)
        plot(inclusions(i).p(1)+inclusions(i).r*cos(phi),...
            inclusions(i).p(2)+inclusions(i).r*sin(phi),'k',...
            'linewidth',2);
    end
    xlabel('$X_1$'),ylabel('$X_2$');
    set(handle,'name','Inclusions','numbertitle','off');
end
fprintf('in %g s\n\n',toc);

end