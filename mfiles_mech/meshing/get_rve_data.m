function nmodes = get_rve_data(CellSize,rvetype,inclusiontype,DiamInner,DiamOuter,...
    mhmax,modetype,modeshape,material,melemtype,mngauss,MaxNiter,TOL_g,TOL_r,...
    maxNumThreads,callmode)

%% Get domain and triangulation
switch rvetype
    case 'rect_square' % rectangular rve, square packing, circles
        nmodes = 1;
        % RVE settings
        mNcell = 2; % sqruare RVE size
        mSizeX = mNcell*CellSize/2; % specifies half size of the domain along x-axis (assumes center in [0,0])
        mSizeY = mNcell*CellSize/2; % specifies half size of the domain along x-axis (assumes center in [0,0])
        Q = 2*mSizeX*2*mSizeY;
        mShift = 0;
        % Init inclusions and mesh
        % inclusions = init_inclusions(mSizeX,mSizeY,mNcell,mNcell,...
        %     CellSize,DiamInner,callmode);
        % [pm,tm] = init_gmsh_mve_opencascade(mSizeX,mSizeY,[0;0],inclusions,mhmax,...
        %     material,melemtype,mngauss,TOL_g,callmode,maxNumThreads);
        [pm,tm] = init_gmsh_mve_opencascade_sym(CellSize,DiamInner,mhmax,material,...
            melemtype,mngauss,TOL_g,callmode,maxNumThreads);
    case 'rect_hexa' % rectangular rve, hexagonal packing, circles/hexagons
        nmodes = 3;
        % Initiate mesh
        [pm,tm,mSizeX,mSizeY] = init_gmsh_rve_hexagonal_rectangle(DiamOuter,DiamInner,...
            inclusiontype,mhmax,material,melemtype,TOL_g,callmode);
        Q = 2*mSizeX*2*mSizeY;
    case 'skew_hexa' % skewed rve, hexagonal packing, circles/hexagons
        nmodes = 3;
        Q = 4*3*sqrt(3)/2*(sin(pi/6)*DiamOuter)^2;
        % Init mesh
        % [pm,tm] = init_gmsh_rve_hexagonal_packing(DiamOuter,DiamInner,InclusionType,...
        %     mhmax,melemtype,callmode);
        % [pm,tm] = init_gmsh_rve_hexagonal_sym(DiamOuter,DiamInner,inclusiontype,...
        %     mhmax,material,melemtype,TOL_g,callmode);
        [pm,tm] = init_gmsh_rve_hexagonal_mirr(DiamOuter,DiamInner,...
            inclusiontype,mhmax,material,melemtype,TOL_g,callmode);
    case 'hexa_hexa' % hexagonal rve, hexagonal packing, circles/hexagons
        nmodes = 3;
        Q = 3*sqrt(3)/2*DiamOuter^2;
        % test hexagonally shaped RVE
        % [pm,tm] = init_gmsh_hexRVE_sym(DiamOuter,DiamInner,inclusiontype,...
        %     mhmax,material,melemtype,TOL_g,callmode);
        [pm,tm] = init_gmsh_hexRVE_mirrored(DiamOuter,DiamInner,inclusiontype,...
            mhmax,material,melemtype,TOL_g,callmode);
    otherwise
        error('rvetype not supported');
end
ndof = length(pm(:));

%% Get analytic modes
if strcmp(modetype,'analytic')
    switch rvetype
        case 'rect_square' % rectangular rve, square packing, circles
            Nx = -sin(2*pi*0.5/CellSize*(pm(1,:)+pm(2,:)-mShift))-sin(2*pi*0.5/CellSize*(-pm(1,:)+pm(2,:)-mShift)); % analytical approximation of N, normalized to 1
            Ny = sin(2*pi*0.5/CellSize*(pm(1,:)+pm(2,:)-mShift))-sin(2*pi*0.5/CellSize*(-pm(1,:)+pm(2,:)-mShift));
            phi = zeros(size(pm(:))); % fluctuation function (its gradient is computed inside build_homogenized_stress)
            phi(1:2:end) = Nx;
            phi(2:2:end) = Ny;
        case 'rect_hexa' % rectangular rve, hexagonal packing, circles/hexagons
            error(['Analytic mode for ',rvetype,' not supported.']);
        case 'skew_hexa' % skewed rve, hexagonal packing, circles/hexagons
            error(['Analytic mode for ',rvetype,' not supported.']);
        case 'hexa_hexa' % hexagonal rve, hexagonal packing, circles/hexagons
            % Assembly analytic modes
            modes = zeros(length(pm(:)),3);
            % Mode 1 (the reference)
            gradvi = [1,1];
            ell = DiamOuter*sqrt(3)/2; % center-to-center distance
            modes(1:2:end,1) = sin(2*pi*pm(2,:)/(sqrt(3)*ell));
            modes(2:2:end,1) = 1/sqrt(3)*sin(2*pi*pm(1,:)/ell);
            meanPhi = build_kinematic_averages(pm,tm,mngauss,gradvi,modes(:,2),maxNumThreads);
            modes(1:2:end,1) = modes(1:2:end,1)-meanPhi(1);
            modes(2:2:end,1) = modes(2:2:end,1)-meanPhi(2);
            % Mode 2 (rotation by -60 degrees)
            theta = -pi/3;
            Rot2 = [cos(theta) -sin(theta)
                sin(theta) cos(theta)];
            rotated2 = Rot2*[modes(1:2:end,1)';modes(2:2:end,1)'];
            modes(:,2) = rotated2(:);
            tpm = Rot2*[pm(1,:);pm(2,:)];
            meanPhi = build_kinematic_averages(pm,tm,mngauss,gradvi,modes(:,2),maxNumThreads);
            modes(1:2:end,2) = modes(1:2:end,2)-meanPhi(1);
            modes(2:2:end,2) = modes(2:2:end,2)-meanPhi(2);
            [~,Uglob,~,~,~,~] = sample_UFP_2d(tpm,tm,material,mngauss,modes(:,2),...
                pm(1,:),pm(2,:),TOL_g,0,maxNumThreads);
            Uglob = [Uglob(1:2:end)';Uglob(2:2:end)'];
            modes(:,2) = Uglob(:);
            % Mode 3 (rotation by +60 degrees)
            theta = pi/3;
            Rot3 = [cos(theta) -sin(theta)
                sin(theta) cos(theta)];
            rotated3 = Rot3*[modes(1:2:end,1)';modes(2:2:end,1)'];
            modes(:,3) = rotated3(:);
            tpm = Rot3*[pm(1,:);pm(2,:)];
            meanPhi = build_kinematic_averages(pm,tm,mngauss,gradvi,modes(:,3),maxNumThreads);
            modes(1:2:end,3) = modes(1:2:end,3)-meanPhi(1);
            modes(2:2:end,3) = modes(2:2:end,3)-meanPhi(2);
            [~,Uglob,~,~,~,~] = sample_UFP_2d(tpm,tm,material,mngauss,modes(:,3),...
                pm(1,:),pm(2,:),TOL_g,0,maxNumThreads);
            Uglob = [Uglob(1:2:end)';Uglob(2:2:end)'];
            modes(:,3) = Uglob(:);
            % Choose between modes and patterns
            if strcmp(modeshape,'mode')
                phi = modes;
            elseif strcmp(modeshape,'pattern')
                phi = [modes(:,1),modes(:,2)+modes(:,3),modes(:,1)+modes(:,2)+modes(:,3)];
            end
    end
end

%% Get master-slave relations Ccon and periodicity constraints Cpbc
if strcmp(rvetype,'rect_square') || strcmp(rvetype,'rect_hexa') % rectangular rves
    % Get boundary nodes
    % Corner IDs for rect_square
    id1 = find(pm(1,:)<-mSizeX+TOL_g & pm(2,:)<-mSizeY+TOL_g); % bottom left
    id2 = find(pm(1,:)>mSizeX-TOL_g & pm(2,:)<-mSizeY+TOL_g); % bottom right
    id3 = find(pm(1,:)>mSizeX-TOL_g & pm(2,:)>mSizeY-TOL_g); % top right
    id4 = find(pm(1,:)<-mSizeX+TOL_g & pm(2,:)>mSizeY-TOL_g); % top left
    % Additional internal IDs for extra constraints to compute patterns and modes
    if strcmp(rvetype,'rect_hexa')
        hx = DiamOuter/2*cos(pi/6);
        hy = 3*DiamOuter/2*sin(pi/6);
        id5 = find(abs(pm(1,:))<TOL_g & abs(pm(2,:))<TOL_g); % mid-point
        id6 = find(pm(1,:)<-mSizeX+TOL_g & abs(pm(2,:))<TOL_g); % mid-left
        id7 = find(abs(pm(1,:))<TOL_g & pm(2,:)<-mSizeY+TOL_g); % mid-bottom
        id8 = find(abs(pm(1,:)+hx)<TOL_g & abs(pm(2,:)+hy)<TOL_g); % bottom left (not corner)
        id9 = find(abs(pm(1,:)-hx)<TOL_g & abs(pm(2,:)-hy)<TOL_g); % top right (not corner)
        id10 = find(abs(pm(1,:)+hx)<TOL_g & abs(pm(2,:)-hy)<TOL_g); % bottom right (not corner)
        id11 = find(abs(pm(1,:)-hx)<TOL_g & abs(pm(2,:)+hy)<TOL_g); % top left (not corner)
    end
    
    % Get Cpbc
    IDGamma_1pbc = find(pm(2,:)<-mSizeY+TOL_g);
    IDGamma_2pbc = find(pm(1,:)>mSizeX-TOL_g & abs(pm(2,:))<mSizeY-TOL_g);
    IDGamma_3pbc = find(pm(2,:)>mSizeY-TOL_g);
    IDGamma_4pbc = find(pm(1,:)<-mSizeX+TOL_g & abs(pm(2,:))<mSizeY-TOL_g);
    [~,ids] = sort(pm(1,IDGamma_1pbc));
    IDGamma_1pbc = IDGamma_1pbc(ids);
    [~,ids] = sort(pm(1,IDGamma_3pbc));
    IDGamma_3pbc = IDGamma_3pbc(ids);
    [~,ids] = sort(pm(2,IDGamma_2pbc));
    IDGamma_2pbc = IDGamma_2pbc(ids);
    [~,ids] = sort(pm(2,IDGamma_4pbc));
    IDGamma_4pbc = IDGamma_4pbc(ids);
    IDGamma = unique([IDGamma_1pbc,IDGamma_2pbc,IDGamma_3pbc,IDGamma_4pbc])';
    % Get tying matrix
    ICpbc = zeros(4+2*2*(length(IDGamma_1pbc)+length(IDGamma_2pbc)),1);
    JCpbc = zeros(4+2*2*(length(IDGamma_1pbc)+length(IDGamma_2pbc)),1);
    SCpbc = zeros(4+2*2*(length(IDGamma_1pbc)+length(IDGamma_2pbc)),1);
    % Dependencies for vertical boundary segments
    for i = 1:length(IDGamma_2pbc)
        % x-direction
        ICpbc((i-1)*4+1:i*4-2) = (2*i-1)*[1,1];
        JCpbc((i-1)*4+1:i*4-2) = [2*IDGamma_2pbc(i)-1,2*IDGamma_4pbc(i)-1];
        SCpbc((i-1)*4+1:i*4-2) = [1,-1];
        % y-direction
        ICpbc((i-1)*4+1+2:i*4) = 2*i*[1,1];
        JCpbc((i-1)*4+1+2:i*4) = [2*IDGamma_2pbc(i),2*IDGamma_4pbc(i)];
        SCpbc((i-1)*4+1+2:i*4) = [1,-1];
    end
    % Dependencies for vertical boundary segments
    for j = 1:length(IDGamma_3pbc)
        % x-direction
        ICpbc((i+j-1)*4+1:(i+j)*4-2) = (2*(i+j)-1)*[1,1];
        JCpbc((i+j-1)*4+1:(i+j)*4-2) = [2*IDGamma_3pbc(j)-1,2*IDGamma_1pbc(j)-1];
        SCpbc((i+j-1)*4+1:(i+j)*4-2) = [1,-1];
        % y-direction
        ICpbc((i+j-1)*4+1+2:(i+j)*4) = 2*(i+j)*[1,1];
        JCpbc((i+j-1)*4+1+2:(i+j)*4) = [2*IDGamma_3pbc(j),2*IDGamma_1pbc(j)];
        SCpbc((i+j-1)*4+1+2:(i+j)*4) = [1,-1];
    end
    % Dependency for the extra corner node
    j = j+1;
    % x-direction
    ICpbc((i+j-1)*4+1:(i+j)*4-2) = (2*(i+j)-1)*[1,1];
    JCpbc((i+j-1)*4+1:(i+j)*4-2) = [2*id4-1,2*id3-1];
    SCpbc((i+j-1)*4+1:(i+j)*4-2) = [1,-1];
    % y-direction
    ICpbc((i+j-1)*4+1+2:(i+j)*4) = 2*(i+j)*[1,1];
    JCpbc((i+j-1)*4+1+2:(i+j)*4) = [2*id4,2*id3];
    SCpbc((i+j-1)*4+1+2:(i+j)*4) = [1,-1];
    % Assemble matrix
    Cpbc = sparse(ICpbc,JCpbc,SCpbc,2+2*(length(IDGamma_1pbc)+length(IDGamma_2pbc)),ndof);
    
    % Get Ccon
    IDFCON = [id1;id2;id4]; % fixed nodes (three RVE control points)
    IDGamma_1con = find(pm(1,:)>-mSizeX+TOL_g & pm(2,:)<-mSizeY+TOL_g);
    IDGamma_2con = find(pm(1,:)>mSizeX-TOL_g & abs(pm(2,:))<mSizeY-TOL_g);
    IDGamma_3con = find(pm(1,:)>-mSizeX+TOL_g & pm(2,:)>mSizeY-TOL_g);
    IDGamma_4con = find(pm(1,:)<-mSizeX+TOL_g & abs(pm(2,:))<mSizeY-TOL_g);
    [~,ids] = sort(pm(1,IDGamma_1con));
    IDGamma_1con = IDGamma_1con(ids);
    [~,ids] = sort(pm(1,IDGamma_3con));
    IDGamma_3con = IDGamma_3con(ids);
    [~,ids] = sort(pm(2,IDGamma_2con));
    IDGamma_2con = IDGamma_2con(ids);
    [~,ids] = sort(pm(2,IDGamma_4con));
    IDGamma_4con = IDGamma_4con(ids);
    % Get dependency matrix
    ICcon = zeros(2*3*(length(IDGamma_1con)+length(IDGamma_2con)),1);
    JCcon = zeros(2*3*(length(IDGamma_1con)+length(IDGamma_2con)),1);
    SCcon = zeros(2*3*(length(IDGamma_1con)+length(IDGamma_2con)),1);
    DepIndicesCON = zeros(2*(length(IDGamma_1con)+length(IDGamma_2con)),1);
    % Dependencies for vertical boundary segments
    for i = 1:length(IDGamma_2con)
        % x-direction
        ICcon((i-1)*6+1:i*6-3) = (2*i-1)*[1,1,1];
        JCcon((i-1)*6+1:i*6-3) = [2*IDGamma_4con(i)-1,2*id2-1,2*id1-1];
        SCcon((i-1)*6+1:i*6-3) = [1,1,-1];
        % y-direction
        ICcon((i-1)*6+1+3:i*6) = 2*i*[1,1,1];
        JCcon((i-1)*6+1+3:i*6) = [2*IDGamma_4con(i),2*id2,2*id1];
        SCcon((i-1)*6+1+3:i*6) = [1,1,-1];
        % Assign dependent indices to ordering of Ca
        DepIndicesCON(2*i-1) = 2*IDGamma_2con(i)-1;
        DepIndicesCON(2*i) = 2*IDGamma_2con(i);
    end
    % Dependencies for vertical boundary segments
    for j = 1:length(IDGamma_3con)
        % x-direction
        ICcon((i+j-1)*6+1:(i+j)*6-3) = (2*(i+j)-1)*[1,1,1];
        JCcon((i+j-1)*6+1:(i+j)*6-3) = [2*IDGamma_1con(j)-1,2*id4-1,2*id1-1];
        SCcon((i+j-1)*6+1:(i+j)*6-3) = [1,1,-1];
        % y-direction
        ICcon((i+j-1)*6+1+3:(i+j)*6) = 2*(i+j)*[1,1,1];
        JCcon((i+j-1)*6+1+3:(i+j)*6) = [2*IDGamma_1con(j),2*id4,2*id1];
        SCcon((i+j-1)*6+1+3:(i+j)*6) = [1,1,-1];
        % Assign dependent indices to ordering of Ca
        DepIndicesCON(2*(i+j)-1) = 2*IDGamma_3con(j)-1;
        DepIndicesCON(2*(i+j)) = 2*IDGamma_3con(j);
    end
    % Assemble matrix
    Ccon = sparse(ICcon,JCcon,SCcon,2*(length(IDGamma_1con)+length(IDGamma_2con)),ndof);
elseif strcmp(rvetype,'skew_hexa') % shewed hexagonal rve
    % Get boundary nodes
    theta0 = -pi/2;
    dtheta = pi/6;
    P01 = [-1.5,-sqrt(3)/2]*DiamOuter*sqrt(3)/4;
    P02 = [0.5,-sqrt(3)/2]*DiamOuter*sqrt(3)/4;
    P03 = [1.5,sqrt(3)/2]*DiamOuter*sqrt(3)/4;
    P04 = [-0.5,sqrt(3)/2]*DiamOuter*sqrt(3)/4;
    P1 = P01+DiamOuter/2*[cos(theta0+5*2*dtheta),sin(theta0+5*2*dtheta)];
    P2 = P02+DiamOuter/2*[cos(theta0+1*2*dtheta),sin(theta0+1*2*dtheta)];
    P3 = P03+DiamOuter/2*[cos(theta0+2*2*dtheta),sin(theta0+2*2*dtheta)];
    P4 = P04+DiamOuter/2*[cos(theta0+3*2*dtheta),sin(theta0+3*2*dtheta)];
    P5 = P02+DiamOuter/2*[cos(theta0+3*2*dtheta),sin(theta0+3*2*dtheta)];
    P6 = P02+DiamOuter/2*[cos(theta0+4*2*dtheta),sin(theta0+4*2*dtheta)];
    P7 = P01+DiamOuter/2*[cos(theta0+3*2*dtheta),sin(theta0+3*2*dtheta)];
    P8 = P03+DiamOuter/2*[cos(theta0+1*2*dtheta),sin(theta0+1*2*dtheta)];
    P9 = P01+DiamOuter/2*[cos(theta0+1*2*dtheta),sin(theta0+1*2*dtheta)];
    P10 = P03+DiamOuter/2*[cos(theta0+3*2*dtheta),sin(theta0+3*2*dtheta)];
    P11 = P01+DiamOuter/2*[cos(theta0+4*2*dtheta),sin(theta0+4*2*dtheta)];
    P12 = P04+DiamOuter/2*[cos(theta0+4*2*dtheta),sin(theta0+4*2*dtheta)];
    P13 = P02+DiamOuter/2*[cos(theta0+0*2*dtheta),sin(theta0+0*2*dtheta)];
    id1 = find(abs(pm(1,:)-P1(1))+abs(pm(2,:)-P1(2))<TOL_g); % bottom left
    id2 = find(abs(pm(1,:)-P2(1))+abs(pm(2,:)-P2(2))<TOL_g); % bottom right
    id3 = find(abs(pm(1,:)-P3(1))+abs(pm(2,:)-P3(2))<TOL_g); % top right
    id4 = find(abs(pm(1,:)-P4(1))+abs(pm(2,:)-P4(2))<TOL_g); % top left
    id5 = find(abs(pm(1,:)-P5(1))+abs(pm(2,:)-P5(2))<TOL_g); % center right
    id6 = find(abs(pm(1,:)-P6(1))+abs(pm(2,:)-P6(2))<TOL_g); % center left
    id7 = find(abs(pm(1,:)-P7(1))+abs(pm(2,:)-P7(2))<TOL_g); % middle left
    id8 = find(abs(pm(1,:)-P8(1))+abs(pm(2,:)-P8(2))<TOL_g); % middle right
    id9 = find(abs(pm(1,:)-P9(1))+abs(pm(2,:)-P9(2))<TOL_g); % center bottom
    id10 = find(abs(pm(1,:)-P10(1))+abs(pm(2,:)-P10(2))<TOL_g); % center top
    id11 = find(abs(pm(1,:)-P11(1))+abs(pm(2,:)-P11(2))<TOL_g);
    id12 = find(abs(pm(1,:)-P12(1))+abs(pm(2,:)-P12(2))<TOL_g);
    id13 = find(abs(pm(1,:)-P13(1))+abs(pm(2,:)-P13(2))<TOL_g);
    p1 = [pm(1,:)-P01(1);pm(2,:)-P01(2)];
    p2 = [pm(1,:)-P02(1);pm(2,:)-P02(2)];
    p3 = [pm(1,:)-P03(1);pm(2,:)-P03(2)];
    p4 = [pm(1,:)-P04(1);pm(2,:)-P04(2)];
    
    % Get Cpbc
    % Gamma_1
    IDGamma_1pbc = [];
    theta = dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p1;
    idedge = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_1pbc = union(IDGamma_1pbc,idedge);
    theta = -dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p1;
    idedge = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_1pbc = union(IDGamma_1pbc,idedge);
    theta = dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p2;
    idedge = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_1pbc = union(IDGamma_1pbc,idedge);
    [~,ids] = sort(pm(1,IDGamma_1pbc));
    IDGamma_1pbc = IDGamma_1pbc(ids);
    IDGamma_1pbc = IDGamma_1pbc(2:end-1);
    % IDGamma_2
    theta = -dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p2;
    IDGamma_2pbc = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    [~,ids] = sort(pm(1,IDGamma_2pbc));
    IDGamma_2pbc = IDGamma_2pbc(ids)';
    % Gamma_3
    IDGamma_3pbc = [];
    theta = -3*dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p2;
    idedge = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_3pbc = union(IDGamma_3pbc,idedge);
    theta = -dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p3;
    idedge = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_3pbc = union(IDGamma_3pbc,idedge);
    theta = -3*dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p3;
    idedge = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_3pbc = union(IDGamma_3pbc,idedge);
    [~,ids] = sort(pm(2,IDGamma_3pbc));
    IDGamma_3pbc = IDGamma_3pbc(ids);
    IDGamma_3pbc = IDGamma_3pbc(2:end-1);
    % Gamma_4
    IDGamma_4pbc = [];
    theta = dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p3;
    idedge = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_4pbc = union(IDGamma_4pbc,idedge);
    theta = -dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p3;
    idedge = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_4pbc = union(IDGamma_4pbc,idedge);
    theta = dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p4;
    idedge = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_4pbc = union(IDGamma_4pbc,idedge);
    [~,ids] = sort(pm(1,IDGamma_4pbc));
    IDGamma_4pbc = IDGamma_4pbc(ids);
    IDGamma_4pbc = IDGamma_4pbc(2:end-1);
    % IDGamma_5
    theta = -dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p4;
    IDGamma_5pbc = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    [~,ids] = sort(pm(1,IDGamma_5pbc));
    IDGamma_5pbc = IDGamma_5pbc(ids)';
    % Gamma_6
    IDGamma_6pbc = [];
    theta = -3*dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p4;
    idedge = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_6pbc = union(IDGamma_6pbc,idedge);
    theta = -dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p1;
    idedge = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_6pbc = union(IDGamma_6pbc,idedge);
    theta = -3*dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p1;
    idedge = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_6pbc = union(IDGamma_6pbc,idedge);
    [~,ids] = sort(pm(2,IDGamma_6pbc));
    IDGamma_6pbc = IDGamma_6pbc(ids);
    IDGamma_6pbc = IDGamma_6pbc(2:end-1);
    % Get tying matrix
    ICpbc = zeros(8+2*2*(length(IDGamma_1pbc)+length(IDGamma_2pbc)+length(IDGamma_3pbc)),1);
    JCpbc = zeros(8+2*2*(length(IDGamma_1pbc)+length(IDGamma_2pbc)+length(IDGamma_3pbc)),1);
    SCpbc = zeros(8+2*2*(length(IDGamma_1pbc)+length(IDGamma_2pbc)+length(IDGamma_3pbc)),1);
    % Dependencies for Gamma_1 and Gamma_4
    for i = 1:length(IDGamma_1pbc)
        % x-direction
        ICpbc((i-1)*4+1:i*4-2) = (2*i-1)*[1,1];
        JCpbc((i-1)*4+1:i*4-2) = [2*IDGamma_1pbc(i)-1,2*IDGamma_4pbc(i)-1];
        SCpbc((i-1)*4+1:i*4-2) = [1,-1];
        % y-direction
        ICpbc((i-1)*4+1+2:i*4) = 2*i*[1,1];
        JCpbc((i-1)*4+1+2:i*4) = [2*IDGamma_1pbc(i),2*IDGamma_4pbc(i)];
        SCpbc((i-1)*4+1+2:i*4) = [1,-1];
    end
    % Dependencies for Gamma_2 and Gamma_5
    for j = 1:length(IDGamma_2pbc)
        % x-direction
        ICpbc((i+j-1)*4+1:(i+j)*4-2) = (2*(i+j)-1)*[1,1];
        JCpbc((i+j-1)*4+1:(i+j)*4-2) = [2*IDGamma_2pbc(j)-1,2*IDGamma_5pbc(j)-1];
        SCpbc((i+j-1)*4+1:(i+j)*4-2) = [1,-1];
        % y-direction
        ICpbc((i+j-1)*4+1+2:(i+j)*4) = 2*(i+j)*[1,1];
        JCpbc((i+j-1)*4+1+2:(i+j)*4) = [2*IDGamma_2pbc(j),2*IDGamma_5pbc(j)];
        SCpbc((i+j-1)*4+1+2:(i+j)*4) = [1,-1];
    end
    % Dependencies for Gamma_3 and Gamma_6
    for k = 1:length(IDGamma_3pbc)
        % x-direction
        ICpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = (2*(i+j+k)-1)*[1,1];
        JCpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = [2*IDGamma_3pbc(k)-1,2*IDGamma_6pbc(k)-1];
        SCpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = [1,-1];
        % y-direction
        ICpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = 2*(i+j+k)*[1,1];
        JCpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = [2*IDGamma_3pbc(k),2*IDGamma_6pbc(k)];
        SCpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = [1,-1];
    end
    % Dependency for the extra corner nodes
    k = k+1;
    % x-direction
    ICpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = (2*(i+j+k)-1)*[1,1];
    JCpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = [2*id3-1,2*id12-1];
    SCpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = [1,-1];
    % y-direction
    ICpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = 2*(i+j+k)*[1,1];
    JCpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = [2*id3,2*id12];
    SCpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = [1,-1];
    k = k+1;
    % x-direction
    ICpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = (2*(i+j+k)-1)*[1,1];
    JCpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = [2*id1-1,2*id2-1];
    SCpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = [1,-1];
    % y-direction
    ICpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = 2*(i+j+k)*[1,1];
    JCpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = [2*id1,2*id2];
    SCpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = [1,-1];
    % Assemble matrix
    Cpbc = sparse(ICpbc,JCpbc,SCpbc,4+2*(length(IDGamma_1pbc)+length(IDGamma_2pbc)+length(IDGamma_3pbc)),ndof);
    
    % Get Ccon
    IDFCON = [id1;id2;id4];
    % Gamma_1
    IDGamma_1con = [];
    theta = dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p1;
    idedge = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_1con = union(IDGamma_1con,idedge);
    theta = -dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p1;
    idedge = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_1con = union(IDGamma_1con,idedge);
    theta = dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p2;
    idedge = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_1con = union(IDGamma_1con,idedge);
    [~,ids] = sort(pm(1,IDGamma_1con));
    IDGamma_1con = IDGamma_1con(ids);
    IDGamma_1con = IDGamma_1con(2:end-1);
    % IDGamma_2
    theta = -dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p2;
    IDGamma_2con = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    [~,ids] = sort(pm(1,IDGamma_2con));
    IDGamma_2con = IDGamma_2con(ids)';
    IDGamma_2con = IDGamma_2con(1:end-1);
    % Gamma_3
    IDGamma_3con = [];
    theta = -3*dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p2;
    idedge = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_3con = union(IDGamma_3con,idedge);
    theta = -dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p3;
    idedge = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_3con = union(IDGamma_3con,idedge);
    theta = -3*dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p3;
    idedge = find(abs(rp(2,:)+DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_3con = union(IDGamma_3con,idedge);
    [~,ids] = sort(pm(2,IDGamma_3con));
    IDGamma_3con = IDGamma_3con(ids);
    IDGamma_3con = IDGamma_3con(2:end);
    % Gamma_4
    IDGamma_4con = [];
    theta = dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p3;
    idedge = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_4con = union(IDGamma_4con,idedge);
    theta = -dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p3;
    idedge = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_4con = union(IDGamma_4con,idedge);
    theta = dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p4;
    idedge = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_4con = union(IDGamma_4con,idedge);
    [~,ids] = sort(pm(1,IDGamma_4con));
    IDGamma_4con = IDGamma_4con(ids);
    IDGamma_4con = IDGamma_4con(2:end-1);
    % IDGamma_5
    theta = -dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p4;
    IDGamma_5con = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    [~,ids] = sort(pm(1,IDGamma_5con));
    IDGamma_5con = IDGamma_5con(ids)';
    IDGamma_5con = IDGamma_5con(1:end-1);
    % Gamma_6
    IDGamma_6con = [];
    theta = -3*dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p4;
    idedge = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_6con = union(IDGamma_6con,idedge);
    theta = -dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p1;
    idedge = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_6con = union(IDGamma_6con,idedge);
    theta = -3*dtheta;
    Rot = [cos(theta) -sin(theta);sin(theta) cos(theta)];
    rp = Rot*p1;
    idedge = find(abs(rp(2,:)-DiamOuter/2*cos(dtheta))<TOL_g & ...
        abs(rp(1,:))<DiamOuter/2*sin(dtheta)+TOL_g);
    IDGamma_6con = union(IDGamma_6con,idedge);
    [~,ids] = sort(pm(2,IDGamma_6con));
    IDGamma_6con = IDGamma_6con(ids);
    IDGamma_6con = IDGamma_6con(2:end);
    IDGamma = unique([IDFCON;IDGamma_1con;IDGamma_2con;IDGamma_3con;IDGamma_4con;IDGamma_5con;IDGamma_6con]);
    % Get dependency matrix
    ICcon = zeros(2*3*(length(IDGamma_1con)+length(IDGamma_2con)+length(IDGamma_3con)),1);
    JCcon = zeros(size(ICcon));
    SCcon = zeros(size(ICcon));
    DepIndicesCON = zeros(2*(length(IDGamma_1con)+length(IDGamma_2con)+length(IDGamma_3con)),1);
    % Dependencies for Gamma_1 and Gamma_4
    for i = 1:length(IDGamma_1con)
        % x-direction
        ICcon((i-1)*6+1:i*6-3) = (2*i-1)*[1,1,1];
        JCcon((i-1)*6+1:i*6-3) = [2*IDGamma_4con(i)-1,2*id4-1,2*id1-1];
        SCcon((i-1)*6+1:i*6-3) = [1,-1,1];
        % y-direction
        ICcon((i-1)*6+1+3:i*6) = 2*i*[1,1,1];
        JCcon((i-1)*6+1+3:i*6) = [2*IDGamma_4con(i),2*id4,2*id1];
        SCcon((i-1)*6+1+3:i*6) = [1,-1,1];
        % Assign dependent indices to ordering of C
        DepIndicesCON(2*i-1) = 2*IDGamma_1con(i)-1;
        DepIndicesCON(2*i) = 2*IDGamma_1con(i);
    end
    % Dependencies for Gamma_2 and Gamma_5
    for j = 1:length(IDGamma_2con)
        % x-direction
        ICcon((i+j-1)*6+1:(i+j)*6-3) = (2*(i+j)-1)*[1,1,1];
        JCcon((i+j-1)*6+1:(i+j)*6-3) = [2*IDGamma_5con(j)-1,2*id4-1,2*id2-1];
        SCcon((i+j-1)*6+1:(i+j)*6-3) = [1,-1,1];
        % y-direction
        ICcon((i+j-1)*6+1+3:(i+j)*6) = 2*(i+j)*[1,1,1];
        JCcon((i+j-1)*6+1+3:(i+j)*6) = [2*IDGamma_5con(j),2*id4,2*id2];
        SCcon((i+j-1)*6+1+3:(i+j)*6) = [1,-1,1];
        % Assign dependent indices to ordering of C
        DepIndicesCON(2*(i+j)-1) = 2*IDGamma_2con(j)-1;
        DepIndicesCON(2*(i+j)) = 2*IDGamma_2con(j);
    end
    % Dependencies for Gamma_3 and Gamma_6
    for k = 1:length(IDGamma_3con)
        % x-direction
        ICcon((i+j+k-1)*6+1:(i+j+k)*6-3) = (2*(i+j+k)-1)*[1,1,1];
        JCcon((i+j+k-1)*6+1:(i+j+k)*6-3) = [2*IDGamma_6con(k)-1,2*id2-1,2*id1-1];
        SCcon((i+j+k-1)*6+1:(i+j+k)*6-3) = [1,1,-1];
        % y-direction
        ICcon((i+j+k-1)*6+1+3:(i+j+k)*6) = 2*(i+j+k)*[1,1,1];
        JCcon((i+j+k-1)*6+1+3:(i+j+k)*6) = [2*IDGamma_6con(k),2*id2,2*id1];
        SCcon((i+j+k-1)*6+1+3:(i+j+k)*6) = [1,1,-1];
        % Assign dependent indices to ordering of C
        DepIndicesCON(2*(i+j+k)-1) = 2*IDGamma_3con(k)-1;
        DepIndicesCON(2*(i+j+k)) = 2*IDGamma_3con(k);
    end
    % Assemble matrix
    Ccon = sparse(ICcon,JCcon,SCcon,2*(length(IDGamma_1con)+length(IDGamma_2con)+length(IDGamma_3con)),ndof);
elseif strcmp(rvetype,'hexa_hexa') % hexagonal rve
    % Get boundary nodes
    theta0 = -pi/2;
    r = DiamOuter;
    P0 = [0,0];
    P1 = P0+r*[cos(theta0+0*pi/3),sin(theta0+0*pi/3)];
    P2 = P0+r*[cos(theta0+1*pi/3),sin(theta0+1*pi/3)];
    P3 = P0+r*[cos(theta0+2*pi/3),sin(theta0+2*pi/3)];
    P4 = P0+r*[cos(theta0+3*pi/3),sin(theta0+3*pi/3)];
    P5 = P0+r*[cos(theta0+4*pi/3),sin(theta0+4*pi/3)];
    P6 = P0+r*[cos(theta0+5*pi/3),sin(theta0+5*pi/3)];
    P11 = P0+r/2*[cos(theta0+0*pi/3),sin(theta0+0*pi/3)];
    P12 = P0+r/2*[cos(theta0+1*pi/3),sin(theta0+1*pi/3)];
    P13 = P0+r/2*[cos(theta0+2*pi/3),sin(theta0+2*pi/3)];
    P14 = P0+r/2*[cos(theta0+3*pi/3),sin(theta0+3*pi/3)];
    P15 = P0+r/2*[cos(theta0+4*pi/3),sin(theta0+4*pi/3)];
    P16 = P0+r/2*[cos(theta0+5*pi/3),sin(theta0+5*pi/3)];
    id6 = find(abs(pm(1,:)-P1(1))+abs(pm(2,:)-P1(2))<TOL_g); % bottom centre
    id2 = find(abs(pm(1,:)-P2(1))+abs(pm(2,:)-P2(2))<TOL_g); % bottom right
    id3 = find(abs(pm(1,:)-P3(1))+abs(pm(2,:)-P3(2))<TOL_g); % top right
    id4 = find(abs(pm(1,:)-P4(1))+abs(pm(2,:)-P4(2))<TOL_g); % top centre
    id5 = find(abs(pm(1,:)-P5(1))+abs(pm(2,:)-P5(2))<TOL_g); % top left
    id1 = find(abs(pm(1,:)-P6(1))+abs(pm(2,:)-P6(2))<TOL_g); % bottom left
    id11 = find(abs(pm(1,:)-P11(1))+abs(pm(2,:)-P11(2))<TOL_g);
    id12 = find(abs(pm(1,:)-P12(1))+abs(pm(2,:)-P12(2))<TOL_g);
    id13 = find(abs(pm(1,:)-P13(1))+abs(pm(2,:)-P13(2))<TOL_g);
    id14 = find(abs(pm(1,:)-P14(1))+abs(pm(2,:)-P14(2))<TOL_g);
    id15 = find(abs(pm(1,:)-P15(1))+abs(pm(2,:)-P15(2))<TOL_g);
    id16 = find(abs(pm(1,:)-P16(1))+abs(pm(2,:)-P16(2))<TOL_g);
    
    % Get Cpbc
    % Gamma_1
    theta = -pi/3;
    Rot = [cos(theta) -sin(theta)
        sin(theta) cos(theta)];
    tpm = Rot*[pm(1,:);pm(2,:)];
    IDGamma_1pbc = find(tpm(1,:)<-(DiamOuter*sqrt(3)/2)+TOL_g & tpm(2,:)<DiamOuter-TOL_g)';
    [~,ids] = sort(pm(1,IDGamma_1pbc));
    IDGamma_1pbc = IDGamma_1pbc(ids);
    % IDGamma_2
    theta = pi/3;
    Rot = [cos(theta) -sin(theta)
        sin(theta) cos(theta)];
    tpm = Rot*[pm(1,:);pm(2,:)];
    IDGamma_2pbc = find(tpm(1,:)>(DiamOuter*sqrt(3)/2)-TOL_g & abs(tpm(2,:))<DiamOuter-TOL_g);
    [~,ids] = sort(pm(1,IDGamma_2pbc));
    IDGamma_2pbc = IDGamma_2pbc(ids)';
    IDGamma_2pbc = IDGamma_2pbc(2:end-1);
    % Gamma_3
    IDGamma_3pbc = find(pm(1,:)>(DiamOuter*sqrt(3)/2)-TOL_g & abs(pm(2,:))<DiamOuter-TOL_g)';
    [~,ids] = sort(pm(2,IDGamma_3pbc));
    IDGamma_3pbc = IDGamma_3pbc(ids);
    IDGamma_3pbc = IDGamma_3pbc(2:end-1);
    % Gamma_4
    theta = -pi/3;
    Rot = [cos(theta) -sin(theta)
        sin(theta) cos(theta)];
    tpm = Rot*[pm(1,:);pm(2,:)];
    IDGamma_4pbc = find(tpm(1,:)>(DiamOuter*sqrt(3)/2)-TOL_g & tpm(2,:)<DiamOuter-TOL_g)';
    [~,ids] = sort(pm(1,IDGamma_4pbc));
    IDGamma_4pbc = IDGamma_4pbc(ids);
    % IDGamma_5
    theta = pi/3;
    Rot = [cos(theta) -sin(theta)
        sin(theta) cos(theta)];
    tpm = Rot*[pm(1,:);pm(2,:)];
    IDGamma_5pbc = find(tpm(1,:)<-(DiamOuter*sqrt(3)/2)+TOL_g & abs(tpm(2,:))<DiamOuter-TOL_g);
    [~,ids] = sort(pm(1,IDGamma_5pbc));
    IDGamma_5pbc = IDGamma_5pbc(ids)';
    IDGamma_5pbc = IDGamma_5pbc(2:end-1);
    % Gamma_6
    IDGamma_6pbc = find(pm(1,:)<-(DiamOuter*sqrt(3)/2)+TOL_g & abs(pm(2,:))<DiamOuter-TOL_g)';
    [~,ids] = sort(pm(2,IDGamma_6pbc));
    IDGamma_6pbc = IDGamma_6pbc(ids);
    IDGamma_6pbc = IDGamma_6pbc(2:end-1);
    % Dependency matrix
    ICpbc = zeros(8+2*2*(length(IDGamma_1pbc)+length(IDGamma_2pbc)+length(IDGamma_3pbc)),1);
    JCpbc = zeros(8+2*2*(length(IDGamma_1pbc)+length(IDGamma_2pbc)+length(IDGamma_3pbc)),1);
    SCpbc = zeros(8+2*2*(length(IDGamma_1pbc)+length(IDGamma_2pbc)+length(IDGamma_3pbc)),1);
    % Dependencies for Gamma_1 and Gamma_4
    for i = 1:length(IDGamma_1pbc)
        % x-direction
        ICpbc((i-1)*4+1:i*4-2) = (2*i-1)*[1,1];
        JCpbc((i-1)*4+1:i*4-2) = [2*IDGamma_1pbc(i)-1,2*IDGamma_4pbc(i)-1];
        SCpbc((i-1)*4+1:i*4-2) = [1,-1];
        % y-direction
        ICpbc((i-1)*4+1+2:i*4) = 2*i*[1,1];
        JCpbc((i-1)*4+1+2:i*4) = [2*IDGamma_1pbc(i),2*IDGamma_4pbc(i)];
        SCpbc((i-1)*4+1+2:i*4) = [1,-1];
    end
    % Dependencies for Gamma_2 and Gamma_5
    for j = 1:length(IDGamma_2pbc)
        % x-direction
        ICpbc((i+j-1)*4+1:(i+j)*4-2) = (2*(i+j)-1)*[1,1];
        JCpbc((i+j-1)*4+1:(i+j)*4-2) = [2*IDGamma_2pbc(j)-1,2*IDGamma_5pbc(j)-1];
        SCpbc((i+j-1)*4+1:(i+j)*4-2) = [1,-1];
        % y-direction
        ICpbc((i+j-1)*4+1+2:(i+j)*4) = 2*(i+j)*[1,1];
        JCpbc((i+j-1)*4+1+2:(i+j)*4) = [2*IDGamma_2pbc(j),2*IDGamma_5pbc(j)];
        SCpbc((i+j-1)*4+1+2:(i+j)*4) = [1,-1];
    end
    % Dependencies for Gamma_3 and Gamma_6
    for k = 1:length(IDGamma_3pbc)
        % x-direction
        ICpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = (2*(i+j+k)-1)*[1,1];
        JCpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = [2*IDGamma_3pbc(k)-1,2*IDGamma_6pbc(k)-1];
        SCpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = [1,-1];
        % y-direction
        ICpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = 2*(i+j+k)*[1,1];
        JCpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = [2*IDGamma_3pbc(k),2*IDGamma_6pbc(k)];
        SCpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = [1,-1];
    end
    % Dependency for the extra corner nodes
    k = k+1;
    % x-direction
    ICpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = (2*(i+j+k)-1)*[1,1];
    JCpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = [2*id2-1,2*id1-1];
    SCpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = [1,-1];
    % y-direction
    ICpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = 2*(i+j+k)*[1,1];
    JCpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = [2*id2,2*id1];
    SCpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = [1,-1];
    k = k+1;
    % x-direction
    ICpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = (2*(i+j+k)-1)*[1,1];
    JCpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = [2*id5-1,2*id3-1];
    SCpbc((i+j+k-1)*4+1:(i+j+k)*4-2) = [1,-1];
    % y-direction
    ICpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = 2*(i+j+k)*[1,1];
    JCpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = [2*id5,2*id3];
    SCpbc((i+j+k-1)*4+1+2:(i+j+k)*4) = [1,-1];
    % Assemble matrix
    Cpbc = sparse(ICpbc,JCpbc,SCpbc,4+2*(length(IDGamma_1pbc)+length(IDGamma_2pbc)+length(IDGamma_3pbc)),ndof);
    
    % Get Ccon
    IDFCON = [id1;id2;id4]; % only three periodicity control points (for mode computation)
    % Gamma_1
    theta = -pi/3;
    Rot = [cos(theta) -sin(theta)
        sin(theta) cos(theta)];
    tpm = Rot*[pm(1,:);pm(2,:)];
    IDGamma_1con = find(tpm(1,:)<-(DiamOuter*sqrt(3)/2)+TOL_g & tpm(2,:)<DiamOuter-TOL_g)';
    [~,ids] = sort(pm(1,IDGamma_1con));
    IDGamma_1con = IDGamma_1con(ids);
    IDGamma_1con = IDGamma_1con(2:end-1);
    % IDGamma_2
    theta = pi/3;
    Rot = [cos(theta) -sin(theta)
        sin(theta) cos(theta)];
    tpm = Rot*[pm(1,:);pm(2,:)];
    IDGamma_2con = find(tpm(1,:)>(DiamOuter*sqrt(3)/2)-TOL_g & abs(tpm(2,:))<DiamOuter-TOL_g);
    [~,ids] = sort(pm(1,IDGamma_2con));
    IDGamma_2con = IDGamma_2con(ids)';
    IDGamma_2con = IDGamma_2con(1:end-1);
    % Gamma_3
    IDGamma_3con = find(pm(1,:)>(DiamOuter*sqrt(3)/2)-TOL_g & abs(pm(2,:))<DiamOuter-TOL_g)';
    [~,ids] = sort(pm(2,IDGamma_3con));
    IDGamma_3con = IDGamma_3con(ids);
    IDGamma_3con = IDGamma_3con(2:end);
    % Gamma_4
    theta = -pi/3;
    Rot = [cos(theta) -sin(theta)
        sin(theta) cos(theta)];
    tpm = Rot*[pm(1,:);pm(2,:)];
    IDGamma_4con = find(tpm(1,:)>(DiamOuter*sqrt(3)/2)-TOL_g & tpm(2,:)<DiamOuter-TOL_g)';
    [~,ids] = sort(pm(1,IDGamma_4con));
    IDGamma_4con = IDGamma_4con(ids);
    IDGamma_4con = IDGamma_4con(2:end-1);
    % IDGamma_5
    theta = pi/3;
    Rot = [cos(theta) -sin(theta)
        sin(theta) cos(theta)];
    tpm = Rot*[pm(1,:);pm(2,:)];
    IDGamma_5con = find(tpm(1,:)<-(DiamOuter*sqrt(3)/2)+TOL_g & abs(tpm(2,:))<DiamOuter-TOL_g);
    [~,ids] = sort(pm(1,IDGamma_5con));
    IDGamma_5con = IDGamma_5con(ids)';
    IDGamma_5con = IDGamma_5con(1:end-1);
    % Gamma_6
    IDGamma_6con = find(pm(1,:)<-(DiamOuter*sqrt(3)/2)+TOL_g & abs(pm(2,:))<DiamOuter-TOL_g)';
    [~,ids] = sort(pm(2,IDGamma_6con));
    IDGamma_6con = IDGamma_6con(ids);
    IDGamma_6con = IDGamma_6con(2:end);
    IDGamma = unique([IDFCON;IDGamma_1con;IDGamma_2con;IDGamma_3con;IDGamma_4con;IDGamma_5con;IDGamma_6con]);
    % Dependency matrix
    ICcon = zeros(2*3*(length(IDGamma_1con)+length(IDGamma_2con)+length(IDGamma_3con)),1);
    JCcon = zeros(2*3*(length(IDGamma_1con)+length(IDGamma_2con)+length(IDGamma_3con)),1);
    SCcon = zeros(2*3*(length(IDGamma_1con)+length(IDGamma_2con)+length(IDGamma_3con)),1);
    DepIndicesCON = zeros(2*(length(IDGamma_1con)+length(IDGamma_2con)+length(IDGamma_3con)),1);
    % Dependencies for Gamma_1 and Gamma_4
    for i = 1:length(IDGamma_1con)
        % x-direction
        ICcon((i-1)*6+1:i*6-3) = (2*i-1)*[1,1,1];
        JCcon((i-1)*6+1:i*6-3) = [2*IDGamma_4con(i)-1,2*id4-1,2*id1-1];
        SCcon((i-1)*6+1:i*6-3) = [1,-1,1];
        % y-direction
        ICcon((i-1)*6+1+3:i*6) = 2*i*[1,1,1];
        JCcon((i-1)*6+1+3:i*6) = [2*IDGamma_4con(i),2*id4,2*id1];
        SCcon((i-1)*6+1+3:i*6) = [1,-1,1];
        % Assign dependent indices to ordering of C
        DepIndicesCON(2*i-1) = 2*IDGamma_1con(i)-1;
        DepIndicesCON(2*i) = 2*IDGamma_1con(i);
    end
    % Dependencies for Gamma_2 and Gamma_5
    for j = 1:length(IDGamma_2con)
        % x-direction
        ICcon((i+j-1)*6+1:(i+j)*6-3) = (2*(i+j)-1)*[1,1,1];
        JCcon((i+j-1)*6+1:(i+j)*6-3) = [2*IDGamma_5con(j)-1,2*id4-1,2*id2-1];
        SCcon((i+j-1)*6+1:(i+j)*6-3) = [1,-1,1];
        % y-direction
        ICcon((i+j-1)*6+1+3:(i+j)*6) = 2*(i+j)*[1,1,1];
        JCcon((i+j-1)*6+1+3:(i+j)*6) = [2*IDGamma_5con(j),2*id4,2*id2];
        SCcon((i+j-1)*6+1+3:(i+j)*6) = [1,-1,1];
        % Assign dependent indices to ordering of C
        DepIndicesCON(2*(i+j)-1) = 2*IDGamma_2con(j)-1;
        DepIndicesCON(2*(i+j)) = 2*IDGamma_2con(j);
    end
    % Dependencies for Gamma_3 and Gamma_6
    for k = 1:length(IDGamma_3con)
        % x-direction
        ICcon((i+j+k-1)*6+1:(i+j+k)*6-3) = (2*(i+j+k)-1)*[1,1,1];
        JCcon((i+j+k-1)*6+1:(i+j+k)*6-3) = [2*IDGamma_6con(k)-1,2*id2-1,2*id1-1];
        SCcon((i+j+k-1)*6+1:(i+j+k)*6-3) = [1,1,-1];
        % y-direction
        ICcon((i+j+k-1)*6+1+3:(i+j+k)*6) = 2*(i+j+k)*[1,1,1];
        JCcon((i+j+k-1)*6+1+3:(i+j+k)*6) = [2*IDGamma_6con(k),2*id2,2*id1];
        SCcon((i+j+k-1)*6+1+3:(i+j+k)*6) = [1,1,-1];
        % Assign dependent indices to ordering of C
        DepIndicesCON(2*(i+j+k)-1) = 2*IDGamma_3con(k)-1;
        DepIndicesCON(2*(i+j+k)) = 2*IDGamma_3con(k);
    end
    % Assemble matrix
    Ccon = sparse(ICcon,JCcon,SCcon,2*(length(IDGamma_1con)+length(IDGamma_2con)+length(IDGamma_3con)),ndof);
end

% Code numbers for the 'dbc' option
DBCIndicesDBC = [2*IDGamma-1;2*IDGamma];
FreeIndicesDBC = setdiff(1:ndof,DBCIndicesDBC);

% Code numbers for the 'pbc' option
FreeIndicesPBC = (1:ndof)';

% Code numbers for FE2
DBCIndicesCON = [2*IDFCON-1;2*IDFCON]; % code numbers for constrained nodes
IndIndicesCON = setdiff(1:ndof,DepIndicesCON)'; % all independent code numbers
FreeIndicesCON = setdiff(IndIndicesCON,DBCIndicesCON); % all free code numbers
DBC2IndCON = zeros(size(DBCIndicesCON));
for i = 1:length(DBCIndicesCON)
    DBC2IndCON(i) = find(DBCIndicesCON(i)==IndIndicesCON);
end
FIIndicesCON = (1:length(IndIndicesCON))';
FIIndicesCON(DBC2IndCON) = [];

%% Get linearized buckling modes
if strcmp(modetype,'linbuckling')
    % Perform lineraized buckling analysis
    fprintf('Linearized buckling analysis... ');t_lin = tic;
    % Prescribe deformation
    G = [-0.1 0
        0 -0.1];
    xy = G*pm(:,IDFCON);
    DBCValuesCON = [xy(1,:)';xy(2,:)'];
    Larc = 1e-2;
    % Take the first loading step and compute stiffness matrices
    [u,Niter,Lambda,minDiag] = solve_r_arc(pm,tm,material,mngauss,zeros(ndof,1),...
        DBCIndicesCON,DBC2IndCON,DBCValuesCON,IndIndicesCON,DepIndicesCON,FreeIndicesCON,...
        FIIndicesCON,Ccon,TOL_r,Larc,0,MaxNiter,'twostep',+1,maxNumThreads);
    [~,~,~,K1,~] = grad_hess_c(pm,tm,material,mngauss,0*FreeIndicesCON,...
        DBCIndicesCON,DBC2IndCON,0*DBCValuesCON,IndIndicesCON,...
        DepIndicesCON,FreeIndicesCON,FIIndicesCON,Ccon,maxNumThreads);
    [~,~,~,K2,~] = grad_hess_c(pm,tm,material,mngauss,u(FreeIndicesCON),...
        DBCIndicesCON,DBC2IndCON,Lambda*DBCValuesCON,IndIndicesCON,...
        DepIndicesCON,FreeIndicesCON,FIIndicesCON,Ccon,maxNumThreads);
    [V,D] = eigs(K2,K1,10,'sm'); % linearized buckling analysis
    % Extract the first nmodes modes
    [~,id] = sort(diag(D));
    phi = zeros(ndof,nmodes);
    phi(FreeIndicesCON,:) = V(:,id(1:nmodes)); % scale the eigenvector for plotting purposes
    phi(DepIndicesCON,:) = Ccon(:,IndIndicesCON)*phi(IndIndicesCON,:);
    fprintf('in %g s\n',toc(t_lin));
end

%% Get true buckling modes
if strcmp(modetype,'truebuckling')
    % Get the three modes as the true buckling deformations
    fprintf('Solving RVE modes...\n');t_mode = tic;
    % Solve for given deformation
    G = 0.05*[-1 0
        0 -1];
    xy = G*pm(:,IDFCON);
    DBCValuesCON = [xy(1,:)';xy(2,:)'];
    nTimeSteps = 50; % number of time steps
    Time = zeros(1,3);
    timeIter = zeros(size(Time));
    U = zeros(2*size(pm,2),length(Time)); % u for all time steps
    time = 0;
    alldtimes = 1/nTimeSteps*ones(size(Time));
    dtime = 2*alldtimes(1);
    i = 2;
    skip = 0;
    while time<1 % take the RVE to the buckling point and then compute eigenmodes
        t_start_2 = tic;
        success = 0;
        Ntrials = 0;
        while ~success
            Ntrials = Ntrials+1;
            % Half the time increment
            dtime = dtime/2;
            if dtime<1/2^10
                fprintf('\n\n\n');
                warning('Step halving failed, dtime < TOL. Continue.');
                fprintf('\n\n\n');
                skip = 1;
                break;
            end
            % Get boundary conditions
            tDBCValues = time*DBCValuesCON;
            dtDBCValues = dtime*DBCValuesCON;
            % SOLVE FOR u
            [u,Niter,minDiag,status1] = solve_r(pm,tm,material,mngauss,U(:,i-1),...
                DBCIndicesCON,DBC2IndCON,tDBCValues,dtDBCValues,IndIndicesCON,DepIndicesCON,...
                FreeIndicesCON,FIIndicesCON,Ccon,TOL_r,MaxNiter,maxNumThreads);
            % Store potentially correct converged minimizing values
            U(:,i) = u;
            if status1==1
                success = 1;
            end
            % Test bifurcation
            if minDiag<0 && success==1
                % Get current Hessian
                t_bif = tic;
                [~,~,~,K1,~] = grad_hess_c(pm,tm,material,mngauss,U(FreeIndicesCON,i),...
                    DBCIndicesCON,DBC2IndCON,tDBCValues+dtDBCValues,IndIndicesCON,...
                    DepIndicesCON,FreeIndicesCON,FIIndicesCON,Ccon,maxNumThreads);
                [V,D] = eigs(K1,10,'sm'); % linearized buckling analysis
                % Extract the first nmodes modes
                diagD = diag(D)';
                [~,id] = sort(diagD);
                phi = zeros(ndof,nmodes);
                phi(FreeIndicesCON,:) = V(:,id(1:nmodes)); % scale the eigenvector for plotting purposes
                phi(DepIndicesCON,:) = Ccon(:,IndIndicesCON)*phi(IndIndicesCON,:);
                fprintf(['eigs = [',num2str(diagD(id(1:nmodes))),']\n']);
                fprintf('Time consumed %g s\n',toc(t_bif));
                % Stop the simulation
                skip = 1;
                dtime = 0;
                break;
            end
            if success==0
                fprintf('Halve the time step.\n');
            end
        end
        if skip==1
            break;
        end
        % Rewrite the minimizing value (could change due to bifurcation)
        U(:,i) = u;
        % PRINT ITERATION MESSAGE
        fprintf('%d step, %d Nwtn it., %g s\n',i,Niter,toc(t_start_2));
        timeIter(i) = Niter;
        if strcmp(callmode,'v')
            % Plot system's current configuration
            handle = figure(3);clf,hold all,axis equal,box on;
            set(handle,'name','Deformed configuration','numbertitle','off');
            r = pm(:)+u;
            if melemtype==2 || melemtype==9 || melemtype==21
                pdeplot([r(1:2:end)';r(2:2:end)'],[],tm);
            end
            plot(r(1:2:end),r(2:2:end),'.k');
            drawnow;
        end
        % Save time and proceed to the next time step
        time = time+dtime;
        Time(i) = time;
        alldtimes(i) = dtime;
        if Ntrials==1
            dtime = 2*1.5*alldtimes(i-1); % increase the time step if everything went well
            dtime = min(dtime,2*alldtimes(1));
        else
            dtime = 2*alldtimes(i-1); % else take the previous time increment
        end
        if time+dtime>1
            dtime = 2*(1-time);
        end
        i = i+1;
    end
    fprintf('\nTIME MODE %g s\n',toc(t_mode));
end

%% Get deformed configurations
if strcmp(modetype,'deformed')
    % Get the three modes as the true buckling deformations
    fprintf('Solving RVE modes...\n');
    numericPhi = zeros(length(pm(:)),nmodes);
    for imode = 1:nmodes
        fprintf('\n\nMODE %g\n',imode);t_mode = tic;
        % Get proper loading to trigger given mode/pattern
        switch rvetype
            case 'rect_square' % rectangular rve, square packing, circles
                loadMult = 5/100;
                G = loadMult*[-1 0
                    0 -1];
                IDF = [id1;id2;id4];
                xy = G*pm;
                DBCIndices = [2*IDF-1;2*IDF];
                DBCValues = [xy(1,IDF)';xy(2,IDF)'];
            case 'rect_hexa' % rectangular rve, hexagonal packing, circles/hexagons
                loadMult = 5/100;
                switch imode
                    case 1
                        if strcmp(modeshape,'mode')
                            IDF = [id1;id2;id4;id5;id6;id7]; % mode (phi^90)
                            G = loadMult*[-1 0;
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        elseif strcmp(modeshape,'pattern')
                            IDF = [id1;id2;id4]; % pattern I
                            G = loadMult*[0 0;
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        end
                    case 2
                        if strcmp(modeshape,'mode')
                            IDF = [id1;id2;id4;id5;id8;id9]; % mode (phi^-30)
                            G = loadMult*[-1 0;
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        elseif strcmp(modeshape,'pattern')
                            IDF = [id1;id2;id4]; % pattern II
                            G = loadMult*[-1 0;
                                0 -0.5];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        end
                    case 3
                        if strcmp(modeshape,'mode')
                            IDF = [id1;id2;id4;id5;id10;id11]; % mode (phi^30)
                            G = loadMult*[-1 0;
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        elseif strcmp(modeshape,'pattern')
                            IDF = [id1;id2;id4]; % pattern III
                            G = loadMult*[-1 0;
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        end
                end
            case 'skew_hexa' % skewed rve, hexagonal packing, circles/hexagons
                loadMult = 5/100;
                switch imode
                    case 1
                        if strcmp(modeshape,'mode')
                            IDF = [id1;id2;id4;id10]; % mode (phi^90)
                            G = loadMult*[-1 0
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        elseif strcmp(modeshape,'pattern')
                            IDF = [id1;id2;id4]; % pattern I
                            G = loadMult*[0 0;
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        end
                    case 2
                        if strcmp(modeshape,'mode')
                            IDF = [id1;id2;id4;id7]; % mode (phi^-30)
                            G = loadMult*[-1 0
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        elseif strcmp(modeshape,'pattern')
                            IDF = [id1;id2;id4]; % pattern II
                            G = loadMult*[-1 0;
                                0 -0.5];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        end
                    case 3
                        if strcmp(modeshape,'mode')
                            IDF = [id1;id2;id4;id5]; % mode (phi^30)
                            G = loadMult*[-1 0
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        elseif strcmp(modeshape,'pattern')
                            IDF = [id1;id2;id4]; % pattern III
                            G = loadMult*[-1 0;
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        end
                end
            case 'hexa_hexa' % hexagonal rve, hexagonal packing, circles/hexagons
                loadMult = 2/100;
                switch imode
                    case 1
                        if strcmp(modeshape,'mode')
                            IDF = [id1;id2;id4;id11]; % mode (phi^90)
                            G = loadMult*[-1 0
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        elseif strcmp(modeshape,'pattern')
                            IDF = [id1;id2;id4]; % pattern I
                            G = loadMult*[0 0;
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        end
                    case 2
                        if strcmp(modeshape,'mode')
                            IDF = [id1;id2;id4;id15]; % mode (phi^-30)
                            G = loadMult*[-1 0
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        elseif strcmp(modeshape,'pattern')
                            IDF = [id1;id2;id4]; % pattern II
                            G = loadMult*[-1 0;
                                0 -0.5];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        end
                    case 3
                        if strcmp(modeshape,'mode')
                            IDF = [id1;id2;id4;id13]; % mode (phi^30)
                            G = loadMult*[-1 0
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        elseif strcmp(modeshape,'pattern')
                            IDF = [id1;id2;id4]; % pattern III
                            G = loadMult*[-1 0;
                                0 -1];
                            xy = G*pm;
                            DBCIndices = [2*IDF-1;2*IDF];
                            DBCValues = [xy(1,IDF)';xy(2,IDF)'];
                        end
                end
        end
        % Code numbers
        DepIndices = DepIndicesCON;
        IndIndices = setdiff(1:ndof,DepIndicesCON)'; % all independent code numbers
        FreeIndices = setdiff(IndIndices,DBCIndices); % all free code numbers
        DBC2Ind = zeros(size(DBCIndices));
        for i = 1:length(DBCIndices)
            DBC2Ind(i) = find(DBCIndices(i)==IndIndices);
        end
        FIIndices = (1:length(IndIndices))';
        FIIndices(DBC2Ind) = [];
        % Solve for given pattern/mode
        nTimeSteps = 30; % number of time steps
        Time = zeros(1,3);
        timeIter = zeros(size(Time));
        U = zeros(2*size(pm,2),length(Time)); % u for all time steps
        time = 0;
        alldtimes = 1/nTimeSteps*ones(size(Time));
        dtime = 2*alldtimes(1);
        i = 2;
        skip = 0;
        while time<1
            t_start_2 = tic;
            success = 0;
            Ntrials = 0;
            while ~success
                Ntrials = Ntrials+1;
                % Half the time increment
                dtime = dtime/2;
                if dtime<1/2^10
                    fprintf('\n\n\n');
                    warning('Step halving failed, dtime < TOL. Continue.');
                    fprintf('\n\n\n');
                    skip = 1;
                    break;
                end
                % Get boundary conditions
                tDBCValues = time*DBCValues;
                dtDBCValues = dtime*DBCValues;
                % SOLVE FOR u
                [u,Niter,minDiag,status1] = solve_r(pm,tm,material,mngauss,U(:,i-1),...
                    DBCIndices,DBC2Ind,tDBCValues,dtDBCValues,IndIndices,DepIndices,...
                    FreeIndices,FIIndices,Ccon,TOL_r,MaxNiter,maxNumThreads);
                % Store potentially correct converged minimizing values
                U(:,i) = u;
                if status1==1
                    success = 1;
                end
                % Test bifurcation
                if minDiag<0 && success==1
                    % Get current Hessian
                    t_bif = tic;
                    [~,~,~,K1,~] = grad_hess_c(pm,tm,material,mngauss,U(FreeIndices,i),...
                        DBCIndices,DBC2Ind,tDBCValues+dtDBCValues,IndIndices,...
                        DepIndices,FreeIndices,FIIndices,Ccon,maxNumThreads);
                    [V,D] = eigs(K1,10,'sm'); % linearized buckling analysis
                    [~,id] = min(diag(D));
                    phi = zeros(size(U(:,i)));
                    phi(FreeIndices) = V(:,id); % scale the eigenvector for plotting purposes
                    phi(DepIndices) = Ccon(:,IndIndices)*phi(IndIndices);
                    rm = pm(:)+DiamInner/3*phi/norm(phi,'inf');
                    if strcmp(callmode,'v')
                        handle = figure(100);clf,hold all,axis equal;
                        plot(rm(1:2:end),rm(2:2:end),'.k');
                        set(handle,'name',['Bifurcatoin mode ',num2str(1),', eigval1 = ',...
                            num2str(D(id,id))],'numbertitle','off');
                    end
                    phi = 1e-3*CellSize/norm(phi,'inf')*phi; % scale the eigenvector
                    % If the eigenvalue is negative
                    if D(id,id)<TOL_g
                        fprintf('Unstable configuration encountered, eigval1 = %g. Updating...\n',D(id,id));
                        % Solve for u again, try multiple times
                        jbif = 1;
                        while minDiag<0 && jbif<11
                            tphi = (jbif*(jbif-1)+1)*phi;
                            [u,Niter,minDiag,status2] = solve_r(pm,tm,material,mngauss,U(:,i)+tphi,...
                                DBCIndices,DBC2Ind,tDBCValues+dtDBCValues,0*DBCValues,...
                                IndIndices,DepIndices,FreeIndices,FIIndices,Ccon,TOL_r,MaxNiter,...
                                maxNumThreads);
                            jbif = jbif+1;
                        end
                        if jbif<11 && status2==1
                            success = 1;
                        else
                            success = 0;
                        end
                    end
                    fprintf('Time consumed %g s\n',toc(t_bif));
                end
                if success==0
                    fprintf('Halve the time step.\n');
                end
            end
            if skip==1
                break;
            end
            % Rewrite the minimizing value (could change due to bifurcation)
            U(:,i) = u;
            % PRINT ITERATION MESSAGE
            fprintf('%d step, %d Nwtn it., %g s\n',i,Niter,toc(t_start_2));
            timeIter(i) = Niter;
            if strcmp(callmode,'v')
                % Plot system's current configuration
                handle = figure(3);clf,hold all,axis equal,box on;
                set(handle,'name','Deformed configuration','numbertitle','off');
                r = pm(:)+u;
                if melemtype==2 || melemtype==9 || melemtype==21
                    pdeplot([r(1:2:end)';r(2:2:end)'],[],tm);
                end
                plot(r(1:2:end),r(2:2:end),'.k');
                drawnow;
            end
            % Save time and proceed to the next time step
            time = time+dtime;
            Time(i) = time;
            alldtimes(i) = dtime;
            if Ntrials==1
                dtime = 2*1.5*alldtimes(i-1); % increase the time step if everything went well
                dtime = min(dtime,2*alldtimes(1));
            else
                dtime = 2*alldtimes(i-1); % else take the previous time increment
            end
            if time+dtime>1
                dtime = 2*(1-time);
            end
            i = i+1;
        end
        txy = Time(end)*G*pm;
        numericPhi(:,imode) = U(:,end)-txy(:);
        fprintf('\nTIME MODE %g s\n',toc(t_mode));
    end
    phi = numericPhi;
end

%% Get mass matrix M
% Get proper integration rule for mass matrix
switch melemtype
    case 2 % linear triangle
        mngaussmass = 3;
    case 3 % four node quadrangle
        mngaussmass = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 9 % quadratic triangle
        mngaussmass = 6;
    case 16 % eight node quadrangle
        mngaussmass = 3 * 3; % product of two one-dimensional Gauss integration rules
    case 10 % nine node quadrangle
        mngaussmass = 3 * 3; % product of two one-dimensional Gauss integration rules
    case 21 % cubic triangle
        mngaussmass = 12;
    case 36 % cubic quadrangle
        mngaussmass = 4 * 4;
    otherwise
        error('Wrong element type chosen.');
end
M = build_mass_matrix_2d(pm,tm,material,mngaussmass,0*pm(:),maxNumThreads);

%% Normalize modes
% Store modes for system perturbation
gradvi = [1,1];
for imode = 1:nmodes
    % numericNormPhi = mean2((phi(1:2:end,imode).^2+phi(2:2:end,imode).^2).^0.5);
    meanPhi = build_kinematic_averages(pm,tm,mngauss,gradvi,phi(:,imode),maxNumThreads);
    phi(1:2:end,imode) = phi(1:2:end,imode)-meanPhi(1);
    phi(2:2:end,imode) = phi(2:2:end,imode)-meanPhi(2);
    normPhi = norm(phi(:,imode));
    phi(:,imode) = phi(:,imode)/normPhi; % normalize all modes by their standard deviations
    % if strcmp(callmode,'v')
    handle = figure(1000+imode);clf,hold all,axis equal,box on,grid on;
    h = pdeplot(pm,[],tm);
    h.Color = 0.75*[1,1,1];
    pdef = reshape(pm(:)-DiamInner/5*phi(:,imode)/norm(phi(:,imode),'inf'),size(pm));
    pdeplot(pdef,[],tm);
    xlabel('$X_1$');ylabel('$X_2$');
    set(handle,'name',[modeshape,' ',num2str(imode)],'numbertitle','off');
    % end
    drawnow;
    % Test volume averaging of individual modes
    % [m1,m2,m3,m4] = build_kinematic_averages(pm,tm,mngauss,gradvi,phi,maxNumThreads);
end

%% Store data to RVE database
% Make the database DATA globally available
global DATA;
% General data
DATA.rvetype = rvetype;
DATA.inclusiontype = inclusiontype;
DATA.DiamOuter = DiamOuter;
DATA.CellSize = CellSize;
DATA.TOL_g = TOL_g;
DATA.TOL_r = TOL_r;
DATA.MaxNiter = MaxNiter;
DATA.material = material;
DATA.elemtype = melemtype;
DATA.hmax = mhmax;
DATA.ngauss = mngauss;
DATA.p = pm;
DATA.t = tm;
DATA.Q = Q;
DATA.nmodes = nmodes;
DATA.phi = phi;
% Indices for static condensation and fe2 (uses three MVE corner points)
DATA.DBCIndicesCON = DBCIndicesCON;
DATA.FreeIndicesCON = FreeIndicesCON;
DATA.IndIndicesCON = IndIndicesCON;
DATA.DepIndicesCON = DepIndicesCON;
DATA.FIIndicesCON = FIIndicesCON;
DATA.DBC2IndCON = DBC2IndCON;
DATA.IDFCON = IDFCON;
DATA.CCON = Ccon;
% Indices for the 'dbc' micromorphic option (uses all MVE boundary points)
DATA.DBCIndicesDBC = DBCIndicesDBC;
DATA.FreeIndicesDBC = FreeIndicesDBC;
% Indices for the 'pbc' micromorphic option (uses four or more MVE corner points)
DATA.FreeIndicesPBC = FreeIndicesPBC;
DATA.CPBC = Cpbc;
% Weight (mass) matrix for orthogonality constraints
DATA.M = M;

end
