function [p,t] = init_gmsh_circle(P0,Radius,SizeX,SizeY,hmax,elemtype,...
    ngauss,material,TOL_g,callmode,maxNumThreads)

% Initialize mesh
fprintf('Init mesh...\n');
if ~isfolder('gmsh')
    mkdir('gmsh');
end
tic;
% Create gmsh input file and call gmsh
fin = fopen('gmsh/circlemesh.geo','w');
fprintf(fin,'SetFactory("OpenCASCADE");\n');
fprintf(fin,'Rectangle(1) = {%.15g, %.15g, 0, %.15g, %.15g};\n',[-SizeX,...
    -SizeY,2*SizeX,2*SizeY]); % bounding box
fprintf(fin,'Disk(2) = {%.15g, %.15g, 0, %.15g};\n',P0(1),P0(2),Radius);
fprintf(fin,'f1() = BooleanIntersection{ Surface{1}; Delete; }{ Surface{2}; Delete; };\n');

% Set maximum element size
fprintf(fin,'Mesh.CharacteristicLengthMax = %.15g;\n',hmax);

% Coherence (removes all duplicit elementary geometrical entities)
% fprintf(fin,'Coherence;\n');

% Mesh optimization
fprintf(fin,'Mesh.HighOrderOptimize = 1;\n');

% Mesh file format
fprintf(fin,'Mesh.MshFileVersion = 2.2;\n');

% Choose algorithm
if elemtype==3 || elemtype==10 || elemtype==16 || elemtype==36 % quads
    fprintf(fin,'Recombine Surface "*";\n');
    if elemtype==16
        fprintf(fin,'Mesh.SecondOrderIncomplete=1;\n');
    end
    % Choose algorithm for quadrangle mesh
    fprintf(fin,'Mesh.Algorithm = 7;\n'); % 2 - automatic, 7 - BAMG, 8 - DelQuad
else
    % Choose algorithm for triangular mesh
    fprintf(fin,'Mesh.Algorithm = 6;\n'); % 1 - MeshAdapt, 2 - automatic, 5 - Delaunay, 6 - Frontal, 7 - BAMG
end

% Set interpolation order, 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9
switch elemtype
    case 2
        elnodes = 3;
        elorder = 1;
    case 3
        elnodes = 4;
        elorder = 1;
    case 9
        elnodes = 6;
        elorder = 2;
    case 10
        elnodes = 9;
        elorder = 2;
    case 16
        elnodes = 8;
        elorder = 2;
    case 21
        elnodes = 8;
        elorder = 3;
    case 36
        elnodes = 16;
        elorder = 3;
    otherwise
        error('Wrong element type chosen.');
end
fclose(fin);

% Call gmsh
system([' gmsh gmsh/circlemesh.geo -o gmsh/circlemesh.msh ',...
    ' -order ',num2str(elorder),' -2 ',...
    ' -clmax ' num2str(hmax)]);

% Open mesh.msh and parse the output data
fout = fopen('gmsh/circlemesh.msh','r');
while ~feof(fout)
    tline = fgetl(fout);
    if strcmp(tline,'$Nodes')
        npoints = str2num(fgets(fout));
        points = fscanf(fout,'%g',[4,npoints])';
    end
    if strcmp(tline,'$Elements')
        nelem = str2num(fgets(fout));
        elements = zeros(nelem,5+elnodes);
        for i = 1:nelem
            tline = fgetl(fout);
            numbers = str2num(tline);
            elements(i,1:length(numbers)) = numbers;
        end
    end
end
fclose('all');

% Delete files to prevent their reloading when unsuccesful call to gmsh made
delete('gmsh/circlemesh.geo');
delete('gmsh/circlemesh.msh');

% Create p and t matrices and proceed as usual.
p = points(:,2:3)';
t = elements(elements(:,2)==elemtype,6:end)';
elemtypes = setdiff(elements(:,2),[1,8,15,26,elemtype]); % factor out 0D, 1D, and elemtype 2D element types
if(~isempty(elemtypes)) % if different type of elements found, throw a warning
    warning(sprintf('Mixed mesh generated.\nIncludes also different kinds of elements than required.'));
end

% Assign materials to elements
elnodes = size(t,1);
t = [t;ones(1,size(t,2))];
[Wq,Uq,Eq,Sq,Xq,Yq] = sample_UES_2d(p,t,material,ngauss,zeros(size(p(:))),[],[],...
    TOL_g,1,maxNumThreads);
Cg = [Xq;Yq];

% Plot the resulting mesh
if strcmp(callmode,'v')
    handle = figure(2);clf,hold all,axis equal,box on;
    plot_mesh(handle,p,t,elemtype,[0,0,1]);
    plot(p(1,:),p(2,:),'.k');
    plot(Cg(1,t(elnodes+1,:)==1),Cg(2,t(elnodes+1,:)==1),'.r');
    plot(Cg(1,t(elnodes+1,:)==2),Cg(2,t(elnodes+1,:)==2),'.g');
    set(handle,'name','Mesh and materials','numbertitle','off');
end
fprintf('Time consumed %g s\n\n',toc);

end
