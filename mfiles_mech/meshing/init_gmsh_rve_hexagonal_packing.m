function [p,t] = init_gmsh_rve_hexagonal_packing(DiamOuter,...
    DiamInner,InclusionType,hmax,elemtype,callmode)

% Initialize mesh
fprintf('Init mesh...\n');t_mesh = tic;
if ~isfolder('gmsh')
    mkdir('gmsh');
end

% Create gmsh input file
fin = fopen('gmsh/mvemesh.geo','w');
fprintf(fin,'SetFactory("OpenCASCADE");\n');

% Create circles
Circles(1).p = [-1.5,-sqrt(3)/2,0]*DiamOuter*sqrt(3)/4;
Circles(2).p = [0.5,-sqrt(3)/2,0]*DiamOuter*sqrt(3)/4;
Circles(3).p = [-0.5,sqrt(3)/2,0]*DiamOuter*sqrt(3)/4;
Circles(4).p = [1.5,sqrt(3)/2,0]*DiamOuter*sqrt(3)/4;
point = 0;
line = 0;
loop = 0;
surface = 0;
for i = 1:length(Circles) % outer shape consisting of four hexagons
    
    % Define points
    theta0 = -pi/2;
    r = DiamOuter/2;
    P0 = Circles(i).p;
    P1 = P0+r*[cos(theta0+0*pi/3),sin(theta0+0*pi/3),0];
    P2 = P0+r*[cos(theta0+1*pi/3),sin(theta0+1*pi/3),0];
    P3 = P0+r*[cos(theta0+2*pi/3),sin(theta0+2*pi/3),0];
    P4 = P0+r*[cos(theta0+3*pi/3),sin(theta0+3*pi/3),0];
    P5 = P0+r*[cos(theta0+4*pi/3),sin(theta0+4*pi/3),0];
    P6 = P0+r*[cos(theta0+5*pi/3),sin(theta0+5*pi/3),0];
    fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+1,P1);
    fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+2,P2);
    fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+3,P3);
    fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+4,P4);
    fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+5,P5);
    fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+6,P6);
    
    % Define lines
    fprintf(fin,'Line(%i) = {%i, %i};\n',line+1,point+1,point+2);
    fprintf(fin,'Line(%i) = {%i, %i};\n',line+2,point+2,point+3);
    fprintf(fin,'Line(%i) = {%i, %i};\n',line+3,point+3,point+4);
    fprintf(fin,'Line(%i) = {%i, %i};\n',line+4,point+4,point+5);
    fprintf(fin,'Line(%i) = {%i, %i};\n',line+5,point+5,point+6);
    fprintf(fin,'Line(%i) = {%i, %i};\n',line+6,point+6,point+1);
    
    % Define line loops
    fprintf(fin,'Line Loop(%i) = {%i, %i, %i, %i, %i, %i};\n',loop+1,line+1,line+2,line+3,line+4,line+5,line+6);
    
    % Define surfaces
    fprintf(fin,'Plane Surface(%i) = {%i};\n',surface+1,loop+1);
    
    % Increment points, lines, line loops, and surfaces
    point = point+6;
    line = line+6;
    loop = loop+1;
    surface = surface+1;
end
for i = 1:length(Circles) % holes
    
    theta0 = -pi/2;
    r = DiamInner/2;
    P0 = Circles(i).p;
    if strcmp(InclusionType,'hexagon')
        
        % Define points
        P1 = P0+r*[cos(theta0+0*pi/3),sin(theta0+0*pi/3),0];
        P2 = P0+r*[cos(theta0+1*pi/3),sin(theta0+1*pi/3),0];
        P3 = P0+r*[cos(theta0+2*pi/3),sin(theta0+2*pi/3),0];
        P4 = P0+r*[cos(theta0+3*pi/3),sin(theta0+3*pi/3),0];
        P5 = P0+r*[cos(theta0+4*pi/3),sin(theta0+4*pi/3),0];
        P6 = P0+r*[cos(theta0+5*pi/3),sin(theta0+5*pi/3),0];
        fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+1,P1);
        fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+2,P2);
        fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+3,P3);
        fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+4,P4);
        fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+5,P5);
        fprintf(fin,'Point(%i) = {%.15g, %.15g, %.15g};\n',point+6,P6);
        
        % Define lines
        fprintf(fin,'Line(%i) = {%i, %i};\n',line+1,point+1,point+2);
        fprintf(fin,'Line(%i) = {%i, %i};\n',line+2,point+2,point+3);
        fprintf(fin,'Line(%i) = {%i, %i};\n',line+3,point+3,point+4);
        fprintf(fin,'Line(%i) = {%i, %i};\n',line+4,point+4,point+5);
        fprintf(fin,'Line(%i) = {%i, %i};\n',line+5,point+5,point+6);
        fprintf(fin,'Line(%i) = {%i, %i};\n',line+6,point+6,point+1);
        
        % Define line loops
        fprintf(fin,'Line Loop(%i) = {%i, %i, %i, %i, %i, %i};\n',loop+1,line+1,line+2,line+3,line+4,line+5,line+6);
        
        % Define surfaces
        fprintf(fin,'Plane Surface(%i) = {%i};\n',surface+1,loop+1);
    elseif strcmp(InclusionType,'circle')
        
        % Define surfaces
        fprintf(fin,'Disk(%d) = {%.15g, %.15g, %.15g, %.15g};\n',surface+1,P0,r);
    else
        error('Wrong inclusion type chosen.');
    end
    
    % Increment points, lines, line loops, and surfaces
    point = point+6;
    line = line+6;
    loop = loop+1;
    surface = surface+1;
end

fprintf(fin,'BooleanDifference { Surface{1:4}; Delete; }{ Surface{5:8}; Delete; }\n');

% Set maximum element size
fprintf(fin,'Mesh.CharacteristicLengthMax = %.15g;\n',hmax);

% Coherence (removes all duplicit elementary geometrical entities)
% fprintf(fin,'Coherence;\n');

% Mesh optimization
fprintf(fin,'Mesh.HighOrderOptimize = 1;\n');

% Mesh file format
fprintf(fin,'Mesh.MshFileVersion = 2.2;\n');

% Choose elements
if elemtype==3 || elemtype==10 || elemtype==16 || elemtype==36 % quads
    fprintf(fin,'Recombine Surface "*";\n');
    if elemtype==16
        fprintf(fin,'Mesh.SecondOrderIncomplete=1;\n');
    end
    % Choose algorithm for quadrangle mesh
    fprintf(fin,'Mesh.Algorithm = 2;\n'); % 2 - automatic, 7 - BAMG, 8 - DelQuad
else
    % Choose algorithm for triangular mesh
    fprintf(fin,'Mesh.Algorithm = 6;\n'); % 1 - MeshAdapt, 2 - automatic, 5 - Delaunay, 6 - Frontal, 7 - BAMG
end
switch elemtype % set interpolation order, 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9
    case 2
        elnodes = 3;
        elorder = 1;
    case 3
        elnodes = 4;
        elorder = 1;
    case 9
        elnodes = 6;
        elorder = 2;
    case 10
        elnodes = 9;
        elorder = 2;
    case 16
        elnodes = 8;
        elorder = 2;
    case 21
        elnodes = 8;
        elorder = 3;
    case 36
        elnodes = 16;
        elorder = 3;
    otherwise
        error('Wrong element type chosen.');
end
fprintf(fin,'Mesh.ElementOrder = %d;\n',elorder);
fclose(fin);

% Call gmsh
system('gmsh gmsh/mvemesh.geo -o gmsh/mvemesh.msh -2');

% Open mesh.msh and parse the output data
fout = fopen('gmsh/mvemesh.msh','r');
while ~feof(fout)
    tline = fgetl(fout);
    if strcmp(tline,'$Nodes')
        npoints = str2num(fgets(fout));
        points = fscanf(fout,'%g',[4,npoints])';
    end
    if strcmp(tline,'$Elements')
        nelem = str2num(fgets(fout));
        elements = zeros(nelem,5+elnodes);
        for i = 1:nelem
            tline = fgetl(fout);
            numbers = str2num(tline);
            elements(i,1:length(numbers)) = numbers;
        end
    end
end
fclose('all');

% Delete files to prevent their reloading when unsuccesful call to gmsh made
delete('gmsh/mvemesh.geo');
delete('gmsh/mvemesh.msh');

% Create p and t matrices and proceed as usual.
p = points(:,2:3)';
t = elements(elements(:,2)==elemtype,6:end)';
elemtypes = setdiff(elements(:,2),[1,8,15,26,elemtype]); % factor out 0D, 1D, and elemtype 2D element types
if(~isempty(elemtypes)) % if different type of elements found, throw a warning
    warning(sprintf('Mixed mesh generated.\nIncludes also different kinds of elements than required.'));
end
t = [t;ones(1,size(t,2))];

% Plot the resulting mesh
if strcmp(callmode,'v')
    handle = figure(2);clf,hold all,axis equal,box on;
    plot_mesh(handle,p,t,elemtype,[0,0,1]);
    plot(p(1,:),p(2,:),'.k');
    set(handle,'name','Mesh and materials','numbertitle','off');
end
fprintf('Time consumed %.g s\n\n',toc(t_mesh));

end
