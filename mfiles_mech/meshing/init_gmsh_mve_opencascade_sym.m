function [p,t] = init_gmsh_mve_opencascade_sym(CellSize,Diameter,hmax,material,...
    elemtype,ngauss,TOL_g,callmode,maxNumThreads)

% Initialize mesh
fprintf('Init mesh...\n');t_mesh = tic;
if ~isfolder('gmsh')
    mkdir('gmsh');
end
% Create gmsh input file
fin = fopen('gmsh/mvemesh.geo','w');
fprintf(fin,'SetFactory("OpenCASCADE");\n');

% Create part of the domain
fprintf(fin,'Point(1) = {%.15g, %.15g, %.15g, %.15g};\n',CellSize*[0.5,0.5,0],hmax);
fprintf(fin,'Point(2) = {%.15g, %.15g, %.15g, %.15g};\n',CellSize*[1,1,0],hmax);
fprintf(fin,'Point(3) = {%.15g, %.15g, %.15g, %.15g};\n',CellSize*[0.5,1,0],hmax);
% Define lines
fprintf(fin,'Line(1) = {1, 2};\n');
fprintf(fin,'Line(2) = {2, 3};\n');
fprintf(fin,'Line(3) = {3, 1};\n');
% Define line loops
fprintf(fin,'Line Loop(1) = {1, 2, 3};\n');
% Define surfaces
fprintf(fin,'Plane Surface(1) = {1};\n');
fprintf(fin,'Disk(2) = {%.15g, %.15g, 0, %.15g};\n',CellSize/2,CellSize/2,Diameter/2);

% Create the resulting domain by subtracting the hole
if material(2,1)==0
    fprintf(fin,'f1() = BooleanDifference{ Surface{1}; Delete; }{ Surface{2}; Delete; };\n');
else
    fprintf(fin,'f1() = BooleanFragments{ Surface{1}; Delete; }{ Surface{2}; Delete; };\n');
end

% Set maximum element size
fprintf(fin,'Mesh.CharacteristicLengthMax = %.15g;\n',hmax);
fprintf(fin,'Mesh.CharacteristicLengthMin = %.15g;\n',hmax);

% Coherence (removes all duplicit elementary geometrical entities)
% fprintf(fin,'Coherence;\n');

% Mesh optimization
fprintf(fin,'Mesh.HighOrderOptimize = 1;\n');

% Mesh file format
fprintf(fin,'Mesh.MshFileVersion = 2.2;\n');

% Choose elements
if elemtype==3 || elemtype==10 || elemtype==16 || elemtype==36 % quads
    fprintf(fin,'Recombine Surface "*";\n');
    if elemtype==16
        fprintf(fin,'Mesh.SecondOrderIncomplete=1;\n');
    end
    % Choose algorithm for quadrangle mesh
    fprintf(fin,'Mesh.Algorithm = 2;\n'); % 2 - automatic, 7 - BAMG, 8 - DelQuad
else
    % Choose algorithm for triangular mesh
    fprintf(fin,'Mesh.Algorithm = 6;\n'); % 1 - MeshAdapt, 2 - automatic, 5 - Delaunay, 6 - Frontal, 7 - BAMG
end
switch elemtype % set interpolation order, 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9
    case 2
        elnodes = 3;
        elorder = 1;
    case 3
        elnodes = 4;
        elorder = 1;
    case 9
        elnodes = 6;
        elorder = 2;
    case 10
        elnodes = 9;
        elorder = 2;
    case 16
        elnodes = 8;
        elorder = 2;
    case 21
        elnodes = 8;
        elorder = 3;
    case 36
        elnodes = 16;
        elorder = 3;
    otherwise
        error('Wrong element type chosen.');
end
fprintf(fin,'Mesh.ElementOrder = %d;\n',elorder);
fclose(fin);

% Call gmsh
system('gmsh gmsh/mvemesh.geo -o gmsh/mvemesh.msh -2');

% Open mesh.msh and parse the output data
fout = fopen('gmsh/mvemesh.msh','r');
while ~feof(fout)
    tline = fgetl(fout);
    if strcmp(tline,'$Nodes')
        npoints = str2num(fgets(fout));
        points = fscanf(fout,'%g',[4,npoints])';
    end
    if strcmp(tline,'$Elements')
        nelem = str2num(fgets(fout));
        elements = zeros(nelem,5+elnodes);
        for i = 1:nelem
            tline = fgetl(fout);
            numbers = str2num(tline);
            elements(i,1:length(numbers)) = numbers;
        end
    end
end
fclose('all');

% Create p and t matrices and proceed
p0a = points(:,2:3)';
t0a = elements(elements(:,2)==elemtype,6:end)';
elemtypes = setdiff(elements(:,2),[1,8,15,26,elemtype]); % factor out 0D, 1D, and elemtype 2D element types
if(~isempty(elemtypes)) % if different type of elements found, throw a warning
    warning(sprintf('Mixed mesh generated.\nIncludes also different kinds of elements than required.'));
end

% Use mex parser for gmsh 4 and newer
% [nodes, elements, elementTypes] = parse_GMSH_output([pwd,'/gmsh/mvemesh.msh']);
% p1 = nodes(1:2,:);
% t1 = elements;

% Delete files to prevent their reloading when unsuccesful call to gmsh made
delete('gmsh/mvemesh.geo');
delete('gmsh/mvemesh.msh');

% Perform symmetry operations on the mesh of one unit cell
p0b = [-p0a(1,:)+CellSize;p0a(2,:)]; % mirror the top right unit cell w.r.t the vertical axis
t0b = t0a+size(p0a,2);

% Take care of mirrored elements: assure cw orientation in node numbering (otherwise Jdet<0 in FEM)
% figure(156);clf;hold all;axis equal;
% for i = 1:size(t1,1)
%     P = p1(:,t1(i,1));
%     plot(P(1),P(2),'.k');
%     text(P(1),P(2),num2str(i)); % check element numbering convention
% end
switch elemtype % set interpolation order, 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9, 36 = Q16
    case 2
        t0b([2 3],:) = t0b([3 2],:);
    case 3
        t0b([2 3 4],:) = t0b([4 3 2],:);
    case 9
        t0b([2 3 4 6],:) = t0b([3 2 6 4],:);
    case 10
        t0b([2 3 4 5 6 7 8],:) = t0b([4 3 2 8 7 6 5],:);
    case 16
        t0b([2 3 4 5 6 7 8],:) = t0b([4 3 2 8 7 6 5],:);
    case 21
        t0b([2 3 4 5 6 7 8 9],:) = t0b([3 2 9 8 7 6 5 4],:);
    case 36
        t0b([2 3 4 5 6 7 8 9 10 11 12 13 14 15 16],:) = t0b([4 3 2 12 11 10 9 8 7 6 5 13 16 15 14],:);
    otherwise
        error('Wrong element type chosen.');
end

% Global p and t matrices
p0 = [p0a,p0b];
t0 = [t0a,t0b];

% Perform symmetry operations on the mesh (rotate three times around CellSize/2*[1,1])
p1 = p0;
t1 = t0;
Pc = CellSize/2*[1,1,0];
for i = 1:3
    
    % Create rotated piece of the mesh
    theta = i*pi/2;
    Rot = [cos(theta) -sin(theta)
        sin(theta) cos(theta)];
    pt = Rot*([p0(1,:)-Pc(1);p0(2,:)-Pc(2)]);
    pt = [pt(1,:)+Pc(1);pt(2,:)+Pc(2)];
    tt = t0+i*size(p0,2);
    
    % Allocate to p and t
    p1 = [p1,pt];
    t1 = [t1,tt];
end

% Perform mirror operations on one unit cell
p2 = [-p1(1,:);p1(2,:)]; % mirror the top right unit cell w.r.t the vertical axis
t2 = t1+size(p1,2);
p3 = [-p1(1,:);-p1(2,:)]; % mirror the top right unit cell w.r.t the horizontal and vertical axes
t3 = t1+2*size(p1,2);
p4 = [p1(1,:);-p1(2,:)]; % mirror the top right unit cell w.r.t the horizontal axis
t4 = t1+3*size(p1,2);

% Take care of mirrored elements
switch elemtype % set interpolation order, 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9, 36 = Q16
    case 2
        t2([2 3],:) = t2([3 2],:);
        t4([2 3],:) = t4([3 2],:);
    case 3
        t2([2 3 4],:) = t2([4 3 2],:);
        t4([2 3 4],:) = t4([4 3 2],:);
    case 9
        t2([2 3 4 6],:) = t2([3 2 6 4],:);
        t4([2 3 4 6],:) = t4([3 2 6 4],:);
    case 10
        t2([2 3 4 5 6 7 8],:) = t2([4 3 2 8 7 6 5],:);
        t4([2 3 4 5 6 7 8],:) = t4([4 3 2 8 7 6 5],:);
    case 16
        t2([2 3 4 5 6 7 8],:) = t2([4 3 2 8 7 6 5],:);
        t4([2 3 4 5 6 7 8],:) = t4([4 3 2 8 7 6 5],:);
    case 21
        t2([2 3 4 5 6 7 8 9],:) = t2([3 2 9 8 7 6 5 4],:);
        t4([2 3 4 5 6 7 8 9],:) = t4([3 2 9 8 7 6 5 4],:);
    case 36
        t2([2 3 4 5 6 7 8 9 10 11 12 13 14 15 16],:) = t2([4 3 2 12 11 10 9 8 7 6 5 13 16 15 14],:);
        t4([2 3 4 5 6 7 8 9 10 11 12 13 14 15 16],:) = t4([4 3 2 12 11 10 9 8 7 6 5 13 16 15 14],:);
    otherwise
        error('Wrong element type chosen.');
end

% Global p and t matrices
p = [p1,p2,p3,p4];
t = [t1,t2,t3,t4];

% Remove duplicities in p and t
% Check uniqueness of points
idPoints = unique(t(:));
tempp = NaN(2,length(idPoints));
k = 1;
for j = 1:length(idPoints)
    P1 = p(:,idPoints(j));
    if isempty(find(abs(tempp(1,:)-P1(1))+abs(tempp(2,:)-P1(2))<TOL_g,1))
        tempp(:,k) = P1;
        k = k+1;
    end
end
tempp = tempp(:,1:k-1);

% Renumber t
temptr = NaN(size(t));
for j = 1:size(t,2)
    % Find points of triangles in t within p and tempp
    for i = 1:size(t,1)
        Pi = p(:,t(i,j));
        indi = find(abs(tempp(1,:)-Pi(1))+abs(tempp(2,:)-Pi(2))<TOL_g,1);
        if ~isempty(indi)
            temptr(i,j) = indi(1);
        else
            fprintf('tempp for temptr renumbering failed.\n');
        end
    end
end

% Assign cleared arrays for results
p = tempp;
tr = temptr;
t = [tr;ones(1,size(tr,2))]; % assign materials to elements
Xq = zeros(size(t,2),1);
Yq = zeros(size(t,2),1);
for i = 1:size(t,2)
    Cg = mean(p(:,t(1:end-1,i)),2);
    Xq(i) = Cg(1);
    Yq(i) = Cg(2);
    if (abs(Cg(1))-CellSize/2)^2+(abs(Cg(2))-CellSize/2)^2<(Diameter/2)^2
        t(end,i) = 2;
    end
end

% Plot the resulting mesh
if strcmp(callmode,'v')
    handle = figure(2);clf,hold all,axis equal,box on;
    plot_mesh(handle,p,t,elemtype,[0,0,1]);
    plot(p(1,:),p(2,:),'.k');
    plot(Xq(t(end,:)==1),Yq(t(end,:)==1),'.r');
    plot(Xq(t(end,:)==2),Yq(t(end,:)==2),'.g');
    set(handle,'name','Mesh and materials','numbertitle','off');
end
fprintf('Time consumed %.g s\n\n',toc(t_mesh));

end
