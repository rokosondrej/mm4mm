function [IP,alpha] = get_gauss_info_1d(Mngauss)

switch Mngauss
    case 1 % linear element
        
        IP = 0;
        alpha = 2;
        
    case 2 % quadratic element
        
        IP = 1/sqrt(3)*[-1
            1];
        alpha = 1.0*[1;1];
        
    case 3 % quadratic element
        
        IP = [-sqrt(3/5)
            0
            sqrt(3/5)];
        alpha = [5/9;8/9;5/9];
        
    otherwise
        error('Required M-Gauss integration rule not implemented.');
end

end
