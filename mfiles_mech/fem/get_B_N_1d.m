function [N,DxN,Jdet,DxiN] = get_B_N_1d(np,tcoord,x)

switch np
    case 2 % linear element
        
        % Evaluations of basis functions
        N = zeros(np,1);
        
        N(1) = 0.5 * (1.0 - tcoord(1));
        N(2) = 0.5 * (1.0 + tcoord(1));
        
        % Derivatives of the basis functions (DxiN - derivatives w.r.t. xi, DetaN - derivatives w.r.t eta)
        DxiN = zeros(np,1);
        
        DxiN(1) = -0.5;
        DxiN(2) = 0.5;
        
        % Jacobian
        Jdet = x*DxiN;
        
        % Derivatives of the basis functions(DxN - derivative of N1 w.r.t. x)
        DxN = DxiN/Jdet;
        
    case 3 % quadratic element
        
        % Evaluations of basis functions
        N = zeros(np,1);
        
        N(1) = -0.5 * tcoord(1) * (1.0 - tcoord(1));
        N(2) = (1.0 + tcoord(1)) * (1.0 - tcoord(1));
        N(3) = 0.5 * tcoord(1) * (1.0 + tcoord(1));
        
        % Derivatives of the basis functions (DxiN - derivatives w.r.t. xi, DetaN - derivatives w.r.t eta)
        DxiN = zeros(np,1);
        
        DxiN(1) = -0.5 + tcoord(1);
        DxiN(2) = -2.0 * tcoord(1);
        DxiN(3) = 0.5 + tcoord(1);
        
        % Jacobian
        Jdet = x*DxiN;
        
        % Derivatives of the basis functions(DxN - derivative of N1 w.r.t. x)
        DxN = DxiN/Jdet;
        
    otherwise
        error('Required M-element not implemented.');
end

end
