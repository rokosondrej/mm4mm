function [uq,Xq] = sample_UFP_1d(pM,tM,u,xq,TOL_g)

% Number of points in an element
np = size(tM,1)-1;

% Allocate outputs
Xq = NaN(size(xq));
uq = NaN(size(xq));

% Loop over all elements
for ie = 1:size(tM,2)
    
    % Get code numbers of i-th element and material ID
    nodes = tM(1:np,ie); % code numbers correspond to node numbers in 1d
    
    % Get element's coordinates
    xp = pM(1,nodes);
    
    % Loop over all query points
    for iq = 1:length(xq)
        
        % Test bounding box
        if min(xp)-TOL_g<xq(iq) && max(xp)+TOL_g>xq(iq)
            
            % Get natural coordinates for xq(iq)
            tcoord = physical2natural_1d(np,xq(iq),xp,TOL_g);
            
            % Get shape function evaluations at tcoord
            N = get_B_N_1d(np,tcoord,xp);
            
            % Reconstruct displacement field
            uq(iq) = dot(N,u(nodes));
            
            % Get coordinates (only check that everything went well)
            Xq(iq) = dot(N,xp);
        end
    end
end
