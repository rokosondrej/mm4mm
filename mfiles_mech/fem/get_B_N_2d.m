function [N,DxN,DyN,Jdet,J] = get_B_N_2d(np,tcoord,x,y)

switch np
    case 3 % linear triangles
        
        % Evaluations of basis functions
        N = tcoord;
        
        % Jacobian corresponds to triangle's area
        J = [];
        Jdet = 1.0 / 2.0 * abs((x(2) * y(3) - x(3) * y(2)) -...
            (x(1) * y(3) - x(3) * y(1)) - (x(2) * y(1) - x(1) * y(2)));
        
        % Coefficients of basis functions
        % double a1 = x(2)*y(3)-x(3)*y(2);
        % double a2 = x(3)*y(1)-x(1)*y(3);
        % double a3 = x(1)*y(2)-x(2)*y(1);
        b1 = y(2) - y(3);
        b2 = y(3) - y(1);
        b3 = y(1) - y(2);
        
        c1 = x(3) - x(2);
        c2 = x(1) - x(3);
        c3 = x(2) - x(1);
        
        % Derivatives of the basis functions(DxN1 - derivative of N1 w.r.t. x, etc.)
        DxN = zeros(np,1);
        DyN = zeros(np,1);
        
        DxN(1) = b1 / (2.0 * Jdet);
        DxN(2) = b2 / (2.0 * Jdet);
        DxN(3) = b3 / (2.0 * Jdet);
        
        DyN(1) = c1 / (2.0 * Jdet);
        DyN(2) = c2 / (2.0 * Jdet);
        DyN(3) = c3 / (2.0 * Jdet);
        
    case 6 % quadratic triangles
        
        % Evaluations of basis functions
        N = [tcoord(1) * (2.0 * tcoord(1) - 1.0);
            tcoord(2) * (2.0 * tcoord(2) - 1.0);
            tcoord(3) * (2.0 * tcoord(3) - 1.0);
            4.0 * tcoord(1) * tcoord(2);
            4.0 * tcoord(2) * tcoord(3);
            4.0 * tcoord(3) * tcoord(1)];
        
        % Jacobian
        dx4 = x(4) - (x(1) + x(2)) / 2.0;
        dx5 = x(5) - (x(2) + x(3)) / 2.0;
        dx6 = x(6) - (x(3) + x(1)) / 2.0;
        dy4 = y(4) - (y(1) + y(2)) / 2.0;
        dy5 = y(5) - (y(2) + y(3)) / 2.0;
        dy6 = y(6) - (y(3) + y(1)) / 2.0;
        
        Jx21 = x(2) - x(1) + 4.0 * (dx4*(tcoord(1) - tcoord(2)) + (dx5 - dx6)*tcoord(3));
        Jx32 = x(3) - x(2) + 4.0 * (dx5*(tcoord(2) - tcoord(3)) + (dx6 - dx4)*tcoord(1));
        Jx13 = x(1) - x(3) + 4.0 * (dx6*(tcoord(3) - tcoord(1)) + (dx4 - dx5)*tcoord(2));
        Jy12 = y(1) - y(2) + 4.0 * (dy4*(tcoord(2) - tcoord(1)) + (dy6 - dy5)*tcoord(3));
        Jy23 = y(2) - y(3) + 4.0 * (dy5*(tcoord(3) - tcoord(2)) + (dy4 - dy6)*tcoord(1));
        Jy31 = y(3) - y(1) + 4.0 * (dy6*(tcoord(1) - tcoord(3)) + (dy5 - dy4)*tcoord(2));
        J = [];
        Jdet = 1.0 / 2.0*(Jx21*Jy31 - Jy12*Jx13);
        
        % Derivatives of the basis functions(DxN1 - derivative of N1 w.r.t. x)
        DxN(1) = (4.0 * tcoord(1) - 1.0)*Jy23 / (2.0 * Jdet);
        DxN(2) = (4.0 * tcoord(2) - 1.0)*Jy31 / (2.0 * Jdet);
        DxN(3) = (4.0 * tcoord(3) - 1.0)*Jy12 / (2.0 * Jdet);
        DxN(4) = 4.0 * (tcoord(2) * Jy23 + tcoord(1) * Jy31) / (2.0 * Jdet);
        DxN(5) = 4.0 * (tcoord(3) * Jy31 + tcoord(2) * Jy12) / (2.0 * Jdet);
        DxN(6) = 4.0 * (tcoord(1) * Jy12 + tcoord(3) * Jy23) / (2.0 * Jdet);
        
        DyN(1) = (4.0 * tcoord(1) - 1.0)*Jx32 / (2.0 * Jdet);
        DyN(2) = (4.0 * tcoord(2) - 1.0)*Jx13 / (2.0 * Jdet);
        DyN(3) = (4.0 * tcoord(3) - 1.0)*Jx21 / (2.0 * Jdet);
        DyN(4) = 4.0 * (tcoord(2) * Jx32 + tcoord(1) * Jx13) / (2.0 * Jdet);
        DyN(5) = 4.0 * (tcoord(3) * Jx13 + tcoord(2) * Jx21) / (2.0 * Jdet);
        DyN(6) = 4.0 * (tcoord(1) * Jx21 + tcoord(3) * Jx32) / (2.0 * Jdet);
        
    case 4 % bi-linear quadrangles
        
        % Evaluations of basis functions
        N = [1.0 / 4.0 * (1.0 - tcoord(1)) * (1.0 - tcoord(2));
            1.0 / 4.0 * (1.0 + tcoord(1)) * (1.0 - tcoord(2));
            1.0 / 4.0 * (1.0 + tcoord(1)) * (1.0 + tcoord(2));
            1.0 / 4.0 * (1.0 - tcoord(1)) * (1.0 + tcoord(2))];
        
        % Derivatives of the basis functions (DxiN - derivatives w.r.t. xi, DetaN - derivatives w.r.t eta)
        DxiN = zeros(np,1);
        DetaN = zeros(np,1);
        
        DxiN(1) = -(1.0 - tcoord(2)) / 4.0;
        DxiN(2) = (1.0 - tcoord(2)) / 4.0;
        DxiN(3) = (1.0 + tcoord(2)) / 4.0;
        DxiN(4) = -(1.0 + tcoord(2)) / 4.0;
        
        DetaN(1) = -(1.0 - tcoord(1)) / 4.0;
        DetaN(2) = -(1.0 + tcoord(1)) / 4.0;
        DetaN(3) = (1.0 + tcoord(1)) / 4.0;
        DetaN(4) = (1.0 - tcoord(1)) / 4.0;
        
        % Jacobian
        J = [DxiN';DetaN']*[x',y'];
        Jdet = det(J);
        
        % Derivatives of basis functions (DxN - derivative w.r.t. x, DyN - derivatives w.r.t. y)
        temp = J\[DxiN';DetaN'];
        DxN = temp(1,:);
        DyN = temp(2,:);
        
    case 8 % 8-node serendipity quadratic quadrangle
        
        % Basis functions expressed in terms of natural coordinates
        N = zeros(np,1);
        N(1) = 1.0 / 4.0 * (1.0 - tcoord(1)) * (1.0 - tcoord(2)) * (-tcoord(1) - tcoord(2) - 1.0);
        N(2) = 1.0 / 4.0 * (1.0 + tcoord(1)) * (1.0 - tcoord(2)) * (tcoord(1) - tcoord(2) - 1.0);
        N(3) = 1.0 / 4.0 * (1.0 + tcoord(1)) * (1.0 + tcoord(2)) * (tcoord(1) + tcoord(2) - 1.0);
        N(4) = 1.0 / 4.0 * (1.0 - tcoord(1)) * (1.0 + tcoord(2)) * (-tcoord(1) + tcoord(2) - 1.0);
        N(5) = 1.0 / 2.0 * (1.0 - tcoord(1) * tcoord(1)) * (1.0 - tcoord(2));
        N(6) = 1.0 / 2.0 * (1.0 + tcoord(1)) * (1.0 - tcoord(2) * tcoord(2));
        N(7) = 1.0 / 2.0 * (1.0 - tcoord(1) * tcoord(1)) * (1.0 + tcoord(2));
        N(8) = 1.0 / 2.0 * (1.0 - tcoord(1)) * (1.0 - tcoord(2) * tcoord(2));
        
        % Derivatives of the basis functions (DxiN - derivatives w.r.t. xi, DetaN - derivatives w.r.t eta)
        DxiN = zeros(np,1);
        DetaN = zeros(np,1);
        
        DxiN(1) = (1.0 - tcoord(2)) * (2.0 * tcoord(1) + tcoord(2)) / 4.0;
        DxiN(2) = (1.0 - tcoord(2)) * (2.0 * tcoord(1) - tcoord(2)) / 4.0;
        DxiN(3) = (1.0 + tcoord(2)) * (2.0 * tcoord(1) + tcoord(2)) / 4.0;
        DxiN(4) = (1.0 + tcoord(2)) * (2.0 * tcoord(1) - tcoord(2)) / 4.0;
        DxiN(5) = tcoord(1) * (tcoord(2) - 1.0);
        DxiN(6) = (1.0 - tcoord(2) * tcoord(2)) / 2.0;
        DxiN(7) = -tcoord(1) * (1.0 + tcoord(2));
        DxiN(8) = -(1.0 - tcoord(2) * tcoord(2)) / 2.0;
        
        DetaN(1) = (1.0 - tcoord(1)) * (tcoord(1) + 2.0 * tcoord(2)) / 4.0;
        DetaN(2) = -(1.0 + tcoord(1)) * (tcoord(1) - 2.0 * tcoord(2)) / 4.0;
        DetaN(3) = (1.0 + tcoord(1)) * (tcoord(1) + 2.0 * tcoord(2)) / 4.0;
        DetaN(4) = -(1.0 - tcoord(1)) * (tcoord(1) - 2.0 * tcoord(2)) / 4.0;
        DetaN(5) = -(1.0 - tcoord(1) * tcoord(1)) / 2.0;
        DetaN(6) = -(1.0 + tcoord(1)) * tcoord(2);
        DetaN(7) = (1.0 - tcoord(1) * tcoord(1)) / 2.0;
        DetaN(8) = -(1.0 - tcoord(1)) * tcoord(2);
        
        % Jacobian
        J = [DxiN';DetaN']*[x',y'];
        Jdet = det(J);
        
        % Derivatives of basis functions (DxN - derivative w.r.t. x, DyN - derivatives w.r.t. y)
        temp = J\[DxiN';DetaN'];
        DxN = temp(1,:);
        DyN = temp(2,:);
        
    otherwise
        error('Required M-element not implemented.');
end

end
