function tcoord = physical2natural_1d(np,xq,xp,TOL_g)

% Invert isoparametric mapping (using standard Newton)
eps = 1+TOL_g;
tcoord = 0.0; % initial guess

% Get initial shape function evaluations and residual vector
[N,~,~,DxiN] = get_B_N_1d(np,tcoord,xp);
f = xq-dot(N,xp);

% Iterate until convergence
Niter = 0;
while eps>TOL_g && Niter<100
    Niter = Niter+1;
    
    % Form Jacobian
    J = -dot(DxiN,xp);
    
    % Solve for increment
    tcoord = tcoord - f/J;
    
    % Update shape function evaluations and residual
    [N,~,~,DxiN] = get_B_N_1d(np,tcoord,xp);
    f = xq-dot(N,xp);
    
    % Update error
    eps = norm(f);
end

% Test maximum number of iterations
if Niter>=100
    error('physical2natural_1d: Niter>100');
end

end
