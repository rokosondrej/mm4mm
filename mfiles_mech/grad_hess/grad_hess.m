function [u,G,H,H_DBC] = grad_hess(p,t,material,ngauss,x,DBCIndices,...
    tDBCValues,FreeIndices,maxNumThreads)

% Reconstruct r
r = p(:);
r(DBCIndices) = r(DBCIndices)+tDBCValues;
r(FreeIndices) = r(FreeIndices)+x;
u = r-p(:);

%% Compute the gradient and Hessian in C++
% [~,G,H] = build_grad_hess_TLF2d(p,t,material,ngauss,u,maxNumThreads); % TL formulation using deformation gradient
if size(t,1)-1==4 && ngauss==1
    % Q4G1 with stabilization
    % 1 hourglass perturbation
    % 2 assumed strain
    method = 2;
    % Penalty for hourglass perturbation (a real non-negative number)
    penalty = 0;
    % Assumed strain stabilization
    % 1 Optimal bending element (OB)
    % 2 Quintessential bending/incompressible element (QBI)
    % 3 Frame-invariant bending element (FIB)
    % 4 Optimal incompressible element (OI)
    element = 2;
    % Get stabilized energy, grad, and Hessian
    [en,eh,G,H] = build_grad_hess_stab_TLE2d(p,t,material,ngauss,u,maxNumThreads,penalty,method,element); % 1 perturbation, 2 assumed strain
    fprintf('eh/en = %g\n',eh/en);
else
    [~,G,H] = build_grad_hess_TLE2d(p,t,material,ngauss,u,maxNumThreads); % TL formulation using Green-Lagrange strain
end

% Construct the gradient
G = G(FreeIndices);

% Assembly the Hessian
H_DBC = H(FreeIndices,DBCIndices);
H = H(FreeIndices,FreeIndices);

end
