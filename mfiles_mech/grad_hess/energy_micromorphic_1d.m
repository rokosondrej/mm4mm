function [en,f,K,fdbc,Kdbc] = energy_micromorphic_1d(x,pM,tM,Mngauss,width,DBCIndv0,...
    DBCValv0,FreeIndv0,DBCIndvi,DBCValvi,FreeIndvi,maxNumThreads,...
    mbctype,testStability,callmode,gradHessType)

% Make the database DATA globally available
global DATA;

% Reconstruct v0 and v1
TOL_g = DATA.TOL_g;
nmodes = DATA.nmodes;
elemtype = DATA.elemtype;
Mnnode = size(pM,2);
xr = zeros((1+nmodes)*Mnnode,1);
xr([FreeIndv0;FreeIndvi]) = x;
xr(DBCIndv0) = DBCValv0; % Dirichled BCs for v0
xr(DBCIndvi) = DBCValvi; % Dirichled BCs for v1
v0nodes = xr(1:Mnnode); % dofs corresponding to v0
vinodes = zeros(Mnnode,nmodes);
for i = 1:nmodes
    vinodes(:,i) = xr(i*Mnnode+1:(i+1)*Mnnode); % dofs corresponding to vi
end
material = DATA.material;
ngauss = DATA.ngauss;
% DiamOuter = DATA.DiamOuter; % outer diameter
Q = DATA.Q;

% Get triangulation and N function
p = DATA.p;
t = DATA.t;
phi = DATA.phi;

% Get data for the m-solver
TOL_r = DATA.TOL_r;
MaxNiter = DATA.MaxNiter;
% DBCIndicesCON = DATA.DBCIndicesCON; % indices for static condensation
% FreeIndicesCON = DATA.FreeIndicesCON;
% IndIndicesCON = DATA.IndIndicesCON;
% DepIndicesCON = DATA.DepIndicesCON;
% FIIndicesCON = DATA.FIIndicesCON;
% DBC2Ind = DATA.DBC2IndCON;
DBCIndicesDBC = DATA.DBCIndicesDBC; % indices for the 'dbc' option
FreeIndicesDBC = DATA.FreeIndicesDBC;
FreeIndicesPBC = DATA.FreeIndicesPBC;
Cpbc = DATA.CPBC;
M = DATA.M;

% Use alternative formulation that Ron suggested.
% Instead of shifting, take integral over Q, but evaluated for
% corresponding deformation gradient F = I+grad(v0)+grad(v1)N+v1*grad(N)
% Get first some data
thickness = material(1,8);
np = size(tM,1)-1;
nelem = size(tM,2);
en = 0;
f = zeros((1+nmodes)*Mnnode,1);
K = sparse((1+nmodes)*Mnnode,(1+nmodes)*Mnnode);
kill = 0; % control switch for NaNs in energy and other kinds of problems

% Get Gauss integration rule
[IP,alpha] = get_gauss_info_1d(Mngauss);

% Loop over all M-elements
for ie = 1:nelem
    
    % Get code numbers of i-th element and material ID
    nodes = tM(1:end-1,ie); % code numbers correspond to node numbers in 1d
    
    % Get element's coordinates
    xp = pM(1,nodes);
    
    % Gauss integration rule, only for linear M-elements
    we = 0;
    f0e = zeros(np,1);
    fie = zeros(np,nmodes);
    K00e = zeros(np);
    K0ie = zeros(np,np,nmodes);
    Kiie = zeros(np,np,nmodes,nmodes);
    K0we = zeros(np,size(p,2)*2);
    Kiwe = zeros(np,size(p,2)*2);
    Kwcond = zeros((1+nmodes)*np);
    switch mbctype
        case 'dbc'
            Kwwe = zeros(size(p,2)*2);
        case 'pbc'
            Kwwe = zeros(size(p,2)*2+size(Cpbc,1)+2);
    end
    
    for ig = 1:Mngauss
        
        % Get natural coordinates of ig-th Gauss integration point
        tcoord = IP(ig,:);
        
        % Get interpolation functions and their derivaties at tcoord
        [N,DxN,Jdet] = get_B_N_1d(np,tcoord,xp);
        
        % Matrix of derivatives Be
        B0e = DxN';
        Nie = N';
        Bie = DxN';
        
        % Get grad(v0), v1, and grad(v1) for given M-element's Gauss point
        gradv0 = [0 0
            0 B0e*v0nodes(nodes)];
        v0 = gradv0*p; % underlying deformation gradient
        v = v0(:); % underlying background field
        vie = zeros(1,nmodes); % vi corresponding to all modes
        gradvie = zeros(2,nmodes); % gradients of vi corresponding to all modes
        for i = 1:nmodes
            % M-quantities
            vie(i) = Nie*vinodes(nodes,i);
            gradvie(:,i) = [0;Bie*vinodes(nodes,i)];
            % Corresponding RVE displacement field driven by grad(v0), vi, and grad(vi)
            vi = (vie(i)+gradvie(:,i)'*p)';
            v(1:2:end) = v(1:2:end)+vi.*phi(1:2:end,i);
            v(2:2:end) = v(2:2:end)+vi.*phi(2:2:end,i);
        end
        
        % Get microfluctuation field w
        switch mbctype
            case 'dbc'
                
                % Check validity of the increment, otherwise kill the call
                % if norm(v-DATA.U(:,(ie-1)*Mngauss+ig),'inf')>0.5*Diameter
                %     kill = 1;
                %     break;
                % end
                
                % Solve the m-problem
                DBCValues = v(DBCIndicesDBC)-DATA.U(DBCIndicesDBC,(ie-1)*Mngauss+ig); % apply DBCs of w
                U = DATA.U(:,(ie-1)*Mngauss+ig);
                status = 1;
                if norm(DBCValues)>TOL_g
                    Time = linspace(0,1,2);
                    status = 0;
                    while ~status && length(Time)<33
                        for it = 2:length(Time)
                            [U(:,it),Niter,minDiag,status] = minimize_r(p,t,material,U(:,it-1),...
                                DBCIndicesDBC,DATA.U(DBCIndicesDBC,(ie-1)*Mngauss+ig)+Time(it-1)*DBCValues,...
                                (Time(it)-Time(it-1))*DBCValues,[],FreeIndicesDBC,TOL_r,MaxNiter,...
                                ngauss,maxNumThreads,1);
                            
                            % Test for success (fail fast and rather choose finer time stepping)
                            if status==0
                                break;
                            end
                            
                            % Test bifurcation
                            if minDiag<0
                                % fprintf('m-solver unstable (%d)\n',tr);
                                % add implementation (maybe also add the solver with cutbacks)
                            end
                        end
                        Time = linspace(0,1,2*length(Time));
                        if status==0 % restart time stepping
                            % Use reference configuration
                            U(:,1) = 0*U(:,1);
                            DBCValues = v(DBCIndicesDBC);
                            DATA.U(:,(ie-1)*Mngauss+ig) = 0*DATA.U(:,(ie-1)*Mngauss+ig);
                            fprintf('m-solver restarted (%d)\n',ie);
                        end
                    end
                end
                % Save U for the next iteration
                DATA.U(:,(ie-1)*Mngauss+ig) = U(:,end);
                
                % Get microscopic tangent for converged U
                [~,~,EH] = build_grad_hess_TLF2d(p,t,material,ngauss,U(:,end),maxNumThreads);
                
                % Get homogenized energy and stresses for u = v0+v1*phi+w = v+w
                [wg,P,Qi,Ri,C00,C0iBB,C0iBN,CiiBB,CiiBN,CiiNN,C0wBB,CiwBB,CiwNB] = micromorphic_computational(p,t,material,...
                    ngauss,U(:,end),nmodes,phi,maxNumThreads);
                
            case 'pbc'
                
                % % Check validity of the increment, otherwise kill the call
                % if norm(v-DATA.U(:,(ie-1)*Mngauss+ig),'inf')>0.5*Diameter
                %     kill = 1;
                %     break;
                % end
                
                % Orthogonality (w,phi) by scalar product
                Cphi = zeros(nmodes,length(v));
                for i = 1:nmodes
                    % Orthogonality (w,phi_i) = 0
                    Cphi(i,:) = phi(:,i)'*M;
                    % Normalize constraints
                    normconst = norm(Cphi(i,:),'inf');
                    Cphi(i,:) = Cphi(i,:)/normconst;
                end
                C = [Cpbc;Cphi];
                
                % Orthogonality (w \otimes X) = 0
                % Cgradv0 = zeros(4,length(v));
                % Cgradv0(1,1:2:end) = p(1,:)*M(1:2:end,1:2:end);
                % Cgradv0(2,2:2:end) = p(1,:)*M(2:2:end,2:2:end);
                % Cgradv0(3,1:2:end) = p(2,:)*M(1:2:end,1:2:end);
                % Cgradv0(4,2:2:end) = p(2,:)*M(2:2:end,2:2:end);
                % % Normalize constraints
                % for i = 1:4
                %     normconst = norm(Cgradv0(i,:),'inf');
                %     Cgradv0(i,:) = Cgradv0(i,:)/normconst;
                % end
                % C = [C;Cgradv0];
                
                % Orthogonality (w,X*phi)
                Cgradphi = zeros(2*nmodes,length(v));
                for i = 1:nmodes
                    % Orthogonality (X \otimes phi1) \cdot w = 0
                    const1 = zeros(length(v),1);
                    const2 = zeros(length(v),1);
                    const1(1:2:end) = p(1,:)'.*phi(1:2:end,i);
                    const1(2:2:end) = p(1,:)'.*phi(2:2:end,i);
                    const2(1:2:end) = p(2,:)'.*phi(1:2:end,i);
                    const2(2:2:end) = p(2,:)'.*phi(2:2:end,i);
                    Cgradphi(2*i-1,:) = const1'*M;
                    Cgradphi(2*i,:) = const2'*M;
                    % Normalize constraints
                    normconst = norm(Cgradphi(2*i-1,:),'inf');
                    Cgradphi(2*i-1,:) = Cgradphi(2*i-1,:)/normconst;
                    normconst = norm(Cgradphi(2*i,:),'inf');
                    Cgradphi(2*i,:) = Cgradphi(2*i,:)/normconst;
                end
                C = [C;Cgradphi];
                
                % Orthogonality (w,1) by components (no DBCs in corners)
                unity = ones(size(p,2),1);
                Cv = zeros(2,length(p(:)));
                Cv(1,1:2:end) = unity'*M(1:2:end,1:2:end);
                Cv(2,2:2:end) = unity'*M(2:2:end,2:2:end);
                % Normalize constraints
                normconst = norm(Cv(1,:),'inf');
                Cv(1,:) = Cv(1,:)/normconst;
                normconst = norm(Cv(2,:),'inf');
                Cv(2,:) = Cv(2,:)/normconst;
                C = [C;Cv];
                % The resulting C and B matrices
                C = sparse(C);
                
                % Solve constrained problem with C*w = B
                U = DATA.U(:,(ie-1)*Mngauss+ig);
                
                % Start always from the reference configuration
                % U(:,1) = 0*U(:,1);
                
                Time = linspace(0,1,2);
                status = 0;
                while ~status && length(Time)<33
                    for it = 2:length(Time)
                        
                        % SOLVE FOR u
                        [U(:,it),Niter,minDiag,status,lambda] = solve_rC(p,t,material,U(:,it-1),...
                            [],[],[],FreeIndicesPBC,C,Time(it)*C*v,TOL_r,MaxNiter,ngauss,maxNumThreads);
                        
                        % Test for success (fail fast and rather choose finer time stepping)
                        if status==0
                            break;
                        end
                        
                        % Test bifurcation
                        if minDiag<0
                            fprintf('m-solver unstable (%d)\n',(ie-1)*Mngauss+ig);
                            % add implementation (maybe also add the solver with cutbacks)
                        end
                        
                    end
                    Time = linspace(0,1,2*length(Time));
                    if status==0 % restart time stepping
                        % Use reference configuration
                        U(:,1) = 0*U(:,1);
                        DATA.U(:,(ie-1)*Mngauss+ig) = 0*DATA.U(:,(ie-1)*Mngauss+ig);
                        fprintf('m-solver restarted (%d)\n',(ie-1)*Mngauss+ig);
                    end
                end
                % Save U for the next iteration
                DATA.U(:,(ie-1)*Mngauss+ig) = U(:,end);
                
                % Get microscopic tangent for converged U
                [~,~,H] = build_grad_hess_TLF2d(p,t,material,ngauss,U(:,end),maxNumThreads);
                EH = [H,C';C,sparse(size(C,1),size(C,1))];
                
                % Get homogenized energy and stresses for u = v0+v1*phi+w = v+w
                [wg,P,Qi,Ri,C00,C0iBB,C0iBN,CiiBB,CiiBN,CiiNN,C0wBB,CiwBB,CiwNB] = ...
                    micromorphic_computational(p,t,material,ngauss,U(:,end),nmodes,...
                    phi,maxNumThreads);
                
                % Compute and plot Lagrange multiplier terms
                if strcmp(callmode,'v')
                    
                    % Compute rhs of the microscopic balance equation
                    lambdaPbc = Cpbc'*lambda(1:size(Cpbc,1));
                    lambdaPhi = Cphi'*lambda(size(Cpbc,1)+1:size(Cpbc,1)+size(Cphi,1));
                    lambdaGradPhi = Cgradphi'*lambda(size(Cpbc,1)+size(Cphi,1)+1:size(Cpbc,1)+size(Cphi,1)+size(Cgradphi,1));
                    lambdaUnity = Cv'*lambda(end-1:end);
                    
                    % Plot corresponding vector fields
                    figure(1);clf;
                    subplot(2,2,1);hold all;axis equal;xlabel('$X_1$');ylabel('$X_2$');title(['PBC, scale = ',num2str(norm(lambdaPbc,'inf'))]);
                    plot_mesh([],p,t,elemtype,0.75*[1,1,1]);
                    quiver(p(1,:)',p(2,:)',lambdaPbc(1:2:end),lambdaPbc(2:2:end),2,'color','r');
                    
                    subplot(2,2,2);hold all;axis equal;xlabel('$X_1$');ylabel('$X_2$');title(['$\sum_{i=1}^{n}\nu_i\vec{\varphi}_i$, scale = ',num2str(norm(lambdaPhi,'inf'))]);
                    plot_mesh([],p,t,elemtype,0.75*[1,1,1]);
                    quiver(p(1,:)',p(2,:)',lambdaPhi(1:2:end),lambdaPhi(2:2:end),2,'color','r');
                    
                    subplot(2,2,3);hold all;axis equal;xlabel('$X_1$');ylabel('$X_2$');title(['$\sum_{i=1}^{n}\vec{\eta}_i\cdot(\vec{\varphi}_i\vec{X}_\mathrm{m})$, scale = ',num2str(norm(lambdaGradPhi,'inf'))]);
                    plot_mesh([],p,t,elemtype,0.75*[1,1,1]);
                    quiver(p(1,:)',p(2,:)',lambdaGradPhi(1:2:end),lambdaGradPhi(2:2:end),2,'color','r');
                    
                    subplot(2,2,4);hold all;axis equal;xlabel('$X_1$');ylabel('$X_2$');title(['$\vec{\mu}$, scale = ',num2str(norm(lambdaUnity,'inf'))]);
                    plot_mesh([],p,t,elemtype,0.75*[1,1,1]);
                    quiver(p(1,:)',p(2,:)',lambdaUnity(1:2:end),lambdaUnity(2:2:end),2,'color','r');
                end
                
            case 'now'
                status = 1; % always success
                
                % Get homogenized energy and stresses for u = v0+v1*phi = v
                [wg,P,Qi,Ri,C00,C0iBB,C0iBN,CiiBB,CiiBN,CiiNN] = ...
                    micromorphic_computational_now(p,t,material,ngauss,v,...
                    nmodes,phi,maxNumThreads);
                
            case 'closed'
                status = 1; % always success
                
                % Get homogenized energy and stresses for u = v0 + vi*phi + w = v + w
                [wg,P,Qi,Ri,C00,C0iBB,C0iBN,CiiBB,CiiBN,CiiNN] = ...
                    micromorphic_closed(p,t,material,ngauss,gradv0,nmodes,...
                    vie,gradvie,phi,maxNumThreads);
                
            otherwise
                error('RVE boundary conditions not supported.');
                
        end
        
        % Plot v, U, and w
        if strcmp(callmode,'v')
            handle = figure(1000+(ie-1)*Mngauss+ig);clf,hold all,axis equal; % reference (black)
            
            plot_mesh(handle,p,t,elemtype,0.75*[1,1,1]); % X (gray)
            
            pdef = reshape(p(:)+v,size(p)); % v (red)
            plot_mesh(handle,pdef,t,elemtype,0.75*[1,0,0]);
            
            if strcmp(mbctype,'pbc') || strcmp(mbctype,'dbc')
                pdef = reshape(p(:)+U(:,end),size(p)); % u (green)
                plot_mesh(handle,pdef,t,elemtype,0.75*[0,1,0]);
                
                % pdef = reshape(p(:)+(U(:,end)-v),size(p)); % w (blue)
                % plot_mesh(handle,pdef,t,elemtype,0.75*[0,0,1]);
                
                % Test periodicity of w
                % plot_periodicity_test(handle,DATA,p,t,U(:,end)-v);
                
                % Export deformed RVE shape into paraview
                ParaView_export_2d('RVE',[0,1],p,t,DATA.elemtype,ngauss,U(:,[1,end]),...
                    material,TOL_g,maxNumThreads,1);
            end
            
            set(handle,'name',['RVE ',num2str((ie-1)*Mngauss+ig)],'numbertitle','off');
            drawnow;
        end
        
        % Test validity of the energy
        if isnan(wg) || status==0
            kill = 1;
            break
        end
        
        % Divide all quantities by the volume (average over all shifts)
        wg = wg/Q;
        P = P/Q;
        Qi = Qi/Q;
        Ri = Ri/Q;
        C00 = C00/Q;
        C0iBB = C0iBB/Q;
        C0iBN = C0iBN/Q;
        CiiBB = CiiBB/Q;
        CiiBN = CiiBN/Q;
        CiiNN = CiiNN/Q;
        
        % Integrate energy
        we = we + alpha(ig)*thickness*Jdet*width*wg; % integrate the entire volume, i.e. Jdet*width*thickness, thickness = 1
        
        % Integrate gradient associated with v0
        f0e = f0e + alpha(ig)*thickness*Jdet*width*B0e'*P(4);
        
        % Integrate gradient associated with vi
        for i = 1:nmodes
            fie(:,i) = fie(:,i) + alpha(ig)*thickness*Jdet*width*(Nie'*Qi(:,i)+Bie'*Ri(2,i));
        end
        
        % Integrate tangent associated with v0
        K00e = K00e + alpha(ig)*thickness*Jdet*width*B0e'*C00(4,4)*B0e;
        
        % Integrate tangent associated with v0 and vi
        for i = 1:nmodes
            K0ie(:,:,i) = K0ie(:,:,i) + alpha(ig)*thickness*Jdet*width*(B0e'*C0iBB(4,2*i)*Bie + B0e'*C0iBN(4,i)*Nie);
        end
        
        % Integrate tangent associated with vi
        for i = 1:nmodes
            for j = 1:nmodes
                Kiie(:,:,i,j) = Kiie(:,:,i,j) + alpha(ig)*thickness*Jdet*width* ...
                    (Bie'*CiiBB(2*i,2*j)*Bie + Bie'*CiiBN(2*i,j)*Nie +...
                    Nie'*CiiBN(2*j,i)'*Bie + Nie'*CiiNN(i,j)*Nie);
            end
        end
        
        % Integrate and condense tangent associated with w
        if strcmp(mbctype,'dbc') || strcmp(mbctype,'pbc')
            EH = EH/Q;
            C0wBB = C0wBB/Q;
            CiwBB = CiwBB/Q;
            CiwNB = CiwNB/Q;
            
            % Integrate Tangent associated with w
            Kwwe = alpha(ig)*thickness*Jdet*width*EH;
            
            % Integrate tangent associated with v0 and w
            K0we = alpha(ig)*thickness*Jdet*width*B0e'*C0wBB(4,:);
            
            % Integrate tangent associated with v1 and w
            for i = 1:nmodes
                Kiwe(:,:,i) = alpha(ig)*thickness*Jdet*width*(Bie'*CiwBB(2*i,:) + Nie'*CiwNB(i,:));
            end
            
            % prepare matrices to condense
            Kw = K0we;
            for i = 1:nmodes
                Kw = [Kw;Kiwe(:,:,i)];
            end
            
            if  strcmp(mbctype,'pbc')
                Kw = [Kw,zeros(np+nmodes*np,size(C,1))];
            else
                Kw(:,DBCIndicesDBC) = [];
                Kwwe(DBCIndicesDBC,:) = [];
                Kwwe(:,DBCIndicesDBC) = [];
            end
            
            % Condense dofs related to w
            Kwcond = Kwcond + Kw*(Kwwe\Kw');
        end
        
    end % end ig point loop
    
    if strcmp(mbctype,'dbc') || strcmp(mbctype,'pbc')
        
        % fill KMe from K00e K0ie Kiie
        KMe = zeros(np+nmodes*np);
        KMe(1:np,1:np) = K00e;
        for i = 1:nmodes
            KMe(1:np,np+(i-1)*np+1:np+i*np) = K0ie(:,:,i);
            KMe(np+(i-1)*np+1:np+i*np,1:np) = K0ie(:,:,i)';
            for j = 1:nmodes
                KMe(np+(i-1)*np+1:np+i*np,np+(j-1)*np+1:np+j*np) = Kiie(:,:,i,j);
            end
        end
        
        % fe = [f0e; fie] - Kw*(Kwwe\fw); but contribution of last part always 0
        % compute condensed Ke
        Ke = KMe - Kwcond;
        
        % fill K00e K0ie Kiie from Ke
        K00e = Ke(1:np,1:np);
        for i = 1:nmodes
            K0ie(:,:,i) = Ke(1:np,np+(i-1)*np+1:np+i*np);
            for j = 1:nmodes
                Kiie(:,:,i,j) = Ke(np+(i-1)*np+1:np+i*np,np+(j-1)*np+1:np+j*np);
            end
        end
    end
    
    % Check validity of the call
    if kill==1
        en = NaN;
        f = NaN*ones(length(xr),1);
        break;
    end
    
    % Allocate energy
    en = en + we;
    
    % Allocate gradient
    f(nodes) = f(nodes)+f0e;
    for i = 1:nmodes
        f(i*Mnnode+nodes) = f(i*Mnnode+nodes) + fie(:,i);
    end
    
    % Allocate tangent
    K(nodes,nodes) = K(nodes,nodes) + K00e;
    for i = 1:nmodes
        for j = 1:nmodes
            K(i*Mnnode+nodes,j*Mnnode+nodes) = K(i*Mnnode+nodes,j*Mnnode+nodes) + Kiie(:,:,i,j);
        end
        K(i*Mnnode+nodes,nodes) = K(i*Mnnode+nodes,nodes) + K0ie(:,:,i)';
        K(nodes,i*Mnnode+nodes) = K(nodes,i*Mnnode+nodes) + K0ie(:,:,i);
    end
    
end

% Take care of boundary conditions
if strcmp(gradHessType,'free')
    fdbc = f([DBCIndv0;DBCIndvi]); % extract reaction forces
    f = f([FreeIndv0;FreeIndvi]); % remove Dirichlet BCs for v0 and v1
    Kdbc = K([FreeIndv0;FreeIndvi],[DBCIndv0;DBCIndvi]);
    K = K([FreeIndv0;FreeIndvi],[FreeIndv0;FreeIndvi]); % remove Dirichlet BCs for v0 and v1
else
    fdbc = [];
    Kdbc = [];
end

end
