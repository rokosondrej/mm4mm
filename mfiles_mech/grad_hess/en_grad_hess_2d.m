function [w,G,H,H_DBC] = en_grad_hess_2d(p,t,material,ngauss,x,DBCIndices,...
    tDBCValues,FreeIndices,maxNumThreads)

% Reconstruct r
r = p(:);
r(DBCIndices) = r(DBCIndices)+tDBCValues;
r(FreeIndices) = r(FreeIndices)+x;
u = r-p(:);

%% Compute the gradient and Hessian in C++
[w,G,H] = build_grad_hess_TLE2d(p,t,material,ngauss,u,maxNumThreads); % TL formulation using Green-Lagrange strain

% Construct the gradient
G = G(FreeIndices);

% Assembly the Hessian
H_DBC = H(FreeIndices,DBCIndices);
H = H(FreeIndices,FreeIndices);

end
