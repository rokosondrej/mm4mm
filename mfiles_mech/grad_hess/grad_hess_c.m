function [u,f_r,f_rDBC,K_r,K_rDBC] = grad_hess_c(p,t,material,ngauss,x,...
    DBCIndices,DBC2Ind,tDBCValues,IndIndices,DepIndices,FreeIndices,...
    FIIndices,C,maxNumThreads)

% Reconstruct r, u
r = p(:);
r(DBCIndices) = r(DBCIndices)+tDBCValues;
r(FreeIndices) = r(FreeIndices)+x;
r(DepIndices) = C(:,IndIndices)*r(IndIndices);
u = r-p(:);

% Compute the gradient and Hessian in C++
% [~,f,K] = build_grad_hess_TLF2d(p,t,material,ngauss,u,maxNumThreads); % TL formulation using deformation gradient
[~,f,K] = build_grad_hess_TLE2d(p,t,material,ngauss,u,maxNumThreads); % TL formulation using Green-Lagrange strain

f_r = f(IndIndices) + C(:,IndIndices)'*f(DepIndices);
f_rDBC = f_r(DBC2Ind);
K_r = K(IndIndices,IndIndices)+K(IndIndices,DepIndices)*C(:,IndIndices)+...
    C(:,IndIndices)'*K(DepIndices,IndIndices)+...
    C(:,IndIndices)'*K(DepIndices,DepIndices)*C(:,IndIndices);
K_rDBC = K_r(FIIndices,DBC2Ind);
K_r = K_r(FIIndices,FIIndices);
f_r = f_r(FIIndices);

end
