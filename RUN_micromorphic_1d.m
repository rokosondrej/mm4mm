%% Clear workspace
clc; % clear command line
close all;
warning('on');
matlabrc; % restore MATLAB path, etc.
path(genpath([pwd,'/mfiles_mech']),path); % add path to mfiles folder containing all *.m files related to mechanics
path([pwd,'/mex'],path); % add path to mex folder containing all *.mex files
path([pwd,'/gmsh'],path); % add path to gmsh folder containing gmsh.exe and corresponding IO files

%% Specify inputs

% Specify maximum number of threads used in mex files by OpenMP
maxNumThreads = 1;

% Call mode of functions: 's' - silent mode, 'v' - verbose mode
callmode = 's';

% RVE type (find further options inside get_rve_data function)
% 'rect_square' rectangular RVE, square packing, circles
% 'rect_hexa' rectangular RVE, hexagonal packing, circles/hexagons
% 'skew_hexa' skewed RVE, hexagonal packing, circles/hexagons
% 'hexa_hexa' hexagonal RVE, hexagonal packing, circles/hexagons
rvetype = 'rect_square';
inclusiontype = 'circle'; % 'circle', 'hexagon'
CellSize = 1; % size of the primitive cell
if strcmp(rvetype,'rect_square')
    DiamOuter = 0.85*CellSize;
    DiamInner = DiamOuter;
    mhmax = CellSize/5;
else
    DiamOuter = CellSize*1.6; % diameter of the outer circle
    if strcmp(inclusiontype,'hexagon')
        const = 1-0.1*tan(pi/6); % hexagon, for w/l = 0.1
        DiamInner = const*DiamOuter; % diameter of the inner circle
        mhmax = (DiamOuter-DiamInner)/2; % maximum element's size
    elseif strcmp(inclusiontype,'circle')
        const = 0.8; % circle
        DiamInner = const*DiamOuter; % diameter of the inner circle
        mhmax = (DiamOuter-DiamInner)/2; % maximum element's size
    end
end

% Type of the long-range correlated mode
% 'analytic' (analytical approximation)
% 'linbuckling' (linear buckling analysis)
% 'deformed' (deformed shape solved by Newton)
modetype = 'analytic';

% Choose mode shape between pattern and mode (applies only for modetype = 'deformed')
modeshape = 'pattern'; % 'pattern', 'mode'

% Material = [SW,m1,m2,m3,m4,m5,kappa,thickness], C = F'*F, I1 = trace(C), I2 = 0.5*(tr(C)^2-tr(C^2)), J = det(F)
% SW = 0: no material (hole)
% SW = 1: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+1/2*kappa(ln(J))^2 (OOFEM)
% SW = 2: W(F) = m1(I1-3)+m2(I1-3)^2-2m1*ln(J)+1/2*kappa(J-1)^2 (Bertoldi, Boyce)
% SW = 3: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+m3(I1/J^{2/3}-3)*(I2/J^{4/3}-3)+m4(I1/J^{2/3}-3)^2+m5(I1/J^{2/3}-3)^3+9/2*kappa(J^{1/3}-1)^2 (Jamus, Green, Simpson)
% SW = 4: W(F) = m1(I1/J^{2/3}-3)+m2(I2/J^{4/3}-3)+m3(I1/J^{2/3}-3)*(I2/J^{4/3}-3)+m4(I1/J^{2/3}-3)^2+m5(I2/J^{2/3}-3)^2+kappa*(J-1)^2 (five-term Mooney-Rivlin)
% SW = 5: W(F) = 0.5*(0.5*(C-I)*(m1*)*0.5*(C-I)) (linear elastic material)
% material = [2,0.55e3,0.3e3,0,0,0,55e3,1 % for the surrounding matrix
%     0,1e-6,0,0,0,0,1e-6,1]; % for circular inclusions

% Material parameters from Bertoldi
c1 = 0.55e3;
c2 = 0.3e3;
% kappa = 55e3;
% Parametrize material model by Bertoldi
K2G = 53; % original is 52.848484848484851, minimum is 8*c2/mu+2/3 to have kappa = 0
% Lame constants
mu = 2*c1; % shear modulus
kappa = mu*(K2G-2/3)-8*c2; % original is 55e3 corresponding to Bertoldi
lambda = 8*c2+kappa;
K = lambda+2/3*mu; % bulk modulus
% Poissson's ratio
fprintf('Poisson ratio nu = %g\n',lambda./(2*(lambda+mu)));
% Material model
material = [2,c1,c2,0,0,0,kappa,1 % for the surrounding matrix
    0,1e-6,0,0,0,0,1e-6,1]; % for circular inclusions

% Choose m-element type
melemtype = 9; % 2 = T3, 9 = T6, 21 = T10; 3 = Q4, 16 = Q8, 10 = Q9, 36 = Q16

% Choose m-Gauss integration rule (the mesh is homogeneous and the same integration rule for all elements is used)
% triangles: ngauss = 1, 3 (interior), -3 (midedge), 4, 6, 7
% quadrangles: ngauss = 1, 4, 9, 16, 25
switch melemtype
    case 2 % linear triangle
        mngauss = 1;
    case 3 % four node quadrangle
        mngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 9 % quadratic triangle
        mngauss = 3;
    case 16 % eight node quadrangle
        mngauss = 2 * 2; % product of two one-dimensional Gauss integration rules
    case 10 % nine node quadrangle
        mngauss = 3 * 3; % product of two one-dimensional Gauss integration rules
    case 21 % cubic triangle
        mngauss = 6;
    case 36 % cubic quadrangle
        mngauss = 4 * 4;
    otherwise
        error('Wrong element type chosen.');
end

% Tolerances
TOL_x = 1e-3; % M-minimization algorithm tolerance (dofs)
TOL_f = 1e-3; % M-minimization algorithm tolerance (gradient)
TOL_r = 1e-3; % m-minimization algorithm tolerance (gradient)
MaxNiter = 30; % maximum number of m-Newton interations
TOL_g = 1e-10; % geometric tolerance; distance < TOL_g is treated as zero

% Matlab optimization toolbox options
optionsun = optimoptions('fminunc','algorithm','trust-region','Display',...
    'iter-detailed','MaxFunEvals',1e6,'MaxIter',1e4,'TolX',0,'TolFun',0,...
    'GradObj','on','Hessian','on','OutputFcn',...
    @(x,optimValues,state)outfun(x,optimValues,state,TOL_x,TOL_f));

%% Get RVE data
time_total = tic;
global DATA; % make DATA global, so its contents can be adjusted inside fminunc during iterative calls
nmodes = get_rve_data(CellSize,rvetype,inclusiontype,DiamInner,DiamOuter,...
    mhmax,modetype,modeshape,material,melemtype,mngauss,MaxNiter,TOL_g,TOL_r,...
    maxNumThreads,callmode);

%% Get macroscopic discretization and boundary conditions
% Specify inputs for M-solver
NcellY = 5;
MSizeY = NcellY*CellSize/2;
mbctype = 'pbc'; % 'closed', 'now', 'pbc', 'dbc'
width = 2*CellSize; % width of the M-domain
Melemtype = 2; % 1 = linear, 2 = quadratic
switch Melemtype
    case 1 % 2-node linear element
        Mngauss = 1;
    case 2 % 3-node quadratic element
        Mngauss = 2;
    otherwise
        error('Wrong M-element type chosen.');
end

% Applied overall strain
G = -0.1;

% Build M-mesh
% Number of line elements throughtout specimen's height (the same for v0 and v1)
Mnnode = max(2*NcellY+1,21);

% Uniform M-mesh
y0 = MSizeY*linspace(-1,1,Mnnode)';

% Optimal M-mesh
% ya = -NcellY/2:2:min(-NcellY/2+6,0);
% if min(abs(ya))>1
%     yb = [min(-NcellY/2+6,0),0];
%     yc = [ya,yb,-ya(end:-1:1),-yb(end:-1:1)];
% else
%     yc = [ya,-ya(end:-1:1)];
% end
% yd = unique(yc);
% ye = sort(yd,'ascend');
% y0 = ye'*CellSize;
% Mnnode = length(y0);

if Melemtype==1
    tM = [1:length(y0)-1;
        2:length(y0)];
elseif Melemtype==2
    tM = [1:2:length(y0)-2;
        2:2:length(y0)-1;
        3:2:length(y0)];
end
tM = [tM;ones(1,size(tM,2))];
pM = y0';

% Get BCs and other constraints
MIDGamma_1 = find(pM<min(pM)+TOL_g)';
MIDGamma_2 = find(pM>max(pM)-TOL_g)';
MIDGamma = unique([MIDGamma_1;MIDGamma_2]);
DBCIndvi = [];
for i = 1:nmodes
    DBCIndvi = [DBCIndvi;i*Mnnode+MIDGamma];
end
DBCValvi = 0*DBCIndvi;
xy = G*pM;
DBCIndv0 = MIDGamma;
DBCValv0 = xy(MIDGamma)';
xinitv0 = xy(:); % init v0 as an affine function corresponding to G

% Get free indices
FreeIndv0 = setdiff(1:Mnnode,DBCIndv0)';
FreeIndvi = setdiff(Mnnode+1:(1+nmodes)*Mnnode,DBCIndvi)';

% Minimize the energy
% Solve the response in a time-stepping manner
Time = linspace(0,1,101);
R = zeros((1+nmodes)*Mnnode,length(Time));
DATA.U = zeros(length(DATA.p(:)),size(tM,2)*Mngauss);
F = zeros((1+nmodes)*Mnnode,length(Time));
Fdbc = zeros(length([DBCIndv0;DBCIndvi]),length(Time));
En = zeros(length(Time),1);
for i = 2:length(Time)
    t_step = tic;
    
    % Print time step message
    fprintf('\n\n\n==============================================\n');
    fprintf('Time step %g/%g\n',i,length(Time));
    fprintf('==============================================\n');
    
    % Get initial guess
    tDBCValv0 = Time(i-1)*DBCValv0;
    dtDBCValv0 = (Time(i)-Time(i-1))*DBCValv0;
    tDBCValvi = Time(i-1)*DBCValvi;
    dtDBCValvi = (Time(i)-Time(i-1))*DBCValvi;
    [~,f,H] = energy_micromorphic_1d(R([FreeIndv0;FreeIndvi],i-1),pM,tM,Mngauss,width,...
        DBCIndv0,tDBCValv0,FreeIndv0,DBCIndvi,tDBCValvi,FreeIndvi,maxNumThreads,mbctype,0,'s','full');
    xinit = R([FreeIndv0;FreeIndvi],i-1)-H([FreeIndv0;FreeIndvi],[FreeIndv0;FreeIndvi])\...
        (f([FreeIndv0;FreeIndvi])+H([FreeIndv0;FreeIndvi],[DBCIndv0;DBCIndvi])*[dtDBCValv0;dtDBCValvi]);
    txinit = zeros((2+nmodes)*Mnnode,1);
    txinit([FreeIndv0;FreeIndvi]) = xinit;
    txinit(DBCIndv0) = tDBCValv0+dtDBCValv0; % dofs corresponding to v0
    txinit(DBCIndvi) = tDBCValvi+dtDBCValvi; % dofs corresponding to vi
    for j = 1:nmodes % :nmodes
        txinit((1+j)*Mnnode+1:(2+j)*Mnnode) = txinit((1+j)*Mnnode+1:(2+j)*Mnnode)+0.1;
    end
    xinit = txinit([FreeIndv0;FreeIndvi]);
    
    % Correct for the influence of w
    [xsol,res,exitFlag,output] = fminunc(@(x)energy_micromorphic_1d(x,...
        pM,tM,Mngauss,width,DBCIndv0,tDBCValv0+dtDBCValv0,FreeIndv0,DBCIndvi,...
        tDBCValvi+dtDBCValvi,FreeIndvi,maxNumThreads,mbctype,0,'s','free'),...
        xinit,optionsun);
    
    % Get energy and forces
    [En(i),F(:,i)] = energy_micromorphic_1d(xsol,pM,tM,Mngauss,width,DBCIndv0,...
        tDBCValv0+dtDBCValv0,FreeIndv0,DBCIndvi,tDBCValvi+dtDBCValvi,FreeIndvi,...
        maxNumThreads,mbctype,0,'s','full');
    
    % Reconstruct the solution
    xr = zeros((1+nmodes)*Mnnode,1);
    xr([FreeIndv0;FreeIndvi]) = xsol;
    xr(DBCIndv0) = tDBCValv0+dtDBCValv0; % dofs corresponding to v0
    xr(DBCIndvi) = tDBCValvi+dtDBCValvi; % dofs corresponding to vi
    
    % Store some results
    R(:,i) = xr;
    fprintf('Increment computed in %g seconds\n',toc(t_step));
end

% Plot the solution
idstep = length(Time);
plot_results_micromorphic_1d(MSizeY,R,idstep,nmodes,Melemtype,pM,tM,Mnnode,TOL_g);

%% Save some data
% save('workspace','-v7.3');

fprintf('\n\n==============================================\nTotal time consumed %g\n',toc(time_total));
fprintf('==============================================\n\n');
